<?php
// Theme Child by GuGo Creative
if ( !defined( 'ABSPATH' ) ) exit;

//Do not modify or remove comment markers above or below:
         
if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        wp_enqueue_style( 'chld_thm_cfg_separate', trailingslashit( get_stylesheet_directory_uri() ) . 'ctc-style.css', array( 'theme-style','theme-adapt','theme-retina','theme-icons','animate','ilightbox','ilightbox-skin-' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css' );

// END ENQUEUE PARENT ACTION
//
// Custom code by GuGo Creative: