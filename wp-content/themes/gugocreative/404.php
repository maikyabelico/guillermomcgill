<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * 404 Error Page Template
 * Created by CMSMasters
 * 
 */


get_header();


$cmsms_option = cmsmasters_get_global_options();

?>

</div>

<!-- _________________________ Start Content _________________________ -->
<div class="entry">
	<div class="error">
		<div class="error_bg">
			<div class="error_wrap">
				<div class="error_inner<?php
					if ($cmsms_option[CMSMS_SHORTNAME . '_error_block_position'] == 'center') {
						echo ' align_block_center';
					}
					
					if ($cmsms_option[CMSMS_SHORTNAME . '_error_text_position'] == 'center') {
						echo ' align_text_center';
					}
					
					if ($cmsms_option[CMSMS_SHORTNAME . '_error_block_position'] == 'right') {
						echo ' align_block_right';
					}
					
					if ($cmsms_option[CMSMS_SHORTNAME . '_error_text_position'] == 'right') {
						echo ' align_text_right';
					}
				?>" 
					
					<?php					
					echo 'style="' . 
						'width:' . ($cmsms_option[CMSMS_SHORTNAME . '_error_block_width']) . '%' . ';' . 
					'"';
					?> >
						
					<h1 class="error_title">
						<span>404</span>
					</h1>
					
					<?php 
					echo '<h2 class="error_subtitle">' . 
						esc_html__("We're sorry, but", 'music-band') . 
						'<strong>' . esc_html__("the page", 'music-band') . '</strong>' . 
						esc_html__("you were looking for", 'music-band') . 
						'<strong>' . esc_html__("doesn't exist.", 'music-band') . '</strong>' . 
					'</h2>';
					?>
					
					<div class="error_search_button_wrap">
					<?php 
						if ($cmsms_option[CMSMS_SHORTNAME . '_error_search']) { 
							get_search_form(); 
						}
						
						
						if ($cmsms_option[CMSMS_SHORTNAME . '_error_sitemap_button'] && $cmsms_option[CMSMS_SHORTNAME . '_error_sitemap_link'] != '') {
							echo '<div class="error_button_wrap"><a href="' . esc_url($cmsms_option[CMSMS_SHORTNAME . '_error_sitemap_link']) . '" class="button">' . esc_html__('Sitemap', 'music-band') . '</a></div>';
						}
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content_wrap fullwidth">
<!-- _________________________ Finish Content _________________________ -->

<?php 
get_footer();

