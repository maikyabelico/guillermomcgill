/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.1
 * 
 * Theme Information
 * Created by CMSMasters
 * 
 */


Version 1.0.1: Update
	music-band/functions.php
	music-band/header.php
	music-band/image.php
	music-band/readme.txt
	music-band/search.php
	music-band/style.css
	music-band/css/adaptive.css
	music-band/css/cmsms-woo-adaptive.css
	music-band/css/cmsms-woo-style.css
	music-band/css/less/adaptive.less
	music-band/css/less/cmsms-woo-adaptive.less
	music-band/css/less/cmsms-woo-style.less
	music-band/css/less/style.less
	music-band/framework/admin/options/cmsms-theme-options.php
	music-band/framework/admin/settings/cmsms-theme-settings-general.php
	music-band/framework/admin/settings/cmsms-theme-settings.php
	music-band/framework/class/widgets.php
	music-band/framework/function/breadcrumbs.php
	music-band/framework/function/template-functions-post.php
	music-band/framework/function/template-functions-profile.php
	music-band/framework/function/template-functions-project.php
	music-band/framework/function/template-functions-shortcodes.php
	music-band/framework/function/template-functions.php
	music-band/framework/function/theme-functions.php
	music-band/framework/function/theme-colors-secondary.php
	music-band/framework/postType/blog/page/default/gallery.php
	music-band/framework/postType/blog/page/masonry/gallery.php
	music-band/framework/postType/blog/page/puzzle/gallery.php
	music-band/framework/postType/blog/page/timeline/gallery.php
	music-band/framework/postType/blog/post/fullwidth/gallery.php
	music-band/framework/postType/blog/post/sidebar/gallery.php
	music-band/framework/postType/portfolio/post/gallery.php
	music-band/framework/postType/portfolio/post/standard.php
	music-band/framework/postType/profile/page/horizontal.php
	music-band/framework/postType/profile/page/vertical.php
	music-band/framework/postType/profile/post/standard.php
	music-band/tribe-events/pro/widgets/list-widget.php


Version 1.0: Release!

