<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.1
 * 
 * Standard Project Format Template
 * Created by CMSMasters
 * 
 */


$cmsms_option = cmsmasters_get_global_options();


$cmsms_project_title = get_post_meta(get_the_ID(), 'cmsms_project_title', true);

$cmsms_project_subtitle = get_post_meta(get_the_ID(), 'cmsms_project_subtitle', true);

$cmsms_project_sharing_box = get_post_meta(get_the_ID(), 'cmsms_project_sharing_box', true);

$cmsms_project_features = get_post_meta(get_the_ID(), 'cmsms_project_features', true);


$cmsms_project_link_text = get_post_meta(get_the_ID(), 'cmsms_project_link_text', true);
$cmsms_project_link_url = get_post_meta(get_the_ID(), 'cmsms_project_link_url', true);
$cmsms_project_link_target = get_post_meta(get_the_ID(), 'cmsms_project_link_target', true);


$cmsms_project_details_title = get_post_meta(get_the_ID(), 'cmsms_project_details_title', true);


$cmsms_project_features_one_title = get_post_meta(get_the_ID(), 'cmsms_project_features_one_title', true);
$cmsms_project_features_one = get_post_meta(get_the_ID(), 'cmsms_project_features_one', true);

$cmsms_project_features_two_title = get_post_meta(get_the_ID(), 'cmsms_project_features_two_title', true);
$cmsms_project_features_two = get_post_meta(get_the_ID(), 'cmsms_project_features_two', true);

$cmsms_project_features_three_title = get_post_meta(get_the_ID(), 'cmsms_project_features_three_title', true);
$cmsms_project_features_three = get_post_meta(get_the_ID(), 'cmsms_project_features_three', true);


$cmsms_project_images = explode(',', str_replace(' ', '', str_replace('img_', '', get_post_meta(get_the_ID(), 'cmsms_project_images', true))));


$project_details = '';

if (
	$cmsms_option[CMSMS_SHORTNAME . '_portfolio_project_like'] || 
	$cmsms_option[CMSMS_SHORTNAME . '_portfolio_project_date'] || 
	$cmsms_option[CMSMS_SHORTNAME . '_portfolio_project_cat'] || 
	$cmsms_option[CMSMS_SHORTNAME . '_portfolio_project_comment'] || 
	$cmsms_option[CMSMS_SHORTNAME . '_portfolio_project_author'] || 
	$cmsms_option[CMSMS_SHORTNAME . '_portfolio_project_tag'] || 
	(
		!empty($cmsms_project_features[1][0]) && 
		!empty($cmsms_project_features[1][1])
	) || 
	$cmsms_option[CMSMS_SHORTNAME . '_portfolio_project_link']
) {
	$project_details = 'true';
}


$project_sidebar = '';

if (
	$project_details == 'true' || 
	$cmsms_project_sharing_box == 'true' || 
	(
		!empty($cmsms_project_features_one[1][0]) && 
		!empty($cmsms_project_features_one[1][1])
	) || (
		!empty($cmsms_project_features_two[1][0]) && 
		!empty($cmsms_project_features_two[1][1])
	) || (
		!empty($cmsms_project_features_three[1][0]) && 
		!empty($cmsms_project_features_three[1][1])
	)
) {
	$project_sidebar = 'true';
}

?>

<!--_________________________ Start Standard Project _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="cmsms_project_img_wrap">
	<?php
		if (!post_password_required()) {
			cmsmasters_thumb(get_the_ID(), 'project-thumb', false, 'img_' . get_the_ID(), true, true, false, true, false);
			
			if ($cmsms_project_images !='') {
			?>
				<div class="cmsms_project_gallery">
				<?php 
					foreach ($cmsms_project_images as $cmsms_project_image) {
						$image_atts = wp_prepare_attachment_for_js($cmsms_project_image);
						
						
						echo '<div>' . 
							'<figure>' . 
								wp_get_attachment_image($cmsms_project_image, 'project-thumb', false, array( 
									'class' => 'full-width', 
									'alt' => ($image_atts['alt'] != '') ? esc_attr($image_atts['alt']) : cmsms_title(get_the_ID(), false), 
									'title' => ($image_atts['title'] != '') ? esc_attr($image_atts['title']) : cmsms_title(get_the_ID(), false) 
								)) . 
							'</figure>' . 
						'</div>';
					}
				?>
				</div>
			<?php 
			}
		}
	?>
	</div>
	<div class="cmsms_project_inner">
	<?php
		if ($cmsms_project_title == 'true') {
			cmsmasters_project_title_nolink(get_the_ID(), 'h2', $cmsms_project_subtitle, 'h4');
		}
		
		
		if ($project_sidebar == 'true') {
			echo '<div class="project_sidebar">';
				
				if ($project_details == 'true') {
					echo '<div class="project_details entry-meta">' . 
						'<h4 class="project_details_title">' . esc_html($cmsms_project_details_title) . '</h4>';
						
						
						cmsmasters_project_like('post');
						
						cmsmasters_project_date('post');
						
						cmsmasters_project_category(get_the_ID(), 'pj-categs', 'post');
						
						cmsmasters_project_comments('post');
						
						cmsmasters_project_author('post');
						
						cmsmasters_project_tags(get_the_ID(), 'pj-tags', 'post');
						
						cmsmasters_project_features('details', $cmsms_project_features, false, 'h4', true);
						
						cmsmasters_project_link($cmsms_project_link_text, $cmsms_project_link_url, $cmsms_project_link_target);
						
					echo '</div>';
				}
				
				
				cmsmasters_project_features('features', $cmsms_project_features_one, $cmsms_project_features_one_title, 'h4', true);
				
				cmsmasters_project_features('features', $cmsms_project_features_two, $cmsms_project_features_two_title, 'h4', true);
				
				cmsmasters_project_features('features', $cmsms_project_features_three, $cmsms_project_features_three_title, 'h4', true);
				
			echo '</div>';
		}
		
		
		echo '<div class="project_content">' . "\n" . 
			'<div class="cmsms_project_content entry-content">' . "\n";
				
				the_content();
				
				
				wp_link_pages(array( 
					'before' => '<div class="subpage_nav" role="navigation">' . '<strong>' . esc_html__('Pages', 'music-band') . ':</strong>', 
					'after' => '</div>', 
					'link_before' => ' [ ', 
					'link_after' => ' ] ' 
				));
				
				
				echo '<div class="cl"></div>' . 
			'</div>' . 
		'</div>';
		
		
		if ($project_sidebar == 'true') {
			echo '<div class="project_sidebar">';
				
				if ($cmsms_project_sharing_box == 'true') {
					//Testos de los proyectos compartir
					//JAT
					cmsmasters_sharing_box(esc_html__('Comparte este proyecto:', 'music-band'), 'h4');
				}
				
			echo '</div>';
		}
	?>
		<div class="cl"></div>
	</div>
</article>
<!--_________________________ Finish Standard Project _________________________ -->

