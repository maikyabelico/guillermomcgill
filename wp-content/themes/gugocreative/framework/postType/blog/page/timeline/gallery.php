<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.1
 * 
 * Blog Page Timeline Gallery Post Format Template
 * Created by CMSMasters
 * 
 */


global $cmsms_metadata;


$cmsms_post_metadata = explode(',', $cmsms_metadata);


$date = (in_array('date', $cmsms_post_metadata) || is_home()) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsms_post_metadata) || is_home())) ? true : false;
$author = (in_array('author', $cmsms_post_metadata) || is_home()) ? true : false;
$views = (in_array('views', $cmsms_post_metadata) || is_home()) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsms_post_metadata) || is_home())) ? true : false;
$likes = (in_array('likes', $cmsms_post_metadata) || is_home()) ? true : false;
$tags = (get_the_tags() && (in_array('tags', $cmsms_post_metadata) || is_home())) ? true : false;
$more = (in_array('more', $cmsms_post_metadata) || is_home()) ? true : false;


$cmsms_post_images = explode(',', str_replace(' ', '', str_replace('img_', '', get_post_meta(get_the_ID(), 'cmsms_post_images', true))));


$uniqid = uniqid();

?>

<!--_________________________ Start Gallery Article _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class('cmsms_timeline_type'); ?>>
	<div class="cmsms_post_info entry-meta">
		<?php $date ? cmsmasters_post_date('page', 'timeline') : ''; ?>
	</div>
	<div class="cmsms_post_cont">
	<?php 
		if (!post_password_required()) {
			if (sizeof($cmsms_post_images) > 1) {
		?>
			<script type="text/javascript">
				jQuery(document).ready(function () { 
					jQuery('#cmsms_hover_slider_<?php echo esc_attr($uniqid); ?>').cmsmsHoverSlider( { 
						sliderBlock : '#cmsms_hover_slider_<?php echo $uniqid; ?>', 
						sliderItems : '.cmsms_hover_slider_items', 
						thumbWidth : '80', 
						thumbHeight : '50', 
						activeSlide : 1, 
						pauseTime : 5000, 
						pauseOnHover : true
					} );
				} );
			</script>
			<div id="cmsms_hover_slider_<?php echo esc_attr($uniqid); ?>" class="cmsms_hover_slider">
				<ul class="cmsms_hover_slider_items">
			<?php
			foreach ($cmsms_post_images as $cmsms_post_image) {
				echo '<li>' . 
					'<figure class="cmsms_hover_slider_full_img">' . 
						wp_get_attachment_image($cmsms_post_image, 'post-thumbnail', false, array( 
							'class' => 	'full-width', 
							'alt' => 	cmsms_title(get_the_ID(), false), 
							'title' => 	cmsms_title(get_the_ID(), false) 
						)) . 
					'</figure>' . 
				'</li>';
			}
			?>
				</ul>
			</div>
			<?php 
			} elseif (sizeof($cmsms_post_images) == 1 && $cmsms_post_images[0] != '') {
				cmsmasters_thumb(get_the_ID(), 'masonry-thumb', false, 'img_' . get_the_ID(), true, true, true, true, $cmsms_post_images[0]);
			} elseif (has_post_thumbnail()) {
				cmsmasters_thumb(get_the_ID(), 'masonry-thumb', false, 'img_' . get_the_ID(), true, true, true, true, false);
			}
		}
		
		
		cmsmasters_post_heading(get_the_ID(), 'h3');
		
		
		if ($author || $categories || $tags) {
			echo '<div class="cmsms_post_cont_info entry-meta' . ((theme_excerpt(20, false) == '') ? ' no_bdb' : '') . '">';
				
				$author ? cmsmasters_post_author('page') : '';
				
				$categories ? cmsmasters_post_category('page') : '';
				
				$tags ? cmsmasters_post_tags('page') : '';
				
			echo '</div>';
		}
		
		
		cmsmasters_post_exc_cont();
		
		
		if ($comments || $likes  || $more) {
			echo '<footer class="cmsms_post_footer entry-meta">';
				
				if ($comments || $likes) {
					echo '<div class="cmsms_post_meta_info">';
					
						$views ? cmsmasters_post_view('page') : '';
						
						$comments ? cmsmasters_post_comments('page') : '';
						
						$likes ? cmsmasters_post_like('page') : '';
						
					echo '</div>';
				}
				
				
				$more ? cmsmasters_post_more(get_the_ID()) : '';
				
			echo '</footer>';
		}
	?>
		<div class="cl"></div>
	</div>
</article>
<!--_________________________ Finish Gallery Article _________________________ -->

