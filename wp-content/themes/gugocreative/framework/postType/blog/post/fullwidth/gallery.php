<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.1
 * 
 * Blog Post Full Width Gallery Post Format Template
 * Created by CMSMasters
 * 
 */


$cmsms_option = cmsmasters_get_global_options();


$cmsms_post_title = get_post_meta(get_the_ID(), 'cmsms_post_title', true);

$cmsms_post_images = explode(',', str_replace(' ', '', str_replace('img_', '', get_post_meta(get_the_ID(), 'cmsms_post_images', true))));


$uniqid = uniqid();

?>

<!--_________________________ Start Gallery Article _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="cmsms_post_cont">
	<?php 
		if (!post_password_required()) {
			if (sizeof($cmsms_post_images) > 1) {
		?>
			<script type="text/javascript">
				jQuery(document).ready(function () { 
					jQuery('#cmsms_hover_slider_<?php echo esc_attr($uniqid); ?>').cmsmsHoverSlider( { 
						sliderBlock : '#cmsms_hover_slider_<?php echo $uniqid; ?>', 
						sliderItems : '.cmsms_hover_slider_items', 
						thumbWidth : '105', 
						thumbHeight : '65', 
						activeSlide : 1, 
						pauseTime : 5000, 
						pauseOnHover : true
					} );
				} );
			</script>
			<div id="cmsms_hover_slider_<?php echo esc_attr($uniqid); ?>" class="cmsms_hover_slider">
				<ul class="cmsms_hover_slider_items">
			<?php
			foreach ($cmsms_post_images as $cmsms_post_image) {
				echo '<li>' . 
					'<figure class="cmsms_hover_slider_full_img">' . 
						wp_get_attachment_image($cmsms_post_image, 'blog-masonry-thumb', false, array( 
							'class' => 	'full-width', 
							'alt' => 	cmsms_title(get_the_ID(), false), 
							'title' => 	cmsms_title(get_the_ID(), false) 
						)) . 
					'</figure>' . 
				'</li>';
			}
			?>
				</ul>
			</div>
			<?php 
			} elseif (sizeof($cmsms_post_images) == 1 && $cmsms_post_images[0] != '') {
				cmsmasters_thumb(get_the_ID(), 'blog-masonry-thumb', false, 'img_' . get_the_ID(), true, true, true, true, $cmsms_post_images[0]);
			} elseif (has_post_thumbnail()) {
				cmsmasters_thumb(get_the_ID(), 'blog-masonry-thumb', false, 'img_' . get_the_ID(), true, true, true, true, false);
			}
		}
		
		echo '<div class="cmsms_post_meta_info">';
		
			cmsmasters_post_date('post');
			
			if ( 
				$cmsms_option[CMSMS_SHORTNAME . '_blog_post_view'] || 
				$cmsms_option[CMSMS_SHORTNAME . '_blog_post_like'] || 
				$cmsms_option[CMSMS_SHORTNAME . '_blog_post_comment'] 
			) {
				
				cmsmasters_post_view('post');
				
				cmsmasters_post_comments('post');
				
				cmsmasters_post_like('post');
				
			}
			
		echo '</div>';
		
		
		if ($cmsms_post_title == 'true') {
			echo '<header class="cmsms_post_header entry-header">' . 
				cmsmasters_post_title_nolink(get_the_ID(), 'h2', false) . 
			'</header>';
		}
		
		
		if ( 
			$cmsms_option[CMSMS_SHORTNAME . '_blog_post_date'] || 
			$cmsms_option[CMSMS_SHORTNAME . '_blog_post_author'] || 
			$cmsms_option[CMSMS_SHORTNAME . '_blog_post_cat'] || 
			$cmsms_option[CMSMS_SHORTNAME . '_blog_post_tag'] || 
			$cmsms_option[CMSMS_SHORTNAME . '_blog_post_like'] || 
			$cmsms_option[CMSMS_SHORTNAME . '_blog_post_comment'] 
		) {
			echo '<div class="cmsms_post_cont_info entry-meta' . ((get_the_content() == '') ? ' no_bdb' : '') . '">';
				
				cmsmasters_post_author('post');
				
				cmsmasters_post_category('post');
				
				cmsmasters_post_tags('post');
				
			echo '</div>';
		}
		
		
		if (get_the_content() != '') {
			echo '<div class="cmsms_post_content entry-content">';
				
				the_content();
				
				
				wp_link_pages(array( 
					'before' => '<div class="subpage_nav" role="navigation">' . '<strong>' . esc_html__('Pages', 'music-band') . ':</strong>', 
					'after' => '</div>', 
					'link_before' => ' [ ', 
					'link_after' => ' ] ' 
				));
				
			echo '<div class="cl"></div>' . 
			'</div>';
		}
	?>
	</div>
</article>
<!--_________________________ Finish Gallery Article _________________________ -->

