<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.0
 * 
 * Posts Slider Audio Post Format Template
 * Created by CMSMasters
 * 
 */


global $cmsms_post_metadata;


$cmsms_metadata = explode(',', $cmsms_post_metadata);

$excerpt = in_array('excerpt', $cmsms_metadata) ? true : false;
$date = in_array('date', $cmsms_metadata) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsms_metadata))) ? true : false;
$author = in_array('author', $cmsms_metadata) ? true : false;
$likes = in_array('likes', $cmsms_metadata) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsms_metadata))) ? true : false;
$views = in_array('views', $cmsms_metadata) ? true : false;
$more = in_array('more', $cmsms_metadata) ? true : false;

?>

<!--_________________________ Start Audio Article _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="cmsms_slider_post_cont">
	<?php 
		if (!post_password_required() && has_post_thumbnail()) {
			echo '<div class="thumb_wrap">';
				
				cmsmasters_thumb(get_the_ID(), 'post-thumbnail', true, false, true, false, true, true, false);
				
				
				if ($date) {
					echo '<div class="cmsms_post_date_wrap">';
						
						$date ? cmsmasters_slider_post_date('post') : '';
						
					echo '</div>';
				}
				
			echo '</div>';
		} else {
			echo '<div class="thumb_wrap no_thumb">' . 
				'<div class="cmsms_post_date_wrap">';
				
					$date ? cmsmasters_slider_post_date('post') : '';
				
				echo '</div>' . 
			'</div>';
		}
		
		
		echo '<div class="cmsms_slider_post_cont_wrap">';
			
			cmsmasters_slider_post_heading(get_the_ID(), 'post', 'h4');
			
			
			if ($author || $categories) {
				echo '<div class="cmsms_post_cont_info entry-meta">';
					
					$author ? cmsmasters_slider_post_author('post') : '';
					
					$categories ? cmsmasters_slider_post_category('post') : '';
					
				echo '</div>';
			}
			
			
			$excerpt ? cmsmasters_slider_post_exc_cont('post') : '';
			
			
			if ($more || $likes || $comments) {
				echo '<footer class="cmsms_slider_post_footer entry-meta">';
					
					$more ? cmsmasters_slider_post_more(get_the_ID()) : '';
					
					if ($likes || $comments) {
						echo '<div class="cmsms_slider_post_meta_info">';
							
							$likes ? cmsmasters_slider_post_like('post') : '';
							
							$comments ? cmsmasters_slider_post_comments('post') : '';
							
							$views ? cmsmasters_slider_post_view('post') : '';
							
						echo '</div>';
					}
					
				echo '</footer>';
			}
			
		echo '</div>';
	?>
	</div>
</article>
<!--_________________________ Finish Audio Article _________________________ -->

