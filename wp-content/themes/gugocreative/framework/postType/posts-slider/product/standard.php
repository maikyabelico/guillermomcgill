<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.4.0
 * 
 * @cmsms_package 	Music Band
 * @cmsms_version 	1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product,
	$cmsms_product_metadata;


$cmsms_metadata = explode(',', $cmsms_product_metadata);

$title = in_array('title', $cmsms_metadata) ? true : false;
$rating = in_array('rating', $cmsms_metadata) ? true : false;
$price = in_array('price', $cmsms_metadata) ? true : false;
$categories = (get_the_terms(get_the_ID(), 'pr-categs') && in_array('categories', $cmsms_metadata)) ? true : false;



// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;
	

if ( ! isset( $classes ) ) 
	$classes = '';



?>
<article <?php post_class( $classes ); ?>>
	<div class="product_outer">
	<?php 
		woocommerce_show_product_loop_sale_flash();
		
		$availability = $product->get_availability();

		if (esc_attr($availability['class']) == 'out-of-stock') {
			echo apply_filters('woocommerce_stock_html', '<span class="' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span>', $availability['availability']);
		}
		
		echo '<div class="product_inner">' . 
			'<div class="cmsms_product_img_wrap">' . 
				'<figure class="cmsms_product_img preloader">' . 
					'<a href="';
						the_permalink();
					echo '">';
					
						woocommerce_template_loop_product_thumbnail();
						
						echo '<span class="cmsms_img_rollover"></span>' . 
						
					'</a>' . 
					
					'<footer class="entry-meta cmsms_product_footer">';
						
							cmsmasters_woocommerce_add_to_cart_button();
							
					echo '</footer>' . 
				'</figure>' . 
			'</div>';
			if ($title) {
				echo '<header class="entry-header cmsms_product_header">' . 
					'<h4 class="entry-title cmsms_product_title">' . 
						'<a href="';
							the_permalink();
						echo '">';
							the_title();
						echo '</a>' . 
					'</h4>' . 
				'</header>';
			}
			
			
			if ($categories) {
				echo $product->get_categories(' , ', '<div class="entry-meta cmsms_product_cat">', '</div>');
			}
			
			echo '<div class="cmsms_product_info">';
			
				if ($rating) {
					cmsmasters_woocommerce_rating('cmsms_theme_icon_star_empty', 'cmsms_theme_icon_star_full');
				}
				
				
				if ($price) {
					woocommerce_template_loop_price();
				}
			echo '</div>' . 
		'</div>' . 
	'</div>' . 
'</article>';

