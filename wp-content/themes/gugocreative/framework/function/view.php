<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.0
 * 
 * Views Operator
 * Changed by CMSMasters
 * 
 */


$parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);

require_once($parse_uri[0] . 'wp-load.php');


$post_ID = $_POST['id'];


$ip = $_SERVER['REMOTE_ADDR'];

$ip_name = str_replace('.', '-', $ip);


if ($post_ID != '') {
	$views = (get_post_meta($post_ID, 'cmsms_views', true) != '') ? get_post_meta($post_ID, 'cmsms_views', true) : '0';
	
	
	$ipPost = new WP_Query(array( 
		'post_type' => 		'cmsms_view', 
		'post_status' => 	'draft', 
		'post_parent' => 	$post_ID, 
		'name' => 			$ip_name 
	));
	
	
	$ipCheck = $ipPost->posts;
	
	
    if (isset($_COOKIE['view-' . $post_ID]) || count($ipCheck) != 0) {
		echo esc_html($views);
	} else {
		$plusView = $views + 1;
		
		
		update_post_meta($post_ID, 'cmsms_views', $plusView);
		
		
		setcookie('view-' . $post_ID, time(), time() + 31536000, '/');
		
		
		wp_insert_post(array( 
			'post_type' => 		'cmsms_view', 
			'post_status' => 	'draft', 
			'post_parent' => 	$post_ID, 
			'post_name' => 		$ip_name, 
			'post_title' => 	$ip 
		));
		
		
		echo esc_html($plusView);
	}
}

