<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * Theme Fonts Rules
 * Created by CMSMasters
 * 
 */


function cmsmasters_theme_fonts() {
	$cmsms_option = cmsmasters_get_global_options();
	
	
	$custom_css = "/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * Theme Fonts Rules
 * Created by CMSMasters
 * 
 */


/***************** Start Theme Font Styles ******************/

	/* Start Content Font */
	body, 
	blockquote footer,
	.archive .portfolio .project .project_outer .project_inner .cmsms_project_content {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_content_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_content_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_style'] . ";
	}
	
	.cmsms_quotes_slider .quote_content, 
	.cmsms_quotes_slider .quote_content a {
		font-size:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] + 2) . "px;
	}
	
	.blog.columns .cmsms_post_content,
	.blog.timeline .cmsms_post_content,
	.portfolio .project .project_outer .project_inner .cmsms_project_content {
		font-size:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] - 1) . "px;
	}
	
	.blog.columns.puzzle .cmsms_post_content,
	.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_content {
		font-size:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] - 4) . "px;
	}
	
	q, 
	#wp-calendar th, 
	#wp-calendar td {
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] . "px;
	}
	
	.cmsms_posts_slider .post .cmsms_slider_post_content { 
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] - 1) . "px;
	}
	
	.gallery .gallery-item .gallery-caption,
	.cmsms_gallery li.cmsms_caption figcaption,
	.wp-caption .wp-caption-text {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] + 1) . "px;
	}
	
	.cmsms_wrap_pagination ul li .cmsms_theme_icon_slide_prev, 
	.cmsms_wrap_pagination ul li .cmsms_theme_icon_slide_next {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] + 14) . "px;
	}
	
	.cmsms_post_filter_but:before, 
	.cmsms_project_filter_but:before {
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] + 2) . "px !important;
	}
	
	q {
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] . "px !important;
	}
	
	.details_item_desc_like,
	.details_item_desc_comments {
		height:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] . "px;
	}
	/* Finish Content Font */


	/* Start Link Font */
	a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_link_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_link_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_link_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_link_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_link_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_link_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_link_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_link_font_text_decoration'] . ";
	}
	
	.pj_ddn .cmsms_project_category,
	.pj_ddn .cmsms_project_category a {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_link_font_font_size'] - 1) . "px;
	}

	a:hover {
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_link_hover_decoration'] . ";
	}
	/* Finish Link Font */


	/* Start Navigation Title Font */
	#navigation > li > a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_text_transform'] . ";
	}
	
	#navigation > li > a > span:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] - 2) . "px;
	}
	
	.footer_inner nav > div > ul > li > a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] - 2) . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_text_transform'] . ";
	}
	
	.header_top_inner nav > div > ul li > a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] - 4) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] - 4) . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_text_transform'] . ";
	}
	
	#navigation > li > a > span > span.nav_subtitle {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] - 5) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] - 4) . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_style'] . ";
		font-weight:700; /* static */
		text-transform:uppercase; /* static */
	}
	
	#navigation > li.menu-item-icon > a > span > span.nav_subtitle,
	#navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li.menu-item-icon > a > span > span.nav_subtitle {
		padding-left:" . ceil(((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] + 3) * 1.4) . "px;
	}
	
	body.rtl #navigation > li.menu-item-icon > a > span > span.nav_subtitle,
	body.rtl #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li.menu-item-icon > a > span > span.nav_subtitle {
		padding-right:" . ceil(((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] + 3) * 1.4) . "px;
		padding-left:0; /* static */
	}
	
	#navigation > li > a[data-tag]:before,
	#navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a[data-tag]:before {
		margin-top:" . round(((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] - ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_line_height'] - 2)) / 2) . "px;
	}
	
	@media only screen and (max-width: 1024px) {
		html #page #header nav #navigation li a {
			font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] - 2) . "px;
			font-weight:400; /* static */
		}
		
		html #page #header nav #navigation > li.menu-item-hide-text > a > span,
		html #page #header nav #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li.menu-item-hide-text > a > span {
			font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] . "px;
			line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] . "px;
		}
		
		html #page #header nav #navigation > li.menu-item-icon > a > span > span.nav_subtitle,
		html #page #header nav #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li.menu-item-icon > a > span > span.nav_subtitle {
			padding-left:" . ceil(((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] + 3) * 1.4) . "px;
		}
	}
	/* Finish Navigation Title Font */


	/* Start Navigation Dropdown Font */
	#navigation ul li a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_text_transform'] . ";
	}
	
	#navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_size'] + 4) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_line_height'] + 4) . "px;
		font-weight:bold; /* static */
	}
	
	#navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a > span > span.nav_subtitle {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_size'] - 3) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] - 3) . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_style'] . ";
		text-transform:uppercase; /* static */
		font-weight:700; /* static */
	}
	
	#navigation li > a[data-tag]:before {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_size'] - 3) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] - 3) . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_text_transform'] . ";
		font-weight:400; /* static */
	}
	
	#navigation ul li a span:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_size'] - 2) . "px;
	}
	
	#navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a > span:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_size'] + 4) . "px;
	}
	
	@media only screen and (max-width: 1024px) {
		html #page #header nav #navigation > li.menu-item-hide-text > a > span > span.nav_subtitle,
		html #page #header nav #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li.menu-item-hide-text > a > span > span.nav_subtitle {
			font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_font_size'] - 2) . "px;
			line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_dropdown_font_line_height'] - 2) . "px;
		}
	}
	
	#page #header .search_bar_wrap input[type=search] {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] + 8) . "px;
	}
	
	#page #header ::-webkit-input-placeholder {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] + 8) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] + 8) . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_style'] . ";
	}
	
	.search_bar_wrap form p.search_field ::-webkit-input-placeholder {
		font-weight:bold; /* static */
		text-transform:uppercase; /* static */
	}
	
	#page #header ::-moz-placeholder {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] + 8) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] + 8) . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_style'] . ";
	}
	
	.search_bar_wrap form p.search_field ::-moz-input-placeholder {
		font-weight:bold; /* static */
		text-transform:uppercase; /* static */
	}
	
	#page #header ::-moz-placeholder {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] + 8) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] + 8) . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_style'] . ";
	}
	
	.search_bar_wrap form p.search_field ::-moz-placeholder {
		font-weight:bold; /* static */
		text-transform:uppercase; /* static */
	}
	
	#page #header :-ms-input-placeholder {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_size'] + 8) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_line_height'] + 8) . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_nav_title_font_font_style'] . ";
	}
	
	.search_bar_wrap form p.search_field :-ms-input-placeholder {
		font-weight:bold; /* static */
		text-transform:uppercase; /* static */
	}
	/* Finish Navigation Dropdown Font */


	/* Start H1 Font */
	h1,
	h1 a,
	#header .logo .title,
	.cmsms_pricing_table .cmsms_currency,
	.cmsms_pricing_table .cmsms_price {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h1_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_text_decoration'] . ";
	}
	
	@media only screen and (max-width: 540px) {
		.headline_outer .headline_inner .headline_text .entry-title {
			font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_size'] - 32) . "px;
			line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] - 32) . "px;
		}
	}
	
	.cmsms_tabs.lpr .ovh small {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h1_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_system_font'] . ";
	}
	
	.cmsms_dropcap {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h1_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_system_font'] . ";
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_text_decoration'] . ";
	}
	
	.cmsms_icon_list_items.cmsms_icon_list_icon_type_number .cmsms_icon_list_item .cmsms_icon_list_icon:before,
	.cmsms_icon_box.box_icon_type_number:before,
	.cmsms_icon_box.cmsms_icon_heading_left.box_icon_type_number .icon_box_heading:before {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h1_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_system_font'] . ";
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_style'] . ";
	}
	
	.cmsms_dropcap.type1 {
		font-size:48px; /* static */
	}
	
	.cmsms_dropcap.type2 {
		font-size:36px; /* static */
	}
	
	.headline_outer .headline_inner .headline_icon:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_size'] - 16) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] + 10) . "px;
	}
	
	.headline_outer .headline_inner.align_center .headline_icon:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] + 4) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] + 35) . "px;
	}
	
	.headline_outer .headline_inner.align_left .headline_icon {
		padding-left:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] + 10) . "px;
	}
	
	.headline_outer .headline_inner.align_right .headline_icon {
		padding-right:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] + 10) . "px;
	}
	
	.headline_outer .headline_inner.align_center .headline_icon {
		padding-top:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] + 30) . "px;
	}
	
	.cmsms_twitter .owl-buttons span:before {
		line-height:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_size'] + 6) / 2) . "px;
	}
	
	.cmsms_twitter .owl-buttons > div, 
	.cmsms_twitter .owl-buttons span, 
	.cmsms_twitter .owl-buttons span:before {
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_size'] + 6) . "px;
		height:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_size'] + 6) / 2) . "px;
	}
	
	.cmsms_twitter .owl-buttons {
		margin-top:-" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_size'] + 6) / 2) . "px;
	}
	/* Finish H1 Font */


	/* Start H2 Font */
	h2,
	h2 a,
	.cmsms_pricing_table .pricing_title,
	.cmsms_counters .cmsms_counter_wrap .cmsms_counter .cmsms_counter_inner .cmsms_counter_counter_wrap > span,
	.cmsms_stats.stats_mode_circles .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner .cmsms_stat_counter_wrap,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .thumb_wrap .cmsms_post_date_wrap .cmsms_post_date .cmsms_year,
	.blog.timeline .post .cmsms_post_info .cmsms_post_date .published span,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .thumb_wrap .cmsms_post_date_wrap .cmsms_post_date .cmsms_day_mon,
	.archive .portfolio .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title,
	.archive .portfolio .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title a,
	.cmsms_search_post_number {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h2_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_decoration'] . ";
	}
	
	.cmsms_sitemap_wrap > h1 {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h2_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_style'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_decoration'] . ";
	}
	
	.error .error_wrap .error_inner .error_subtitle,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .thumb_wrap .cmsms_post_date_wrap .cmsms_post_date .cmsms_year {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] - 6) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] - 6) . "px;
	}
	
	.blog.timeline .post .cmsms_post_info .cmsms_post_date .published .cmsms_day_mon,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .thumb_wrap .cmsms_post_date_wrap .cmsms_post_date .cmsms_day_mon {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] - 24) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] - 24) . "px;
	}
	
	.blog.timeline .post .cmsms_post_info {
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] + 34) . "px;
		height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] + 34) . "px;
	}
	
	.cmsms_posts_slider .post .cmsms_slider_post_cont .thumb_wrap .cmsms_post_date_wrap .published {
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] + 20) . "px;
		height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] + 20) . "px;
	}
	
	.blog.timeline .post.cmsms_timeline_left .cmsms_post_info {
		right:-" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] + 35) / 2) . "px;
	}
	
	.blog.timeline .post.cmsms_timeline_right .cmsms_post_info {
		left:-" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] + 35) / 2) . "px;
	}
	
	.blog.timeline .post.cmsms_timeline_type:before {
		top:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] + 34) / 2) . "px;
	}
	
	.cmsms_stats.stats_mode_circles .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner .cmsms_stat_counter_wrap {
		font-size:24px; /* static */
		line-height:30px; /* static */
	}
	/* Finish H2 Font */


	/* Start H3 Font */
	h3,
	h3 a,
	.cmsms_sitemap_wrap .cmsms_sitemap > li > a,
	.cmsms_quotes .quote_title,
	.profiles.opened-article .profile .cmsms_profile_inner .profile_sidebar .profile_social_icons .profile_social_icons_title,
	.portfolio.opened-article .project .cmsms_project_inner .project_sidebar .project_details .project_details_title,
	.portfolio.opened-article .project .cmsms_project_inner .project_sidebar .share_posts .share_posts_title,
	.portfolio.opened-article .about_author .about_author_title,
	.widget_custom_contact_info_entries .widgettitle {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h3_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_decoration'] . ";
	}
	
	.cmsms_quotes_slider.cmsms_quotes_slider_type_center .owl-buttons span:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] + 22) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] + 12) . "px;
	}
	
	.cmsms_quotes_slider.cmsms_quotes_slider_type_center .owl-buttons > div, 
	.cmsms_quotes_slider.cmsms_quotes_slider_type_center .owl-buttons span, 
	.cmsms_quotes_slider.cmsms_quotes_slider_type_center .owl-buttons span:before {
		width:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] + 20) / 2) . "px;
		height:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] + 20) / 2) . "px;
		line-height:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] + 20) / 2) . "px;
	}
	
	.post_nav > span:before {
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] . "px;
	}
	
	.post_nav > span:before {
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] . "px;
	}
	
	.portfolio .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title, 
	.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_header .cmsms_slider_project_title {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] - 2) . "px;
	}
	
	.post.cmsms_timeline_type .cmsms_post_cont .cmsms_post_title:before {
		height:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] . "px;
		margin-top:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] - (int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size']) / 2) . "px;
	}
	
	#cancel-comment-reply-link {
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] . "px;
	}
	
	.widget .owl-buttons span:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] + 6) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] + 4) . "px;
	}
	
	.widget .owl-buttons div,
	.widget .owl-buttons div span {
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] - 14) . "px;
		height:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] . "px;
	}
	/* Finish H3 Font */


	/* Start H4 Font */
	h4, 
	h4 a, 
	.cmsms_sitemap_wrap .cmsms_sitemap_archive li,
	.cmsms_counters .cmsms_counter_wrap .cmsms_counter .cmsms_counter_inner .cmsms_counter_counter_wrap .cmsms_counter_title,
	.cmsms_stats.stats_mode_bars .cmsms_stat_wrap .cmsms_stat_counter_wrap,
	.cmsms_quotes .quote_title_wrap,
	.cmsms_quotes .quote_title_inner .quote_link,
	.profiles.opened-article .profile .cmsms_profile_inner .profile_sidebar .profile_details .profile_details_item .profile_details_item_desc,
	.profiles.opened-article .profile .cmsms_profile_inner .profile_sidebar .profile_details .profile_details_item .profile_details_item_desc *,
	.portfolio.opened-article .project .cmsms_project_inner .project_sidebar .project_details .project_details_item .project_details_item_desc,
	.portfolio.opened-article .project .cmsms_project_inner .project_sidebar .project_details .project_details_item .project_details_item_desc * {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_decoration'] . ";
	}
	
	.cmsms-form-builder label,
	.cmsms_sitemap_wrap .cmsms_sitemap_category > li > a,
	.cmsms_sitemap_wrap .cmsms_sitemap > li > ul > li > a,
	.cmsms_sitemap_wrap .cmsms_sitemap_archive > li > a,
	.cmsms_img .cmsms_img_caption,
	.cmsms_table tr.cmsms_table_row_header,
	.cmsms_table tr.cmsms_table_row_header a,
	.cmsms_table tr.cmsms_table_row_header th,
	.cmsms_table tr.cmsms_table_row_footer,
	.cmsms_table tr.cmsms_table_row_footer a,
	.cmsms_table tr.cmsms_table_row_footer td,
	.cmsms_tabs .cmsms_tabs_list .cmsms_tabs_list_item > a,
	.cmsms_toggles .cmsms_toggle_wrap .cmsms_toggle_title > a,
	.blog.columns.puzzle .post .puzzle_post_content_wrapper .cmsms_post_header .cmsms_post_title,
	.blog.columns.puzzle .post .puzzle_post_content_wrapper .cmsms_post_header .cmsms_post_title a,
	.portfolio .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title,
	.portfolio .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title a,
	.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_header .cmsms_slider_project_title,
	.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_header .cmsms_slider_project_title a,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .cmsms_slider_post_header .cmsms_slider_post_title,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .cmsms_slider_post_header .cmsms_slider_post_title a,
	.about_author .about_author_inner .ovh h3,
	.about_author .about_author_inner .ovh h3 a,
	.post_comments .commentlist .comment-body .comment-content h3,
	.post_comments .commentlist .comment-body .comment-content h3 a,
	.profiles.opened-article .profile .cmsms_profile_inner .cmsms_profile_header .cmsms_profile_subtitle,
	.profiles.opened-article .profile .cmsms_profile_inner .profile_sidebar .profile_details .profile_details_item .profile_details_item_title,
	.portfolio.opened-article .project .cmsms_project_inner .cmsms_project_header .cmsms_project_subtitle,
	.portfolio.opened-article .project .cmsms_project_inner .project_sidebar .project_details .project_details_item .project_details_item_title,
	.cmsms_wrap_pagination ul li .page-numbers,
	.widget_custom_popular_projects_entries .pj_ddn .entry-title,
	.widget_custom_popular_projects_entries .pj_ddn .entry-title a,
	.widget_custom_latest_projects_entries .pj_ddn .entry-title, 
	.widget_custom_latest_projects_entries .pj_ddn .entry-title a,
	.widget.widget_recent_entries ul li a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_decoration'] . ";
		font-weight:bold; /* static */
	}
	
	.cmsms_sitemap_wrap .cmsms_sitemap > li.menu-item-type-custom > ul > li > a,
	.cmsms_sitemap_wrap .cmsms_sitemap > li.menu-item-object-custom > ul > li > a,
	.post_nav > span,
	.post_nav > span a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_style'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_decoration'] . ";
		font-weight:bold; /* static */
		text-transform:uppercase; /* static */
	}
	
	.color_2,
	.required {
		font-size:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] - 4) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] - 6) . "px;
	}
	
	.cmsms_breadcrumbs .cmsms_breadcrumbs_inner * {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] - 3) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] - 4) . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_decoration'] . ";
	}
	
	.cmsms_stats.stats_mode_bars.stats_type_vertical .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner:before {
		font-size:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] + 2) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] + 10) . "px;
	}
	
	.cmsms_quotes .quote_content,
	.cmsms_quotes .quote_content a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_weight'] . ";
	}
	
	.portfolio .project .project_outer .project_inner .cmsms_project_footer a.cmsmsLike,
	.portfolio .project .project_outer .project_inner .cmsms_project_footer a.cmsms_post_comments {
		font-size:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] - 6) . "px;
	}
	
	.post_nav > span.cmsms_next_post:before,
	.post_nav > span.cmsms_prev_post:before {
		font-size:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] + 22) . "px;
		line-height:" .  ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] + 22) . "px;
	}
	
	.pj_ddn .cmsmsLike, 
	.pj_ddn .cmsmsLike:before {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] + 2) . "px !important;
	}
	
	.post.cmsms_masonry_type .cmsms_post_cont .cmsms_post_title:before, 
	.post.cmsms_puzzle_type .cmsms_post_cont .cmsms_post_title:before {
		height:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		margin-top:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] - (int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size']) / 2) . "px;
	}
	
	.cmsms_single_slider .cmsms_single_slider_inner .cmsms_single_slider_item .cmsms_single_slider_item_outer .cmsms_single_slider_item_inner .cmsms_single_slider_title,
	.cmsms_single_slider .cmsms_single_slider_inner .cmsms_single_slider_item .cmsms_single_slider_item_outer .cmsms_single_slider_item_inner .cmsms_single_slider_title a {
		font-weight:bold; /* static */
	}
	/* Finish H4 Font */


	/* Start H5 Font */
	h5,
	h5 a,
	.share_posts .share_posts_inner > a,
	.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_list li > a.button,
	.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_list li > a.button,
	.widget ul li > a,
	.cmsms_tabs.lpr .cmsms_tabs_list .cmsms_tabs_list_item > a,
	table caption, 
	table th, 
	table th a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_decoration'] . ";
	}
	
	.cmsms_tabs.lpr .cmsms_tabs_list .cmsms_tabs_list_item > a {
		font-weight:bold; /* static */
	}
	
	.cmsms_sitemap_wrap .cmsms_sitemap > li > ul > li > ul a,
	.cmsms_sitemap_wrap .cmsms_sitemap_category > li > ul a,
	.cmsms_stats .cmsms_stat_wrap .cmsms_stat_title {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_decoration'] . ";
		font-weight:bold; /* static */
	}
	
	.cmsms_table tbody tr td {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] - 1) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] . "px;
	}
	
	.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_list li > a.button,
	.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_list li > a.button {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] - 1) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] - 2) . "px;
	}
	
	.cmsms_stats .cmsms_stat_wrap .cmsms_stat_title,
	.cmsms_stats.stats_mode_bars.stats_type_horizontal .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] + 2) . "px;
	}
	
	.cmsms_toggles .cmsms_toggles_filter,
	.cmsms_toggles .cmsms_toggles_filter *,
	.cmsms_twitter .cmsms_twitter_item_content,
	.cmsms_twitter .cmsms_twitter_item_content a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] - 1) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_decoration'] . ";
		font-weight:normal; /* static */
	}
	
	.cmsms_twitter .cmsms_twitter_item_content,
	.cmsms_twitter .cmsms_twitter_item_content a {
		font-style:italic; /* static */
	}
	/* Finish H5 Font */


	/* Start H6 Font */
	h6,
	h6 a,
	.cmsms_twitter .published,
	.cmsms_post_wrap_info,
	.cmsms_post_wrap_info a,
	.blog.columns.puzzle .post .puzzle_post_content_wrapper .cmsms_post_read_more,
	.cmsms-jplayer .jp-playlist a.jp-playlist-item,
	.cmsms-jplayer span.jp-artist,
	.cmsms-jplayer span.jp-title,
	.cmsms_pricing_table .cmsms_period,
	.cmsms_pricing_table .cmsms_coins,
	.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_cat,
	.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_cat a,
	.cmsms_search_post .cmsms_search_post_number_wrap .cmsms_post_type_label,
	.cmsms_tabs .cmsms_tabs_wrap .cmsms_tab .ovh a,
	.cmsms_tabs .cmsms_tabs_wrap .cmsms_tab.tab_comments > ul > li > a,
	.cmsms_tabs .cmsms_tabs_wrap .cmsms_tab.tab_comments > ul > li > span,
	.copyright,
	.widget_custom_contact_info_entries *,
	input[type=submit], 
	input[type=button], 
	button, 
	dl dt, 
	table.shop_attributes th, 
	fieldset legend,
	.cmsms_post_read_more, 
	blockquote footer,
	.comment-edit-link, 
	.comment-reply-link, 
	#cancel-comment-reply-link {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
	}
	
	.cmsms-jplayer span.jp-artist,
	.cmsms-jplayer span.jp-title {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] + 1) . "px;
	}
	
	.cmsms-jplayer .jp-playlist span.jp-free-media,
	.cmsms-jplayer .jp-playlist span.jp-free-media a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] - 2) . "px;
	}
	
	.cmsms_pricing_table .feature_list li,
	.cmsms_pricing_table .feature_list li *,
	.cmsms_project_cont_info,
	.cmsms_project_cont_info a,
	.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_cont_info .cmsms_slider_project_category,
	.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_cont_info .cmsms_slider_project_category a,
	.about_author .about_author_inner .ovh p,
	.post_comments .commentlist .comment-body .comment-content,
	.widget_recent_entries ul li .post-date,
	.widget .tweet_list *,
	.wpcf7 form.wpcf7-form span.wpcf7-list-item .wpcf7-list-item-label, 
	.cmsms-form-builder .check_parent label {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		font-weight:normal; /* static */
	}
	
	.cmsms_twitter .published,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .cmsms_post_cont_info,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .cmsms_post_cont_info a,
	.widget_recent_entries ul li .post-date,
	.widget .tweet_list *,
	.copyright,
	.post.cmsms_masonry_type .cmsms_post_header .cmsms_post_subtitle,
	.post.cmsms_timeline_type .cmsms_post_header .cmsms_post_subtitle,
	.post.cmsms_puzzle_type .cmsms_post_header .cmsms_post_subtitle {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] - 1) . "px;
	}
	
	.post_comments .commentlist .comment-body .alignleft .comment-reply-link {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] + 1) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_style'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
		text-transform:uppercase; /* static */
	}
	
	.blog.columns.puzzle .post .puzzle_post_content_wrapper .cmsms_post_read_more,
	.cmsms_posts_slider .post .cmsms_slider_post_cont .cmsms_slider_post_footer .cmsms_slider_post_meta_info > * {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] - 2) . "px;
	}
	
	.cmsms_post_date,
	.cmsms_post_comments,
	.cmsmsLike, 
	.cmsmsView.cmsms_theme_icon_view,
	.cmsms_post_cont_info,
	.cmsms_post_cont_info a,
	.archive .cmsms_project_cont_info,
	.archive .cmsms_project_cont_info a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] + 1) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
		font-weight:normal;
	}
	
	.cmsmsLike:before,
	.cmsms_post_comments:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] + 1) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
	}
	
	.cmsmsView.cmsms_theme_icon_view:before,
	.post .cmsms_post_meta_info > span.cmsmsView.cmsms_theme_icon_view:before,
	.tribe_events .cmsms_post_meta_info > span.cmsmsView.cmsms_theme_icon_view:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] + 3) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
	}
	
	.opened-article .cmsms_post_date {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] + 2) . "px;
	}
	
	.blog.columns.puzzle .post .cmsms_post_cont .cmsms_post_comments, 
	.blog.columns.puzzle .post .cmsms_post_cont .cmsmsLike {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] - 2) . "px;
	}
	
	.cmsms_search_post .cmsms_search_post_number_wrap .cmsms_post_type_label {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] - 2) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
	}
	
	.cmsms_post_cont_info a,
	.cmsms_post_wrap_info a,
	.archive .cmsms_project_cont_info,
	.archive .cmsms_project_cont_info a {
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_weight'] . ";
	}
	
	.cmsms_post_meta_info,
	.cmsms_slider_post_meta_info {
		height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
	}
	
	.contact_widget_name.cmsms_theme_icon_person:before, 
	.contact_widget_email.cmsms_theme_icon_user_mail:before, 
	.contact_widget_phone.cmsms_theme_icon_user_phone:before, 
	.adress_wrap.cmsms_theme_icon_user_address:before {
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px !important;
	}
	
	.post .cmsms_slider_post_header .cmsms_slider_post_subtitle,
	.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_cat,
	.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_cat a {
		font-weight:normal; /* static */
	}
	
	.cmsms_pricing_table .cmsms_period,
	.cmsms_pricing_table .cmsms_coins,
	.opened-article .cmsms_post_header h2,
	.opened-article > aside > h3,
	.opened-article .comment-respond > h3,
	.profiles.opened-article .profile .cmsms_profile_inner .profile_sidebar .profile_details .profile_details_title,
	.portfolio.opened-article .about_author .about_author_title, 
	.cmsms_search_post .cmsms_search_post_number_wrap .cmsms_post_type_label,
	.post .cmsms_post_header .cmsms_post_subtitle {
		text-transform:uppercase; /* static */
	}
	/* Finish H6 Font */


	/* Start Button Font */
	.cmsms_button,
	.cmsms_post_read_more,
	form .submit,
	.cmsms_search .cmsms_search_post .cmsms_search_post_cont .cmsms_post_cont_info .cmsms_post_read_more,
	.widget_wysija input[type=submit] {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_button_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_button_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_text_transform'] . ";
	}
	
	.button {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_button_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_button_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_style'] . ";
	}
	
	.gform_wrapper .gform_footer input.button, 
	.gform_wrapper .gform_footer input[type=submit] {
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_size'] . "px !important;
	}
	
	.cmsms_posts_slider .cmsms_slider_post_header .cmsms_slider_post_subtitle {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_size'] + 4) . "px;
	}
	
	.cmsms_search .cmsms_search_post .cmsms_search_post_cont .cmsms_post_cont_info * {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] + 14) . "px;
	}
	
	.cmsms_search .cmsms_search_post .cmsms_search_post_cont .cmsms_post_cont_info .cmsms_post_meta_info {
		height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] + 14) . "px;
	}
	
	.widget_wysija input[type=submit] {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] - 2) . "px;
	}
	
	.cmsms_button.cmsms_but_icon_dark_bg, 
	.cmsms_button.cmsms_but_icon_light_bg, 
	.cmsms_button.cmsms_but_icon_divider, 
	.cmsms_button.cmsms_but_icon_inverse {
		padding-left:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] + 20) . "px;
	}
	
	.cmsms_button.cmsms_but_icon_dark_bg:before, 
	.cmsms_button.cmsms_but_icon_light_bg:before, 
	.cmsms_button.cmsms_but_icon_divider:before, 
	.cmsms_button.cmsms_but_icon_inverse:before, 
	.cmsms_button.cmsms_but_icon_dark_bg:after, 
	.cmsms_button.cmsms_but_icon_light_bg:after, 
	.cmsms_button.cmsms_but_icon_divider:after, 
	.cmsms_button.cmsms_but_icon_inverse:after {
		width:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] . "px;
	}
	/* Finish Button Font */


	/* Start Small Text Font */
	small,
	.meta_wrap,
	.meta_wrap a,
	form .formError .formErrorContent {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_small_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_small_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_text_transform'] . ";
	}

	#navigation > li.menu-item-mega > div.menu-item-mega-container ul ul li.menu-item-mega-description span.menu-item-mega-description-container {
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_line_height'] . "px;
	}
	
	.cmsms_posts_slider .cmsms_slider_post_cont_info,
	.cmsms_posts_slider .cmsms_slider_post_cont_info a {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] + 1) . "px;
	}
	
	.cmsms_tabs.lpr .ovh small {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] + 2) . "px;
		font-weight:normal; /* static */
	}
	
	.gform_wrapper .description, 
	.gform_wrapper .gfield_description, 
	.gform_wrapper .gsection_description, 
	.gform_wrapper .instruction {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_small_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_small_font_system_font'] . " !important;
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] . "px !important;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_line_height'] . "px !important;
	}
	
	.meta_wrap > div[class^=\"cmsms-icon-\"]:before,
	.meta_wrap > p[class^=\"cmsms-icon-\"]:before,
	.meta_wrap > span[class^=\"cmsms-icon-\"]:before,
	.meta_wrap > strong[class^=\"cmsms-icon-\"]:before,
	.meta_wrap > div[class*=\" cmsms-icon-\"]:before,
	.meta_wrap > p[class*=\" cmsms-icon-\"]:before,
	.meta_wrap > span[class*=\" cmsms-icon-\"]:before,
	.meta_wrap > strong[class*=\" cmsms-icon-\"]:before, 
	.meta_wrap > div[class^=\"cmsms_theme_icon_\"]:before,
	.meta_wrap > p[class^=\"cmsms_theme_icon_\"]:before,
	.meta_wrap > span[class^=\"cmsms_theme_icon_\"]:before,
	.meta_wrap > strong[class^=\"cmsms_theme_icon_\"]:before,
	.meta_wrap > div[class*=\" cmsms_theme_icon_\"]:before,
	.meta_wrap > p[class*=\" cmsms_theme_icon_\"]:before,
	.meta_wrap > span[class*=\" cmsms_theme_icon_\"]:before,
	.meta_wrap > strong[class*=\" cmsms_theme_icon_\"]:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_small_font_line_height'] - 2) . "px;
	}
	
	@media only screen and (max-width: 1024px) {
		html #page #header .header_top .header_top_outer .header_top_inner .header_top_right .nav_wrap nav #top_line_nav > li > a {
			font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] + 1) . "px;
			font-weight:700; /* static */
		}
	}
	/* Finish Small Text Font */


	/* Start Text Fields Font */
	input[type=text],
	input[type=email],
	input[type=password],
	input[type=number],
	input[type=url],
	input[type=tel],
	input[type=search],
	input[type=tel],
	textarea,
	select,
	option, 
	code {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_input_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_input_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_input_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_input_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_input_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_input_font_font_style'] . ";
	}
	
	.gform_wrapper input[type=text], 
	.gform_wrapper input[type=url], 
	.gform_wrapper input[type=email], 
	.gform_wrapper input[type=tel], 
	.gform_wrapper input[type=number], 
	.gform_wrapper input[type=password], 
	.gform_wrapper textarea, 
	.gform_wrapper select {
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_input_font_font_size'] . "px !important;
	}
	/* Finish Text Fields Font */


	/* Start Blockquote Font */
	blockquote {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_quote_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_quote_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_quote_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_quote_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_quote_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_quote_font_font_style'] . ";
	}
	
	q {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_quote_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_quote_font_system_font'] . ";
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_quote_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_quote_font_font_style'] . ";
	}
	/* Finish Blockquote Font */

/***************** Finish Theme Font Styles ******************/


";


if (CMSMS_WOOCOMMERCE) {

	$custom_css .= "
/***************** Start WooCommerce Font Styles ******************/

	/* Start Content Font */
	ul.order_details.bacs_details li strong,
	.shop_table.order_details td.product-name, 
	.shop_table.order_details td.product-name * {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_content_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_content_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_style'] . ";
	}
	
	.product .product_outer .product_inner .cmsms_product_footer,
	.product .product_outer .product_inner .cmsms_product_footer > a {
		height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] + 12) . "px;
	}
	/* Finish Content Font */
	
	
	/* Start Link Font */
	/* Finish Link Font */
	
	
	/* Start H1 Font */
	/* Finish H1 Font */
	
	
	/* Start H2 Font */
	.cmsms_single_product .cmsms_product_right_column .product_title,
	.cmsms_single_product .cmsms_product_right_column .price ins,
	.cmsms_single_product .cmsms_product_right_column .price ins .amount {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h2_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_decoration'] . ";
	}
	
	.cmsms_single_product .cmsms_product_right_column .price .amount {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h2_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_decoration'] . ";
		font-weight:normal; /* static */
	}
	
	.cmsms_single_product .cmsms_product_right_column .price ins,
	.cmsms_single_product .cmsms_product_right_column .price ins .amount {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] - 8) . "px;
	}
	/* Finish H2 Font */
	
	
	/* Start H3 Font */
	.product .product_outer .product_inner .cmsms_product_info .price ins,
	.product .product_outer .product_inner .cmsms_product_info .price ins .amount,
	.cart_totals h4,
	.cart_totals table tr.order-total,
	.cart_totals table tr.order-total th,
	.product .related.products h2,
	.woocommerce-shipping-fields > h4,
	.checkout #order_review table tr.order-total td,
	.checkout #order_review table tr.order-total td *,
	.checkout #order_review table tr.order-total th,
	.order_details_title,
	.customer_details_title h4 {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h3_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_decoration'] . ";
	}
	
	.product .product_outer .product_inner .cmsms_product_info .price .amount,
	.cmsms_single_product .cmsms_product_right_column .price del .amount {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h3_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_decoration'] . ";
		font-weight:normal; /* static */
	}
	/* Finish H3 Font */
	
	
	/* Start H4 Font */
	.product .product_outer .product_inner .cmsms_product_info .price del .amount,
	.cmsms_single_product .cmsms_product_right_column .cart .quantity .qty,
	.cmsms_single_product .cmsms_woo_tabs h2,
	.cmsms_single_product .cmsms_woo_tabs .shop_attributes td,
	.shop_table td.product-price,
	.shop_table td.product-subtotal,
	.shop_table td.product-quantity .quantity input,
	.cart_totals table tr.cart-subtotal td,
	.woocommerce-message,
	.woocommerce-info,
	.woocommerce-error,
	.checkout #order_review .shop_table th, 
	.checkout #order_review .shop_table td, 
	.checkout #order_review .shop_table th *, 
	.checkout #order_review .shop_table td *,
	#order_review #payment .payment_methods li label,
	.shop_table.order_details th,
	.shop_table.order_details td,
	.shop_table.order_details th *,
	.shop_table.order_details td *,
	.shop_table.customer_details td,
	.shop_table.customer_details td *,
	.shop_table.shop_table_responsive tr td,
	.shop_table.shop_table_responsive tr td *,
	.shop_table.shop_table_responsive tr th,
	.shop_table.shop_table_responsive tr th *,
	.widget_shopping_cart_content .cart_list li .quantity,
	.widget_shopping_cart_content .total .amount,
	.product_list_widget li .amount {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_decoration'] . ";
	}
	
	.product .product_outer .product_inner .cmsms_product_header .cmsms_product_title,
	.product .product_outer .product_inner .cmsms_product_header .cmsms_product_title a,
	.cmsms_single_product .cmsms_product_right_column .product_meta a,
	.cmsms_single_product .cmsms_woo_tabs .shop_attributes th,
	.cmsms_single_product .cmsms_woo_tabs #reviews #comments .commentlist .comment .comment_container .comment-text .meta h5,
	.shop_table thead th,
	.shop_table td.product-name,
	.shop_table td.product-name a,
	.cart_totals table tr.cart-subtotal th,
	.cart_totals table tr.shipping th,
	.shipping-calculator-button-wrap .shipping-calculator-button,
	.woocommerce-message a, 
	.woocommerce-info a, 
	.woocommerce-error a,
	.woocommerce-billing-fields label,
	.woocommerce-shipping-fields label,
	.checkout #order_review table tr th.product-name,
	.checkout #order_review table tr th.product-total,
	.checkout #order_review table tr.cart-subtotal th,
	.checkout #order_review table tr.cart-subtotal td,
	.checkout #order_review table tr.cart-subtotal td *,
	ul.order_details li > span,
	.shop_table.order_details thead tr:first-child th,
	.shop_table.order_details thead tr:first-child td,
	.shop_table.order_details tfoot tr:first-child th,
	.shop_table.order_details tfoot tr:first-child td, 
	.shop_table.order_details tfoot tr:first-child td *,
	.shop_table.order_details tfoot tr:last-child th,
	.shop_table.order_details tfoot tr:last-child td,
	.shop_table.order_details tfoot tr:last-child td *,
	.shop_table.customer_details th,
	.track_order label,
	.widget_shopping_cart_content .cart_list li .quantity span,
	.widget_shopping_cart_content .total strong,
	ul.order_details.bacs_details li {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_decoration'] . ";
		font-weight:bold; /* static */
	}
	
	.onsale {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] + 32) . "px;
		text-transform:uppercase; /* static */
		font-weight:bold; /* static */
	}
	
	.onsale {
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] + 32) . "px;
		height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] + 32) . "px;
	}
	
	.cmsms_single_product .cmsms_product_left_column .onsale {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] + 42) . "px;
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] + 42) . "px;
		height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] + 42) . "px;
	}
	
	.form-row label .required,
	.cmsms_single_product .cmsms_product_right_column .cart .quantity .qty {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] - 6) . "px;
	}
	/* Finish H4 Font */
	
	
	/* Start H5 Font */
	.widget_shopping_cart_content .cart_list li a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_decoration'] . ";
	}
	
	.widget_shopping_cart_content .total .amount {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_system_font'] . ";
	}
	
	.widget_shopping_cart_content .cart_list li a.remove {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] + 6) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] + 2) . "px;
	}
	
	#order_review #payment .payment_methods .payment_box {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] - 1) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_decoration'] . ";
		font-weight:normal; /* static */
	}
	/* Finish H5 Font */
	
	
	/* Start H6 Font */
	.shop_table.shop_table_responsive tr .button,
	.widget_price_filter .price_slider_wrapper .price_slider_amount .price_label,
	.product_list_widget li del .amount,
	.cart_totals table tr.cart-subtotal {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
	}
	
	.out-of-stock {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] - 6) . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_style'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
		text-transform:uppercase; /* static */
	}
	
	.out-of-stock {
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] + 40) . "px;
		height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] + 40) . "px;
		padding-top:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] - 4) . "px;
		padding-bottom:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] - 4) . "px;
	}
	
	.product .product_outer .product_inner .cmsms_product_cat,
	.product .product_outer .product_inner .cmsms_product_cat a,
	.cmsms_single_product .cmsms_woo_tabs #reviews #comments .commentlist .comment .comment_container .comment-text .meta,
	.cart_totals table tr.shipping td,
	.woocommerce-shipping-fields #ship-to-different-address label {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
		font-weight:normal; /* static */
	}
	
	.cmsms_single_product .cmsms_woo_tabs #reviews #comments .commentlist .comment .comment_container .comment-text .description {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
		font-style:italic; /* static */
		font-weight:normal; /* static */
	}
	/* Finish H6 Font */
	
	
	/* Start Button Font */
	#order_review #payment .place-order .button {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_button_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_button_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_text_transform'] . ";
	}
	
	.cmsms_added_product_info .cmsms_added_product_info_text, 
	.product_list_widget li > a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_button_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_button_font_system_font'] . ";
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_text_transform'] . ";
	}
	
	.product_list_widget li del .amount {
		font-weight:normal; /* static */
	}
	
	.product_list_widget li del + ins .amount {
		font-weight:bold; /* static */
	}
	
	.cart_totals table tr.order-total th,
	.shipping-calculator-button-wrap a.shipping-calculator-button,
	.woocommerce-billing-fields > h4,
	.woocommerce-shipping-fields > h4,
	.checkout #order_review table tr.order-total td,
	.checkout #order_review table tr.order-total td *,
	.checkout #order_review table tr.order-total th,
	ul.order_details li > span,
	.order_details_title,
	.customer_details_title h4 {
		text-transform:uppercase; /* static */
	}
	/* Finish Button Font */

/***************** Finish WooCommerce Font Styles ******************/


";

}


if (CMSMS_EVENTS_CALENDAR) {

	$custom_css .= "
/***************** Start Events Font Styles ******************/

	/* Start Content Font */
	.widget .vcalendar .vevent .tribe-events-list-widget-content-wrap .cmsms_widget_event_info *,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-grid-body .tribe-week-grid-hours, 
	#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .time-details,
	.widget .vcalendar .vevent .cmsms_widget_event_venue_info_loc, 
	.widget .vcalendar .vevent .cmsms_widget_event_venue_info_loc *, 
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td * {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_content_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_content_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_style'] . ";
	}
	
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"],
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"] a {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] - 4) . "px;
	}
	
	.widget .vcalendar .vevent .tribe-events-list-widget-content-wrap .cmsms_widget_event_info * {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] - 8) . "px;
	}
	
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-list-event-description,
	#tribe-events-content.tribe-events-photo #tribe-events-photo-events .tribe-events-photo-event .tribe-events-photo-event-wrap .tribe-events-event-details .tribe-events-list-photo-description,
	#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .time-details,
	.widget .vcalendar .vevent .cmsms_widget_event_venue_info_loc, 
	.widget .vcalendar .vevent .cmsms_widget_event_venue_info_loc * {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] - 1) . "px;
	}
	
	.widget .vcalendar .vevent .tribe-events-list-widget-content-wrap .cmsms_widget_event_info *,
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td * {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_content_font_font_size'] - 2) . "px;
	}
	
	.tribe_events .cmsms_post_meta_info {
		height:" . $cmsms_option[CMSMS_SHORTNAME . '_content_font_line_height'] . "px;
	}
	/* Finish Content Font */
	
	
	/* Start Link Font */
	/* Finish Link Font */
	
	
	/* Start H1 Font */
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-number,
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-colon {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h1_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_text_decoration'] . ";
	}
	
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-number, 
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-colon {
		font-size:36px; /* static */
		line-height:40px; /* static */
	}
	
	#tribe-events-content .cmsms_event_date:before {
		width:" . $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] . "px;
		margin-left:-" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] / 2) . "px;
	}
	/* Finish H1 Font */
	
	
	/* Start H2 Font */
	#tribe-events-content .cmsms_event_day,
	.tribe-events-week #tribe-mobile-container .tribe-mobile-day .tribe-mobile-day-date,
	#tribe-mobile-container .tribe-mobile-day .tribe-mobile-day-heading, 
	#tribe-mobile-container .tribe-mobile-day .tribe-mobile-day-date {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h2_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_text_decoration'] . ";
	}
	
	.tribe-events-sub-nav li a {
		width:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] - 14) . "px;
		height:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] . "px;
	}
	
	.tribe-events-sub-nav li a:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] - 12) . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] . "px;
	}
	
	.vcalendar .vevent .cmsms_event_day {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h2_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_system_font'] . ";
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_weight'] . ";
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] - 10) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] - 10) . "px;
	}
	
	.tribe-events-list-widget-events .cmsms_event_day {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] - 4) . "px;
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] - 4) . "px;
	}
	
	.tribe_events .cmsms_post_cont .cmsms_post_title:before {
		height:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size'] . "px;
		margin-top:" . (((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] - (int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_font_size']) / 2) . "px;
	}
	
	.tribe-events-list-widget-events .cmsms_event_date:before, 
	.vcalendar .vevent .cmsms_event_date:before {
		width:" . $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] . "px;
		margin-left:-" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h2_font_line_height'] / 2) . "px;
	}
	/* Finish H2 Font */
	
	
	/* Start H3 Font */
	#tribe-events-content.tribe-events-single .cmsms_single_event_meta .tribe-events-meta-group .tribe-events-single-section-title,
	.tribe-events-adv-list-widget .vcalendar li .tribe-events-list-widget-content-wrap .entry-title,
	.tribe-events-adv-list-widget .vcalendar li .tribe-events-list-widget-content-wrap .entry-title a,
	.widget .vcalendar .vevent .entry-title,
	.widget .vcalendar .vevent .entry-title a,
	#tribe-events-content.tribe-events-list .tribe-events-list-separator-month,
	.tribe-events-week #tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .summary,
	.tribe-events-week #tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .summary a,
	#tribe-events-content.tribe-events-day .tribe-events-day-time-slot > h5 {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h3_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_decoration'] . ";
	}
	
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .cmsms_events_list_event_header .tribe-events-event-cost {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h3_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_size'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h3_font_text_decoration'] . ";
		font-weight:normal; /* static */
	}
	/* Finish H3 Font */
	
	
	/* Start H4 Font */
	#tribe-events-content.tribe-events-single .cmsms_single_event_meta .tribe-events-meta-group .cmsms_event_meta_info .cmsms_event_meta_info_item {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_decoration'] . ";
	}
	
	#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-date-filter #tribe-bar-dates label,
	#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-search-filter label,
	#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-geoloc-filter label,
	#tribe-events-content.tribe-events-month table.tribe-events-calendar thead th,
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"],
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"] a,
	.tribe-events-tooltip .entry-title,
	.tribe-events-tooltip .entry-title a,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-header .tribe-grid-content-wrap .column,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-header .tribe-grid-content-wrap .column *,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-event > div:first-child > .entry-title,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-event > div:first-child > .entry-title a,
	#tribe-events-content.tribe-events-single .cmsms_single_event_meta .tribe-events-meta-group .cmsms_event_meta_info .cmsms_event_meta_info_item .cmsms_event_meta_info_item_title,
	ul.tribe-related-events > li .tribe-related-event-info .tribe-related-events-title,
	ul.tribe-related-events > li .tribe-related-event-info .tribe-related-events-title a,
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .tribe-mini-calendar-nav div,
	.widget.tribe-events-venue-widget .vcalendar .vevent .entry-title,
	.widget.tribe-events-venue-widget .vcalendar .vevent .entry-title a,
	.widget .tribe-events-list-widget-events .tribe-events-list-widget-content-wrap h3,
	.widget .tribe-events-list-widget-events .tribe-events-list-widget-content-wrap h3 a,
	.tribe-events-countdown-widget .tribe-countdown-text a,
	.tribe-events-venue-widget .tribe-venue-widget-wrapper .tribe-venue-widget-venue .tribe-venue-widget-venue-name, 
	.tribe-events-venue-widget .tribe-venue-widget-wrapper .tribe-venue-widget-venue .tribe-venue-widget-venue-name a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h4_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_text_decoration'] . ";
		font-weight:bold; /* static */
	}
	
	#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-submit {
		padding-top:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] / 2 + 1) . "px;
		padding-bottom:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_line_height'] / 2 + 1) . "px;
	}
	
	#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_left .tribe-events-schedule > div:before, 
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta > div:before, 
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta > div > div:before, 
	#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-back:before, 
	.tribe-events-venue .cmsms_events_venue_header .cmsms_events_venue_header_right a:before, 
	.tribe-events-venue-widget .tribe-venue-widget-wrapper .cmsms_widget_event_info > div:before,
	.widget .vcalendar .vevent .cmsms_widget_event_info > div:before, 
	.widget .vcalendar .vevent .cmsms_widget_event_venue_info_loc > div:before, 
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info .duration:before, 
	#tribe-events-content.tribe-events-photo #tribe-events-photo-events .tribe-events-photo-event .tribe-events-photo-event-wrap .tribe-events-event-details .tribe-events-event-meta .time-details:before {
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] . "px;
	}
	
	.widget .vcalendar .vevent .cmsms_widget_event_info > div:before, 
	.widget .vcalendar .vevent .cmsms_widget_event_venue_info_loc > div:before, 
	.tribe-events-venue-widget .tribe-venue-widget-wrapper .cmsms_widget_event_info > div:before, 
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info .duration:before {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_size'] + 2) . "px;
	}
	
	.tribe-events-venue-widget .tribe-venue-widget-wrapper .entry-title a {
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h4_font_font_weight'] . ";
	}
	/* Finish H4 Font */
	
	
	/* Start H5 Font */
	#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_left .tribe-events-schedule,
	#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-back a,
	#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-cal-links a,
	.tribe-events-list-widget-events .cmsms_event_month, 
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info h2, 
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info h2 a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h5_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h5_font_text_decoration'] . ";
	}
	/* Finish H5 Font */
	
	
	/* Start H6 Font */
	#tribe-events-content .cmsms_event_month,
	.recurringinfo a,
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta .time-details a,
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta .tribe-events-venue-details a,
	#tribe-events-content.tribe-events-photo #tribe-events-photo-events .tribe-events-photo-event .tribe-events-photo-event-wrap .tribe-events-event-details .tribe-events-event-meta .time-details a,
	.tribe-events-tooltip .tribe-events-event-body .duration,
	ul.tribe-related-events > li .tribe-related-event-info a,
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar th.tribe-mini-calendar-dayofweek,
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td,
	.vcalendar .vevent .cmsms_event_month,
	#tribe-events-bar #tribe-bar-views ul.tribe-bar-views-list li.tribe-bar-views-option a, 
	#tribe-events-footer > a, 
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td .tribe-events-viewmore, 
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td .tribe-events-viewmore a,
	#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .tribe-events-read-more, 
	.tribe-events-viewmore a {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
	}
	
	#tribe-events-content .cmsms_event_month {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] - 1) . "px;
	}
	
	.recurringinfo,
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta .time-details,
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta .tribe-events-venue-details,
	#tribe-events-content.tribe-events-photo #tribe-events-photo-events .tribe-events-photo-event .tribe-events-photo-event-wrap .tribe-events-event-details .tribe-events-event-meta .time-details,
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-event-\"] .tribe-events-month-event-title,
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-event-\"] .tribe-events-month-event-title a,
	.tribe-events-tooltip .tribe-events-event-body .description,
	ul.tribe-related-events > li .tribe-related-event-info,
	.widget .vcalendar .vevent .cmsms_widget_event_info * {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_line_height'] . "px;
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_text_decoration'] . ";
		font-weight:normal; /* static */
	}
	
	#tribe-events-bar #tribe-bar-views ul.tribe-bar-views-list li.tribe-bar-views-option a,
	.vcalendar .vevent .cmsms_event_month,
	#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-back a:before, 
	.tribe-events-organizer .cmsms_events_organizer_header .cmsms_events_organizer_header_right a:before {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] + 1) . "px;
	}
	
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td .tribe-events-viewmore, 
	#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td .tribe-events-viewmore a,
	.tribe-events-tooltip .tribe-events-event-body .duration,
	.tribe-events-tooltip .tribe-events-event-body .description,
	.vcalendar .vevent .cmsms_event_month {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_h6_font_font_size'] - 1) . "px;
	}
	/* Finish H6 Font */
	
	
	/* Start Button Font */
	#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-submit input,
	#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-list-event-description .tribe-events-read-more,
	.widget .tribe-events-widget-link > a,
	.tribe-events-week #tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .tribe-events-read-more {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_button_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_button_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_button_font_text_transform'] . ";
	}
	/* Finish Button Font */
	
	
	/* Start Small Text Font */
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-grid-body .tribe-week-grid-hours,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-allday .column.first,
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-number .tribe-countdown-under {
		font-family:" . cmsmasters_get_google_font($cmsms_option[CMSMS_SHORTNAME . '_h6_font_google_font']) . $cmsms_option[CMSMS_SHORTNAME . '_h6_font_system_font'] . ";
		font-size:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] . "px;
		line-height:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_line_height'] . "px;
		font-weight:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_weight'] . ";
		font-style:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_style'] . ";
		text-transform:" . $cmsms_option[CMSMS_SHORTNAME . '_small_font_text_transform'] . ";
	}
	
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-grid-body .tribe-week-grid-hours {
		line-height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_small_font_line_height'] - 1) . "px;
	}
	
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-allday .column.first > span {
		height:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_small_font_line_height'] * 2) . "px;
	}
	
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-grid-body .tribe-week-grid-hours > div {
		padding-top:" . (60 - (int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] / 2 + 2) . "px;
		padding-bottom:" . (60 - (int) $cmsms_option[CMSMS_SHORTNAME . '_h1_font_line_height'] / 2 + 2) . "px;
	}
	
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-allday .column.first {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] + 1) . "px;
	}
	
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-number .tribe-countdown-under {
		font-size:" . ((int) $cmsms_option[CMSMS_SHORTNAME . '_small_font_font_size'] + 3) . "px;
	}
	
	#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_left .tribe-events-schedule .tribe-events-date,
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td,
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-number .tribe-countdown-under {
		font-weight:normal; /* static */
	}
	
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-allday .column.first,
	.widget .vcalendar .vevent .tribe-events-list-widget-content-wrap a,
	.widget .tribe-events-list-widget-events .tribe-events-list-widget-content-wrap a {
		font-weight:bold; /* static */
	}
	
	.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar th.tribe-mini-calendar-dayofweek {
		text-transform:uppercase; /* static */
	}
	
	#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-date-filter #tribe-bar-dates label, 
	#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-search-filter label, 
	#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-geoloc-filter label,
	#tribe-events-content.tribe-events-month table.tribe-events-calendar thead th,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-allday .column.first,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-header .tribe-grid-content-wrap .column,
	#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-header .tribe-grid-content-wrap .column *,
	#tribe-events-content.tribe-events-single .cmsms_single_event_meta .tribe-events-meta-group .tribe-events-single-section-title {
		text-transform:uppercase; /* static */
	}
	/* Finish Small Text Font */

/***************** Finish Events Font Styles ******************/


";

}
	
	return $custom_css;
}

