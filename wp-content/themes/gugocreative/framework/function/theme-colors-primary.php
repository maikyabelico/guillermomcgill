<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * Theme Primary Color Schemes Rules
 * Created by CMSMasters
 * 
 */


function cmsmasters_theme_colors_primary() {
	$cmsms_option = cmsmasters_get_global_options();
	
	
	$cmsms_color_schemes = cmsms_color_schemes_list();
	
	
	$custom_css = "/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * Theme Primary Color Schemes Rules
 * Created by CMSMasters
 * 
 */

";
	
	
	foreach ($cmsms_color_schemes as $scheme => $title) {
		$rule = (($scheme != 'default') ? "html .cmsms_color_scheme_{$scheme} " : '');
		
		
		$custom_css .= "
/***************** Start {$title} Color Scheme Rules ******************/

	/* Start Main Content Font Color */
	" . (($scheme == 'default') ? "body," : '') . (($scheme != 'default') ? ".cmsms_color_scheme_{$scheme}," : '') . "
	{$rule}input[type=text],
	{$rule}input[type=number],
	{$rule}input[type=email],
	{$rule}input[type=password],
	{$rule}input[type=search],
	{$rule}input[type=tel],
	{$rule}textarea,
	{$rule}select,
	{$rule}option,
	{$rule}.pl_subtitle,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_info .price del,
	{$rule}.widget .tweet_list a:hover,
	{$rule}.widget_wysija input[type=text]:focus,
	{$rule}.profiles.opened-article .profile .cmsms_profile_header .cmsms_profile_subtitle,
	{$rule}.cmsms_search_post .cmsms_post_cont_info,
	{$rule}.post.cmsms_search_post .cmsms_post_cont_info,
	{$rule}.cmsms_tabs .cmsms_tabs_wrap .cmsms_tab.tab_comments > ul > li > span {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_color']) . "
	}
	
	{$rule}.widget_wysija input[type=text],
	{$rule}.widget_recent_entries ul li .post-date {
		color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_color']) . ", 0.3);
	}
	
	{$rule}input::-webkit-input-placeholder {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_color']) . "
	}
	
	{$rule}input:-moz-placeholder {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_color']) . "
	}
	/* Finish Main Content Font Color */
	
	
	/* Start Primary Color */
	{$rule}a,
	{$rule}.cmsms_sitemap_wrap a,
	{$rule}input[type=submit],
	{$rule}input[type=button],
	{$rule}.cmsms_counters .cmsms_counter_wrap .cmsms_counter .cmsms_counter_inner:before,
	{$rule}.post .cmsms_post_cont_info a,
	{$rule}.post .cmsms_post_cont_info .cmsms_post_tags a:hover,
	{$rule}.comment-content abbr,
	{$rule}.profiles.opened-article .profile .profile_sidebar .profile_social_icons .profile_social_icons_list li a,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_info .price,
	{$rule}.post_nav span:before,
	{$rule}.widget ul li a,
	{$rule}.widget ul li a:hover,
	{$rule}.related_posts_tabs .cmsms_tab_inner .one_half .rel_post_content:hover a,
	{$rule}.cmsms_dropcap.type1, 
	{$rule}.tweet_time:before,
	{$rule}.cmsms_icon_box.cmsms_icon_top:before,
	{$rule}.cmsms_icon_box.cmsms_icon_heading_left .icon_box_heading:before,
	{$rule}.cmsms_icon_list_items.cmsms_color_type_icon .cmsms_icon_list_icon:before,
	{$rule}#cancel-comment-reply-link, 
	{$rule}.img_placeholder_small:hover,
	{$rule}#wp-calendar thead th {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	" . (($scheme == 'default') ? "mark," : '') . (($scheme != 'default') ? ".cmsms_color_scheme_{$scheme} mark," : '') . "
	{$rule}.cmsms_icon_box.cmsms_icon_box_top:before,
	{$rule}.cmsms_icon_box.cmsms_icon_box_left_top:before,
	{$rule}.cmsms_icon_box.cmsms_icon_box_left:before,
	{$rule}.cmsms_icon_list_items.cmsms_color_type_bg .cmsms_icon_list_item .cmsms_icon_list_icon,
	{$rule}.cmsms_icon_list_items.cmsms_color_type_icon .cmsms_icon_list_item:hover .cmsms_icon_list_icon,
	{$rule}button,
	{$rule}.owl-page:hover, 
	{$rule}.owl-page.active,
	{$rule}.cmsms_quotes_slider.cmsms_quotes_slider_type_box .owl-pagination .owl-page:hover, 
	{$rule}.cmsms_quotes_slider.cmsms_quotes_slider_type_box .owl-pagination .owl-page.active,
	{$rule}.cmsms_dropcap.type2,
	{$rule}.cmsms_tabs.lpr .cmsms_tabs_list .cmsms_tabs_list_item a:hover:before {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsms_project_filter_list .button, 
	{$rule}.cmsms_project_filter_list .button:hover, 
	{$rule}.cmsms_project_filter_list .current .button, 
	{$rule}.cmsms_post_filter_list .button, 
	{$rule}.cmsms_post_filter_list .button:hover, 
	{$rule}.cmsms_post_filter_list .current .button {
		background-color:transparent; /* static */
	}
	
	{$rule}.widget ul li > a {
		border-color:transparent; /* static */
	}
	
	{$rule}.cmsms_icon_list_items.cmsms_color_type_border .cmsms_icon_list_item .cmsms_icon_list_icon:after {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	/* Finish Primary Color */
	
	
	/* Start Highlight Color */
	{$rule}a:hover,
	{$rule}h1 a:hover,
	{$rule}h2 a:hover,
	{$rule}h3 a:hover,
	{$rule}h4 a:hover,
	{$rule}h5 a:hover,
	{$rule}h6 a:hover,
	{$rule}.color_2,
	" . (($scheme == 'default') ? "{$rule}.headline_outer a," : '') . "
	{$rule}.cmsmsLike:hover:before,
	{$rule}.cmsmsLike.active:before,
	{$rule}.cmsms_post_comments:hover:before,
	{$rule}.search_bar_wrap button:hover:before,
	{$rule}.cmsms_prev_arrow:hover,
	{$rule}.cmsms_next_arrow:hover,
	{$rule}.cmsms_sitemap_wrap a[href]:hover,
	{$rule}.cmsms_notice .notice_close:hover,
	{$rule}.cmsms_clients_slider .owl-buttons span:hover,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item .cmsms_price_wrap .cmsms_currency,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item .cmsms_price_wrap .cmsms_price,
	{$rule}.cmsms_tabs .cmsms_tabs_list .cmsms_tabs_list_item a:hover,
	{$rule}.cmsms_toggles .cmsms_toggles_filter > a.current_filter,
	{$rule}.cmsms_twitter .owl-controls .owl-buttons .owl-prev:hover .cmsms_prev_arrow:before,
	{$rule}.cmsms_twitter .owl-controls .owl-buttons .owl-next:hover .cmsms_next_arrow:before,
	{$rule}.cmsmsView.active:before,
	{$rule}.blog.columns.puzzle .post figure:hover + .puzzle_post_content_wrapper .cmsms_post_title,
	{$rule}.blog.columns.puzzle .post figure:hover + .puzzle_post_content_wrapper .cmsms_post_title a,
	{$rule}.blog.columns.puzzle .post.format-gallery figure:hover + .dn + .puzzle_post_content_wrapper .cmsms_post_title,
	{$rule}.blog.columns.puzzle .post.format-gallery figure:hover + .dn + .puzzle_post_content_wrapper .cmsms_post_title a,
	{$rule}.blog.columns.puzzle .tribe_events figure:hover + .puzzle_post_content_wrapper .cmsms_post_title,
	{$rule}.blog.columns.puzzle .tribe_events figure:hover + .puzzle_post_content_wrapper .cmsms_post_title a,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont a:hover,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsms_post_wrap_info a:hover > span,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsms_post_comments:hover:before,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsmsLike:hover:before,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsmsLike.active:before,
	{$rule}.post .cmsms_post_cont_info a:hover,
	{$rule}.pl_social_list li a:hover,
	{$rule}.cmsms_img_rollover_wrap .cmsms_img_rollover .cmsms_open_link:hover,
	{$rule}.portfolio.puzzle .project .project_outer .project_inner a:hover,
	{$rule}.portfolio.puzzle .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title a:hover,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsms_post_comments:hover:before,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsmsLike:hover:before,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsmsLike.active:before,
	{$rule}.cmsms_posts_slider .owl-buttons span:hover,
	{$rule}.profiles.opened-article .profile .profile_sidebar .profile_social_icons .profile_social_icons_list li a:hover,
	{$rule}.cmsms_profile.vertical .profile .pl_img:hover .pl_noimg,
	{$rule}.portfolio.opened-article .project.format-gallery .project_gallery_row .cmsms_img_rollover_wrap .cmsms_img_rollover .cmsms_image_link:hover > span:before,
	{$rule}.cmsms_single_slider .cmsms_single_slider_item .cmsms_img_wrap:hover + .cmsms_single_slider_item_inner .cmsms_single_slider_title a,
	{$rule}.cmsms_single_slider .preloader:hover > span:before,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_footer > a:hover,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_footer > a:hover:before,
	{$rule}.cmsms_posts_slider .product.outofstock .product_outer:hover .out-of-stock,
	{$rule}.cmsms_wrap_pagination ul li .page-numbers:hover,
	{$rule}.cmsms_wrap_pagination ul li .page-numbers.current,
	{$rule}.img_placeholder_small,
	{$rule}.post_nav span:hover:before,
	{$rule}.widget_nav_menu ul li.current-menu-item a,
	{$rule}.widget_pages ul li.current_page_item > a,
	{$rule}.widget_search .search_bar_wrap form p.search_button button[type=submit]:hover:before,
	{$rule}.widget_recent_entries ul li a:hover,
	{$rule}.widget .tweet_list a,
	{$rule}.widget_wysija input[type=submit],
	{$rule}#cancel-comment-reply-link:hover {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsms_button:hover,
	{$rule}input[type=submit]:hover,
	{$rule}input[type=button]:hover,
	{$rule}.cmsms-form-builder .button:hover,
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=\"checkbox\"] + span.wpcf7-list-item-label:after,
	{$rule}.cmsms-form-builder .check_parent input[type=\"checkbox\"] + label:after,
	{$rule}.cmsms-form-builder .check_parent input[type=\"radio\"] + label:after,
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=\"radio\"] + span.wpcf7-list-item-label:after,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item.pricing_best .cmsms_pricing_item_inner,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item.pricing_best .cmsms_button,
	{$rule}.cmsms_stats.stats_mode_circles .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner,
	{$rule}.cmsms_stats.stats_mode_bars .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner,
	{$rule}.cmsms_toggles.toggles_mode_accordion .cmsms_toggle_wrap:not(.current_toggle) .cmsms_toggle_title > a:hover,
	{$rule}.cmsms_post_read_more:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but.current,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but.current,
	{$rule}.blog.columns.puzzle .post .preloader,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but:hover,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but.current,
	{$rule}.cmsms_posts_slider .post .cmsms_slider_post_cont .thumb_wrap.no_thumb .cmsms_post_date_wrap .published {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.content_slider .owl-controls .owl-buttons .owl-prev:hover,
	{$rule}.content_slider .owl-controls .owl-buttons .owl-next:hover {
		background-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . ", 0.3);
	}
	
	{$rule}.cmsms_button:hover,
	{$rule}input[type=text]:focus,
	{$rule}input[type=number]:focus,
	{$rule}input[type=email]:focus,
	{$rule}input[type=password]:focus,
	{$rule}input[type=search]:focus,
	{$rule}input[type=tel]:focus,
	{$rule}textarea:focus,
	{$rule}select:focus,
	{$rule}input[type=text]:hover,
	{$rule}input[type=number]:hover,
	{$rule}input[type=email]:hover,
	{$rule}input[type=password]:hover,
	{$rule}input[type=search]:hover,
	{$rule}input[type=submit]:hover,
	{$rule}input[type=button]:hover,
	{$rule}input[type=tel]:hover,
	{$rule}textarea:hover,
	{$rule}select:hover,
	{$rule}.cmsms-form-builder .button:hover,
	{$rule}.search_bar_wrap:hover input[type=search],
	{$rule}.cmsms_pricing_table .cmsms_pricing_item.pricing_best .cmsms_pricing_item_inner,
	{$rule}.quote_grid .cmsms_quote:hover,
	{$rule}.cmsms_post_read_more:hover,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but:hover,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but.current,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but.current,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but.current,
	{$rule}.cmsms_img_rollover_wrap .cmsms_img_rollover .cmsms_open_link:hover,
	{$rule}.cmsms_single_slider .preloader:hover > span,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_footer > a:hover,
	{$rule}.widget_wysija input[type=submit] {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsms_tabs .cmsms_tabs_list .cmsms_tabs_list_item.current_tab a,
	{$rule}.cmsms_toggles.toggles_mode_toggle .cmsms_toggle_wrap:not(.current_toggle):hover,
	{$rule}.post .cmsms_post_cont_info .cmsms_post_tags a:hover,
	{$rule}.cmsms_profile.horizontal .profile .profile_inner .pl_wrap_title .entry-title a:hover,
	{$rule}.cmsms_profile.vertical .pl_content .entry-title a:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_list li > a.button:hover span, 
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_list li.current > a.button span,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_list li > a.button:hover span, 
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_list li.current > a.button span,
	{$rule}.portfolio.grid .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title a:hover,
	{$rule}.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_header .cmsms_slider_project_title a:hover,
	{$rule}.post_comments .commentlist .comment-body .comment-edit-link:hover,
	{$rule}.post_comments .commentlist .comment-body .comment-reply-link:hover,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_header .cmsms_product_title a:hover,
	{$rule}.widget ul li > a:hover {
		" . cmsms_color_css('border-bottom-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	/* Finish Highlight Color */
	
	
	/* Start Headings Color */
	{$rule}h1,
	{$rule}h2,
	{$rule}h3,
	{$rule}h4,
	{$rule}h5,
	{$rule}h6,
	{$rule}h1 a,
	{$rule}h2 a,
	{$rule}h3 a,
	{$rule}h4 a,
	{$rule}h5 a,
	{$rule}h6 a,
	{$rule}.button,
	{$rule}.cmsms_button,
	{$rule}.search_bar_wrap button:before,
	{$rule}.cmsms_prev_arrow,
	{$rule}.cmsms_next_arrow,
	{$rule}.cmsms_table tr th,
	{$rule}.cmsms_table tr td,
	" . (($scheme != 'default') ? "{$rule}.headline_outer," : '') . "
	" . (($scheme == 'default') ? "#slide_top:hover," : '') . "
	{$rule}.content_slider .owl-controls .owl-buttons .owl-prev:hover .cmsms_prev_arrow:before,
	{$rule}.content_slider .owl-controls .owl-buttons .owl-next:hover .cmsms_next_arrow:before,
	{$rule}.cmsms_clients_slider .owl-buttons span,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item .cmsms_price_wrap .cmsms_coins,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item .cmsms_price_wrap .cmsms_period,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item.pricing_best .cmsms_button:hover,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item.pricing_best .cmsms_price_wrap .cmsms_price,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item.pricing_best .cmsms_price_wrap .cmsms_currency,
	{$rule}.cmsms_counters .cmsms_counter_wrap .cmsms_counter .cmsms_counter_inner .cmsms_counter_counter_wrap, 
	{$rule}.cmsms_stats.stats_mode_bars.stats_type_vertical .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner,
	{$rule}.cmsms_stats.stats_mode_bars.stats_type_vertical .cmsms_stat_wrap .cmsms_stat_title,
	{$rule}.cmsms_stats.stats_mode_bars.stats_type_horizontal .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner,
	{$rule}.cmsms_stats.stats_mode_bars.stats_type_horizontal .cmsms_stat_wrap .cmsms_stat_counter_wrap,
	{$rule}.cmsms_stats.stats_mode_circles .cmsms_stat_wrap .cmsms_stat .cmsms_stat_inner .cmsms_stat_counter_wrap,
	{$rule}.cmsms_stats.stats_mode_circles .cmsms_stat_wrap .cmsms_stat_title,
	{$rule}.cmsms_tabs .cmsms_tabs_list .cmsms_tabs_list_item a,
	{$rule}.cmsms_tabs .cmsms_tabs_list .cmsms_tabs_list_item.current_tab a,
	{$rule}.cmsms_toggles .cmsms_toggle_wrap .cmsms_toggle_title > a,
	{$rule}.cmsms_toggles .cmsms_toggle_wrap .cmsms_toggle_title:hover > a,
	{$rule}.cmsms_toggles .cmsms_toggle_wrap.current_toggle .cmsms_toggle_title > a,
	{$rule}.cmsms_twitter .owl-controls .owl-buttons .owl-prev .cmsms_prev_arrow:before,
	{$rule}.cmsms_twitter .owl-controls .owl-buttons .owl-next .cmsms_next_arrow:before,
	{$rule}.cmsms_notice .notice_close,
	{$rule}.cmsms_post_read_more,
	{$rule}.cmsmsLike,
	{$rule}.cmsmsView,
	{$rule}.cmsmsLike:hover,
	{$rule}.cmsmsLike:before,
	{$rule}.quote_subtitle,
	{$rule}.cmsms_post_comments,
	{$rule}.cmsms_post_comments:hover,
	{$rule}.cmsms_post_comments:before,
	{$rule}.blog.timeline .post .cmsms_post_info,
	{$rule}.blog.columns.puzzle .post .preloader span:before,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but:before,
	{$rule}.cmsms_post_filter_list .button:hover,
	{$rule}.cmsms_post_filter_list .current .button,
	{$rule}.cmsms_profile.horizontal .pl_wrap_title .entry-title a,
	{$rule}.cmsms_profile.horizontal .pl_wrap_title .entry-title a:hover,
	{$rule}.cmsms_profile.vertical .pl_content .entry-title a,
	{$rule}.cmsms_profile.vertical .pl_content .entry-title a:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but,
	{$rule}.cmsms_project_filter_list .button,
	{$rule}.cmsms_project_filter_list .button:hover,
	{$rule}.cmsms_project_filter_list .current .button,
	{$rule}.portfolio .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title a,
	{$rule}.portfolio .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title a:hover,
	{$rule}.cmsms_posts_slider .post .cmsms_slider_post_cont .thumb_wrap .cmsms_post_date_wrap .cmsms_post_date .published,
	{$rule}.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_header .cmsms_slider_project_title,
	{$rule}.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_header .cmsms_slider_project_title a,
	{$rule}.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_header .cmsms_slider_project_title:hover,
	{$rule}.cmsms_posts_slider .project .slider_project_outer .slider_project_inner .cmsms_slider_project_header .cmsms_slider_project_title a:hover,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_header .cmsms_product_title,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_header .cmsms_product_title a,
	{$rule}.post_comments .commentlist .comment-body .comment-edit-link,
	{$rule}.post_comments .commentlist .comment-body .comment-reply-link,
	{$rule}.cmsms_search .cmsms_search_post .cmsms_search_post_number_wrap,
	{$rule}.cmsms_search .cmsms_search_post .cmsms_search_post_cont .cmsms_post_meta_info a span,
	{$rule}.cmsms_wrap_pagination ul li .page-numbers,
	{$rule}.cmsms_twitter_wrap .twr_icon:before,
	{$rule}.cmsms_twitter_wrap .cmsms_twitter .published,
	{$rule}.widget_search .search_bar_wrap form p.search_button button[type=submit]:before,
	{$rule}.widget .tweet_list .tweet_time,
	{$rule}.cmsms_img_wrap .img_placeholder:before,
	{$rule}.cmsms_img_rollover_wrap .img_placeholder:before,
	{$rule}#wp-calendar caption,
	{$rule}table caption,
	{$rule}#header .logo,
	{$rule}fieldset legend,
	{$rule}.related_posts_tabs .cmsms_tab_inner .one_half .rel_post_content figure a,
	{$rule}blockquote footer,
	{$rule}.cmsms_post_filter_list .button,
	{$rule}.project .cmsms_project_cont_info  {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.cmsms_table tr.cmsms_table_row_header,
	{$rule}.cmsms_hover_slider .cmsms_hover_slider_thumbs > li a:before,
	{$rule}.content_slider .owl-controls .owl-pagination .owl-page,
	{$rule}.cmsms_stats.stats_mode_circles .cmsms_stat_wrap .cmsms_stat,
	{$rule}.cmsms_stats.stats_mode_circles .cmsms_stat_wrap .cmsms_stat canvas,
	{$rule}.cmsms_quotes_slider.cmsms_quotes_slider_type_center .cmsms_quote .cmsms_quote_inner .quote_image span,
	{$rule}.cmsms_toggles .cmsms_toggles_filter .cmsms_toggles_filter_sep > span,
	{$rule}.cmsms_toggles .cmsms_toggles_filter .cmsms_toggles_filter_sep > span:after,
	{$rule}.cmsms_toggles .cmsms_toggles_filter .cmsms_toggles_filter_sep > span:before,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsms_post_footer,
	{$rule}.blog.columns.puzzle .post .preloader:after,
	{$rule}.cmsms_profile.horizontal .profile .pl_img:before,
	{$rule}.cmsms_icon_list_items .cmsms_icon_list_item .cmsms_icon_list_icon,
	" . (($scheme == 'default') ? "{$rule}.headline_outer," : '') . "
	" . (($scheme != 'default') ? "{$rule}.headline_outer .headline_text .cmsms_breadcrumbs .cmsms_breadcrumbs_inner .breadcrumbs_sep > span," : '') . "
	" . (($scheme != 'default') ? "{$rule}.headline_outer .headline_text .cmsms_breadcrumbs .cmsms_breadcrumbs_inner .breadcrumbs_sep > span:before," : '') . "
	" . (($scheme != 'default') ? "{$rule}.headline_outer .headline_text .cmsms_breadcrumbs .cmsms_breadcrumbs_inner .breadcrumbs_sep > span:after," : '') . "
	{$rule}.button:hover,
	{$rule}.current > .button,
	{$rule}.button.current,
	{$rule}button:hover,
	{$rule}form .formError .formErrorContent {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.owl-pagination .owl-page,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but,
	{$rule}.cmsms_post_read_more,
	{$rule}.blog.columns.puzzle .post .puzzle_post_content_wrapper:before,
	{$rule}.blog.columns.puzzle .post .preloader span {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.content_slider .owl-controls .owl-buttons .owl-prev,
	{$rule}.content_slider .owl-controls .owl-buttons .owl-next {
		background-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 0.3);
	}
	
	{$rule}.cmsms_single_slider .preloader,
	{$rule}.cmsms_posts_slider .product:not(.outofstock) .product_outer .product_inner:hover .cmsms_product_img_wrap .cmsms_product_img a:before,
	{$rule}.cmsms_profile.vertical .profile .pl_img .pl_noimg,
	{$rule}.project_outer:hover .cmsms_img_rollover, 
	{$rule}.slider_project_outer:hover .cmsms_img_rollover, 	
	{$rule}.project_gallery_row .cmsms_img_rollover_wrap:hover .cmsms_img_rollover {
		background-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 0.9);
	}
	
	{$rule}.cmsms_stats.stats_mode_bars.stats_type_horizontal .cmsms_stat_wrap:before {
		background-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 0.1);
	}
	/* Finish Headings Color */
	
	
	/* Start Main Background Color */
	{$rule}.cmsms_button:hover,
	" . (($scheme == 'default') ? "#slide_top," : '') . "
	{$rule}.cmsms_table tr.cmsms_table_row_header th,
	{$rule}input[type=submit]:hover,
	{$rule}input[type=button]:hover,
	" . (($scheme == 'default') ? "{$rule}.headline_outer," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer h1," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer h2," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer h3," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer h4," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer h5," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer h6," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer a:hover," : '') . "
	{$rule}.content_slider .owl-controls .owl-buttons .owl-prev .cmsms_prev_arrow:before,
	{$rule}.content_slider .owl-controls .owl-buttons .owl-next .cmsms_next_arrow:before,
	{$rule}.cmsms_quotes_slider.cmsms_quotes_slider_type_center .cmsms_quote .cmsms_quote_inner .quote_image span:before,
	{$rule}.cmsms_post_read_more:hover,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont *,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsms_post_comments:before,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsmsLike:before,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but:hover,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but:hover:before,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but.current,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but.current:before,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but:hover:before,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but.current,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but.current:before,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but:hover,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but:hover:before,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but.current,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but.current:before,
	{$rule}.cmsms_img_rollover_wrap .cmsms_img_rollover .cmsms_open_link,
	{$rule}.portfolio.puzzle .project .project_outer .project_inner *,
	{$rule}.portfolio.puzzle .project .project_outer .project_inner .cmsms_project_header .cmsms_project_title a,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsms_post_comments,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsms_post_comments:hover,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsms_post_comments:before,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsmsLike,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsmsLike:hover,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsmsLike.active,
	{$rule}.portfolio.puzzle .project .project_outer .cmsms_project_footer .cmsmsLike:before,
	{$rule}.portfolio.opened-article .project.format-gallery .project_gallery_row .cmsms_img_rollover_wrap .cmsms_img_rollover .cmsms_image_link > span:before,
	{$rule}.cmsms_single_slider .preloader > span:before,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_footer > a,
	{$rule}.cmsms_search .cmsms_search_post .cmsms_search_post_cont .cmsms_post_cont_info .cmsms_post_read_more:hover,
	{$rule}.cmsms_profile.vertical .profile .pl_img .pl_noimg,
	{$rule}.cmsms_icon_box.cmsms_icon_box_left_top:before,
	{$rule}.cmsms_icon_box.cmsms_icon_box_left:before,
	{$rule}.cmsms_icon_box.cmsms_icon_box_top:before,
	{$rule}.cmsms_icon_list_items.cmsms_color_type_border .cmsms_icon_list_item .cmsms_icon_list_icon:before,
	{$rule}mark,
	{$rule}form .formError .formErrorContent {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsms_post_date .published,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsmsView > span,
	{$rule}.blog.columns.puzzle .post .cmsms_post_cont .cmsmsView:before {
		color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 0.5);
	}
	
	" . (($scheme == 'default') ? "body," : '') . (($scheme != 'default') ? ".cmsms_color_scheme_{$scheme}," : '') . "
	{$rule}input[type=text],
	{$rule}input[type=number],
	{$rule}input[type=email],
	{$rule}input[type=password],
	{$rule}input[type=search],
	{$rule}input[type=tel],
	{$rule}textarea,
	{$rule}select,
	{$rule}option,
	{$rule}.button,
	{$rule}.cmsms_button,
	{$rule}input[type=submit],
	{$rule}input[type=button],
	{$rule}.cmsms_notice,
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=\"checkbox\"] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsms-form-builder .check_parent input[type=\"checkbox\"]+label:before,
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=\"radio\"] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsms-form-builder .check_parent input[type=\"radio\"]+label:before,
	" . (($scheme == 'default') ? "{$rule}.headline_outer .headline_text .cmsms_breadcrumbs .cmsms_breadcrumbs_inner .breadcrumbs_sep > span," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer .headline_text .cmsms_breadcrumbs .cmsms_breadcrumbs_inner .breadcrumbs_sep > span:before," : '') . "
	" . (($scheme == 'default') ? "{$rule}.headline_outer .headline_text .cmsms_breadcrumbs .cmsms_breadcrumbs_inner .breadcrumbs_sep > span:after," : '') . "
	{$rule}.content_slider .owl-controls .owl-pagination .owl-page:hover,
	{$rule}.content_slider .owl-controls .owl-pagination .owl-page.active,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item.pricing_best .cmsms_button:hover,
	{$rule}.cmsms_post_read_more,
	{$rule}.blog.timeline .post .cmsms_post_info,
	{$rule}.cmsms_profile .profile .pl_img figure a:after,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_sort_block .cmsms_project_sort_but,
	{$rule}.cmsms_project_filter_wrap .cmsms_project_filter .cmsms_project_filter_block .cmsms_project_filter_but,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but,
	{$rule}.cmsms_posts_slider .post .cmsms_slider_post_cont .thumb_wrap .cmsms_post_date_wrap .published,
	{$rule}.cmsms_posts_slider .project .slider_project_outer,
	{$rule}.cmsms_search .cmsms_search_post .cmsms_search_post_number,
	{$rule}.cmsms_wrap_pagination ul li .page-numbers,
	{$rule}.search_bar_wrap,
	{$rule}.search_bar_wrap input[type=text],
	{$rule}.search_bar_wrap input[type=text]:focus,
	{$rule}.portfolio .project .project_outer,
	" . (($scheme == 'default') ? ".middle_inner," : '') . "
	{$rule}.related_posts_tabs .cmsms_tab_inner .one_half .rel_post_content figure a {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.content_slider .owl-controls .owl-pagination .owl-page,
	{$rule}.cmsms_img_rollover_wrap .cmsms_img_rollover .cmsms_open_link,
	{$rule}.cmsms_single_slider .preloader > span,
	{$rule}.cmsms_posts_slider .product .product_outer .product_inner .cmsms_product_footer > a {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	/* Finish Main Background Color */
	
	
	/* Start Alternate Background Color */
	{$rule}.button:hover,
	{$rule}button,
	{$rule}button:hover,
	{$rule}.current > .button,
	{$rule}.button.current, 
	{$rule}.owl-buttons,
	{$rule}.cmsms_icon_list_items.cmsms_color_type_icon .cmsms_icon_list_icon_wrap,
	{$rule}.cmsms_dropcap.type2 {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}input[type=text]:focus,
	{$rule}input[type=number]:focus,
	{$rule}input[type=email]:focus,
	{$rule}input[type=password]:focus,
	{$rule}input[type=search]:focus,
	{$rule}input[type=tel]:focus,
	{$rule}textarea:focus,
	{$rule}select:focus,
	{$rule}input[type=text]:hover,
	{$rule}input[type=number]:hover,
	{$rule}input[type=email]:hover,
	{$rule}input[type=password]:hover,
	{$rule}input[type=search]:hover,
	{$rule}input[type=tel]:hover,
	{$rule}textarea:hover,
	{$rule}select:hover,
	{$rule}.owl-page,
	" . (($scheme != 'default') ? "{$rule}.headline_outer," : '') . "
	{$rule}.cmsms_table tr,
	{$rule}.cmsms_featured_block,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item .cmsms_pricing_item_inner,
	{$rule}.cmsms_counters .cmsms_counter_wrap .cmsms_counter,
	{$rule}.quote_grid .quotes_list .cmsms_quote,
	{$rule}.cmsms_tabs.tabs_mode_tab .cmsms_tabs_wrap,
	{$rule}.cmsms_toggles.toggles_mode_accordion .cmsms_toggle_wrap,
	{$rule}.post .cmsms_post_cont,
	{$rule}.post .wp-caption,
	{$rule}.cmsms_profile .profile,
	{$rule}.portfolio.grid .project .project_outer .project_inner,
	{$rule}.post_comments .commentlist .comment-body,
	{$rule}.widget_nav_menu ul li a,
	{$rule}.cmsms_icon_box.cmsms_icon_box_top,
	{$rule}.cmsms_icon_box.cmsms_icon_box_left,
	{$rule}.cmsms_icon_list_items.cmsms_color_type_icon .cmsms_icon_list_icon,
	{$rule}.gallery-item .gallery-icon,
	{$rule}.gallery-item .gallery-caption,
	{$rule}.cmsms_gallery li.cmsms_caption figure,
	{$rule}.about_author .about_author_inner,
	{$rule}.cmsms_img.with_caption,
	{$rule}.cmsms_tabs.lpr .cmsms_tabs_wrap,
	{$rule}.tweet_list li,
	{$rule}.widget_custom_latest_projects_entries .pj_ddn,
	{$rule}.widget_custom_popular_projects_entries .pj_ddn,
	{$rule}code, 
	{$rule}fieldset,
	{$rule}fieldset legend {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_alternate']) . "
	}
	/* Finish Alternate Background Color */
	
	
	/* Start Borders Color */
	{$rule}.pl_social_list li a {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.cmsms_sitemap_wrap .cmsms_sitemap > li:before,
	{$rule}.cmsms_toggles .cmsms_toggle_wrap .cmsms_toggle_title .cmsms_toggle_plus .cmsms_toggle_plus_hor,
	{$rule}.cmsms_toggles .cmsms_toggle_wrap .cmsms_toggle_title .cmsms_toggle_plus .cmsms_toggle_plus_vert, 
	{$rule}.cmsms_icon_list_items.cmsms_icon_list_type_block .cmsms_icon_list_item:before {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}input[type=text],
	{$rule}input[type=number],
	{$rule}input[type=email],
	{$rule}input[type=password],
	{$rule}input[type=search],
	{$rule}input[type=tel],
	{$rule}textarea,
	{$rule}select,
	{$rule}option,
	{$rule}.search_bar_wrap,
	{$rule}.button,
	{$rule}.cmsms_table tr,
	{$rule}.cmsms_table tr th,
	{$rule}.cmsms_table tr td,
	{$rule}input[type=submit],
	{$rule}input[type=button],
	{$rule}.cmsms_notice,
	{$rule}.cmsms_tabs.tabs_mode_tab .cmsms_tabs_list,
	{$rule}.cmsms_toggles .cmsms_toggle_wrap,
	{$rule}.cmsms_toggles.toggles_mode_accordion .cmsms_toggle_title > a,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item .cmsms_pricing_item_inner,
	{$rule}.cmsms_pricing_table .cmsms_pricing_item.pricing_best .cmsms_button:hover,
	{$rule}.blog.timeline:before,
	{$rule}.post.cmsms_timeline_type:before,
	{$rule}.blog.timeline .post .cmsms_post_info,
	{$rule}.cmsms_post_filter_wrap .cmsms_post_filter .cmsms_post_filter_block .cmsms_post_filter_but,
	{$rule}.cmsms_search .cmsms_search_post .cmsms_search_post_number,
	{$rule}.cmsms_img.with_caption,
	{$rule}.img_placeholder_small,
	{$rule}.widget_pages ul li > a,
	{$rule}.widget_pages ul li.current_page_item > a:hover,
	{$rule}.cmsms_icon_wrap .cmsms_simple_icon, 
	{$rule}.cmsms_icon_box.cmsms_icon_box_top,
	{$rule}.cmsms_icon_box.cmsms_icon_box_left,
	{$rule}.cmsms_icon_list_items.cmsms_icon_list_type_block .cmsms_icon_list_item,
	{$rule}.cmsms_icon_list_items.cmsms_color_type_bg .cmsms_icon_list_icon:after,
	{$rule}.cmsms_icon_list_items.cmsms_color_type_icon .cmsms_icon_list_icon:after,
	{$rule}.widget_custom_popular_projects_entries .img_placeholder,
	{$rule}.widget_custom_latest_projects_entries .img_placeholder,
	{$rule}code {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.cmsms_wrap_pagination ul,
	{$rule}.widget ul li {
		" . cmsms_color_css('border-top-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}hr,
	{$rule}.cmsms_tabs .cmsms_tabs_list .cmsms_tabs_list_item a,
	{$rule}.widget_nav_menu > div > ul a,
	{$rule}.widget .tweet_list .tweet_time,
	{$rule}.cmsms_divider,
	{$rule}.cmsms_widget_divider {
		" . cmsms_color_css('border-bottom-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	/* Finish Borders Color */
	
	
	/* Start Custom Rules */
	{$rule}::selection {
		" . cmsms_color_css('background', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ";
	}
	
	{$rule}::-moz-selection {
		" . cmsms_color_css('background', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	";
	
	
	if ($scheme == 'default') {
	$custom_css .= "
	#slide_top {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	";
	}
	
	
	$custom_css .= "
	/* Finish Custom Rules */

/***************** Finish {$title} Color Scheme Rules ******************/


/***************** Start Bottom Color Scheme Rules ******************/
	#bottom .widget_wysija_cont .updated, .widget_wysija_cont .login .message li {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_bottom_color']) . "
	}
	
	#bottom .widget_wysija_cont .updated, .widget_wysija_cont .login .message {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_bottom_bg']) . "
	}
	
	#bottom .widget_wysija_cont .updated, .widget_wysija_cont .login .message {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_bottom_border']) . "
	}
/***************** Finish Bottom Color Scheme Rules ******************/


/***************** Start Footer Color Scheme Rules ******************/
	.footer_inner .social_wrap .social_wrap_inner a,
	.cmsms_footer_default .footer_inner nav > div > ul > li > a {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_footer_heading']) . "
	}
	
	.footer_inner .social_wrap .social_wrap_inner a:hover,
	.cmsms_footer_default .footer_inner nav > div > ul > li > a:hover {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_footer_hover']) . "
	}
/***************** Finish Footer Color Scheme Rules ******************/


/***************** Start {$title} Button Color Scheme Rules ******************/
	
	{$rule}.cmsms_button.cmsms_but_bg_hover {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_bg_hover:hover, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_bg_hover {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsms_button.cmsms_but_bg_slide_left, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_right, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_top, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_bottom, 
	{$rule}.cmsms_button.cmsms_but_bg_expand_vert, 
	{$rule}.cmsms_button.cmsms_but_bg_expand_hor, 
	{$rule}.cmsms_button.cmsms_but_bg_expand_diag {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_bg_slide_left:hover, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_right:hover, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_top:hover, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_bottom:hover, 
	{$rule}.cmsms_button.cmsms_but_bg_expand_vert:hover, 
	{$rule}.cmsms_button.cm.sms_but_bg_expand_hor:hover, 
	{$rule}.cmsms_button.cmsms_but_bg_expand_diag:hover, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_bg_slide_left, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_bg_slide_right, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_bg_slide_top, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_bg_slide_bottom, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_bg_expand_vert, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_bg_expand_hor, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_bg_expand_diag {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_bg_slide_left:after, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_right:after, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_top:after, 
	{$rule}.cmsms_button.cmsms_but_bg_slide_bottom:after, 
	{$rule}.cmsms_button.cmsms_but_bg_expand_vert:after, 
	{$rule}.cmsms_button.cmsms_but_bg_expand_hor:after, 
	{$rule}.cmsms_button.cmsms_but_bg_expand_diag:after {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	
	{$rule}.cmsms_button.cmsms_but_shadow {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_shadow:hover, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_shadow {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsms_button.cmsms_but_icon_dark_bg, 
	{$rule}.cmsms_button.cmsms_but_icon_light_bg, 
	{$rule}.cmsms_button.cmsms_but_icon_divider {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_dark_bg:hover, 
	{$rule}.cmsms_button.cmsms_but_icon_light_bg:hover, 
	{$rule}.cmsms_button.cmsms_but_icon_divider:hover, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_dark_bg, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_light_bg, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_divider {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_divider:after {
		" . cmsms_color_css('border-right-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_inverse {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_inverse:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_inverse:after {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_inverse:hover, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_inverse {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_inverse:hover:before, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_inverse:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_inverse:hover:after, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_inverse:after {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	
	{$rule}.cmsms_button.cmsms_but_icon_slide_left, 
	{$rule}.cmsms_button.cmsms_but_icon_slide_right {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_slide_left:hover, 
	{$rule}.cmsms_button.cmsms_but_icon_slide_right:hover, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_slide_left, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_slide_right {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsms_button.cmsms_but_icon_hover_slide_left, 
	{$rule}.cmsms_button.cmsms_but_icon_hover_slide_right, 
	{$rule}.cmsms_button.cmsms_but_icon_hover_slide_top, 
	{$rule}.cmsms_button.cmsms_but_icon_hover_slide_bottom {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_button.cmsms_but_icon_hover_slide_left:hover, 
	{$rule}.cmsms_button.cmsms_but_icon_hover_slide_right:hover, 
	{$rule}.cmsms_button.cmsms_but_icon_hover_slide_top:hover, 
	{$rule}.cmsms_button.cmsms_but_icon_hover_slide_bottom:hover, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_hover_slide_left, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_hover_slide_right, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_hover_slide_top, 
	{$rule}.cmsms_paypal_donations > form:hover + .cmsms_button.cmsms_but_icon_hover_slide_bottom {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}

/***************** Finish {$title} Button Color Scheme Rules ******************/


";
	}
	
	
	return $custom_css;
}

