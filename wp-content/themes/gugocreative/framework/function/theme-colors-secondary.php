<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.1
 * 
 * Theme Secondary Color Schemes Rules
 * Created by CMSMasters
 * 
 */


function cmsmasters_theme_colors_secondary() {
	$cmsms_option = cmsmasters_get_global_options();
	
	
	$cmsms_color_schemes = cmsms_color_schemes_list();
	
	
	$custom_css = "/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * Theme Secondary Color Schemes Rules
 * Created by CMSMasters
 * 
 */

";
	
	
	foreach ($cmsms_color_schemes as $scheme => $title) {
		$rule = (($scheme != 'default') ? "html .cmsms_color_scheme_{$scheme} " : '');
		
		
		if (CMSMS_WOOCOMMERCE) {
			$custom_css .= "
/***************** Start {$title} WooCommerce Color Scheme Rules ******************/

	/* Start Main Content Font Color */
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_info .price del, 
	{$rule}.cmsms_single_product .cmsms_product_right_column .price del, 
	{$rule}#order_review .shop_table td.product-name, 
	{$rule}#order_review .shop_table td.product-name *, 
	{$rule}.shop_table.order_details td.product-name, 
	{$rule}.shop_table.order_details td.product-name *, 
	{$rule}.widget_shopping_cart_content .cart_list li .quantity, 
	{$rule}.product_list_widget li del .amount, 
	{$rule}.select2-container .select2-choice, 
	{$rule}.select2-container.select2-drop-above .select2-choice, 
	{$rule}.select2-container.select2-container-active .select2-choice, 
	{$rule}.select2-container.select2-container-active.select2-drop-above .select2-choice, 
	{$rule}.select2-drop.select2-drop-active, 
	{$rule}.select2-drop.select2-drop-above.select2-drop-active,
	{$rule}ul.order_details.bacs_details li strong {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_color']) . "
	}
	/* Finish Main Content Font Color */
	
	
	/* Start Primary Color */
	{$rule}.widget_shopping_cart_content .cart_list li a.remove,
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_info .price, 
	{$rule}.cmsms_single_product .cmsms_product_right_column .price,
	{$rule}.shop_table .product-name a:hover, 
	{$rule}.shop_table td.product-subtotal, 
	{$rule}.cart_totals table tr.cart-subtotal td, 
	{$rule}.cart_totals table tr.order-total td, 
	{$rule}#order_review .shop_table tr.order-total th, 
	{$rule}#order_review .shop_table tr.order-total td, 
	{$rule}.shop_table.order_details td.product-name a:hover, 
	{$rule}.shop_table.order_details tfoot tr:last-child th, 
	{$rule}.shop_table.order_details tfoot tr:last-child td, 
	{$rule}.widget_shopping_cart_content .cart_list li .quantity .amount, 
	{$rule}.widget_shopping_cart_content .total .amount, 
	{$rule}.product_list_widget li > a:hover, 
	{$rule}.product_list_widget li .amount, 
	{$rule}.widget_product_categories li.current-cat a {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	/* Finish Primary Color */
	
	
	/* Start Highlight Color */
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_footer > a:hover,
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_footer > a:hover:before,
	{$rule}.cmsms_star_rating .cmsms_star_color_wrap .cmsms_star,
	{$rule}.comment-form-rating .stars > span a:hover,
	{$rule}.comment-form-rating .stars > span a.active,
	{$rule}.cmsms_products .product.outofstock .product_outer:hover .out-of-stock,
	{$rule}.shop_table td.product-name a:hover,
	{$rule}.shop_table td.product-remove .remove:hover,
	{$rule}.required,
	{$rule}.widget_shopping_cart_content .cart_list li a.remove:hover,
	{$rule}.widget_shopping_cart_content .cart_list li a:hover,
	{$rule}.widget ul.product_list_widget li > a:hover {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsms_single_product .cmsms_product_right_column .cart .single_add_to_cart_button:hover,
	{$rule}.shipping-calculator-form .button,
	{$rule}.cart_totals .wc-proceed-to-checkout .button:hover,
	{$rule}.input-checkbox + label:after,
	{$rule}.input-radio + label:after,
	{$rule}input.shipping_method + label:after,
	{$rule}.return-to-shop .button:hover,
	{$rule}.shop_table.shop_table_responsive tr .order-actions .button:hover,
	{$rule}.widget_shopping_cart_content .buttons .button:hover,
	{$rule}.widget_price_filter .price_slider_wrapper .price_slider .ui-slider-range,
	{$rule}.widget_price_filter .price_slider_wrapper .price_slider_amount .button:hover,
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .tribe-events-read-more:hover {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_footer > a:hover,
	{$rule}.cmsms_single_product .cmsms_product_right_column .cart .single_add_to_cart_button:hover,
	{$rule}.shipping-calculator-form .button,
	{$rule}.cart_totals .wc-proceed-to-checkout .button:hover,
	{$rule}.return-to-shop .button:hover,
	{$rule}.shop_table.shop_table_responsive tr .order-actions .button:hover,
	{$rule}.widget_shopping_cart_content .buttons .button:hover,
	{$rule}.widget_price_filter .price_slider_wrapper .price_slider_amount .button:hover,
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .tribe-events-read-more:hover,
	{$rule}.select2-container.select2-container-active .select2-choice, 
	{$rule}.select2-container.select2-container-active.select2-drop-above .select2-choice, 
	{$rule}.select2-drop.select2-drop-active, 
	{$rule}.select2-drop.select2-drop-above.select2-drop-active {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_header .cmsms_product_title a:hover,
	{$rule}.shipping-calculator-button-wrap a.shipping-calculator-button:hover {
		" . cmsms_color_css('border-bottom-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	/* Finish Highlight Color */
	
	/* Start Headings Color */
	{$rule}.onsale,
	{$rule}.out-of-stock,
	{$rule}.woocommerce-info,
	{$rule}.woocommerce-message,
	{$rule}.woocommerce-error,
	{$rule}.cmsms_star_rating .cmsms_star,
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_header .cmsms_product_title,
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_header .cmsms_product_title a,
	{$rule}.shop_table td.product-name,
	{$rule}.shop_table td.product-name a,
	{$rule}.shipping-calculator-button-wrap a.shipping-calculator-button,
	{$rule}.widget_shopping_cart_content .cart_list li a,
	{$rule}.cmsms_dynamic_cart .widget_shopping_cart_content .buttons .button,
	{$rule}.shipping-calculator-form .button:hover,
	{$rule}.cart_totals table tr.shipping th,
	{$rule}.cart_totals table tr.cart-subtotal th,
	{$rule}.cart_totals table tr.order-total th,
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .tribe-events-read-more,
	{$rule}.cmsms_single_product .cmsms_woo_tabs .shop_attributes th,
	{$rule}#order_review .shop_table thead tr th, 
	{$rule}.quantity .text,
	{$rule}#order_review .shop_table tr.cart-subtotal th, 
	{$rule}#order_review .shop_table tr.cart-subtotal td, 
	{$rule}dl.customer_details dt, 
	{$rule}.shop_table.order_details thead tr:first-child th, 
	{$rule}.shop_table.order_details thead tr:first-child td, 
	{$rule}.shop_table.order_details tfoot tr:first-child th, 
	{$rule}.shop_table.order_details tfoot tr:first-child td, 
	{$rule}#order_review #payment .payment_methods label, 
	{$rule}.widget_shopping_cart_content .total strong, 
	{$rule}.cmsms_added_product_info .cmsms_added_product_info_text, 
	{$rule}.product_list_widget li > a, 
	{$rule}.woocommerce-error li {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.widget_price_filter .price_slider_wrapper .price_slider .ui-slider-handle:hover,
	{$rule}ul.order_details.bacs_details li,
	{$rule}ul.order_details li > span,
	{$rule}.shop_table thead tr th,
	{$rule}.cmsms_single_product .cmsms_product_images .cmsms_product_thumbs .cmsms_product_thumb:before {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.cmsms_products .product:not(.outofstock) .product_outer .product_inner:hover .cmsms_product_img_wrap .cmsms_product_img a:before {
		background-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 0.9);
	}
	
	{$rule}.shipping-calculator-form .button:hover {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.cmsms_single_product .cmsms_product_right_column .cart .quantity .qty,
	{$rule}.shop_table td.product-quantity .quantity input {
		border-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 0.15);
	}
	/* Finish Headings Color */
	
	
	/* Start Main Background Color */
	{$rule}.cmsms_dynamic_cart .cmsms_dynamic_cart_button,
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_footer > a,
	{$rule}.shipping-calculator-form .button,
	{$rule}ul.order_details li > span,
	{$rule}ul.order_details.bacs_details li,
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .tribe-events-read-more:hover {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.onsale,
	{$rule}.out-of-stock,
	{$rule}.shipping-calculator-form .button:hover,
	{$rule}.cmsms_dynamic_cart .widget_shopping_cart_content,
	{$rule}.widget_price_filter .price_slider_wrapper .price_slider .ui-slider-handle,
	{$rule}ul.order_details.bacs_details li strong,
	{$rule}.shop_table td.actions,
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .tribe-events-read-more,
	{$rule}.quantity .text, 
	{$rule}.select2-container .select2-choice, 
	{$rule}.select2-container.select2-drop-above .select2-choice {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsms_products .product .product_outer .product_inner .cmsms_product_footer > a,
	{$rule}.cmsms_dynamic_cart .cmsms_dynamic_cart_button,
	{$rule}.cmsms_dynamic_cart .widget_shopping_cart_content:after {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.rtl .cmsms_dynamic_cart .widget_shopping_cart_content:after {
		border-right-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 1) !important;
	}
	/* Finish Main Background Color */
	
	
	/* Start Alternate Background Color */
	{$rule}.shop_table thead tr th {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.woocommerce-info,
	{$rule}.woocommerce-message,
	{$rule}.woocommerce-error,
	{$rule}ul.order_details li,
	{$rule}.shop_table tr th,
	{$rule}.shop_table tr td,
	{$rule}.cart_totals table tr th,
	{$rule}.cart_totals table tr td,
	{$rule}.cart_totals .wc-proceed-to-checkout,
	{$rule}.cmsms_products .product .product_outer .product_inner,
	{$rule}#order_review .shop_table thead tr th, 
	{$rule}.input-checkbox + label:before,
	{$rule}.input-radio + label:before, 
	{$rule}input.shipping_method + label:before, 
	{$rule}.checkout #order_review #payment, 
	{$rule}.shop_table.order_details thead tr:first-child th, 
	{$rule}.shop_table.order_details thead tr:first-child td,
	{$rule}.cmsms_added_product_info, 
	{$rule}.select2-container.select2-container-active .select2-choice, 
	{$rule}.select2-container.select2-container-active.select2-drop-above .select2-choice, 
	{$rule}.select2-drop.select2-drop-active, 
	{$rule}.select2-drop.select2-drop-above.select2-drop-active {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.cmsms_added_product_info:after {
		" . cmsms_color_css('border-bottom-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_alternate']) . "
	}
	/* Finish Alternate Background Color */
	
	
	/* Start Borders Color */
	{$rule}.comment-form-rating .stars > span {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.widget_price_filter .price_slider_wrapper .price_slider {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.shop_table,
	{$rule}.shop_table th,
	{$rule}.shop_table td,
	{$rule}.woocommerce-info,
	{$rule}.woocommerce-message,
	{$rule}.woocommerce-error,
	{$rule}.cart_totals .wc-proceed-to-checkout,
	{$rule}#order_review #payment,
	{$rule}.widget_price_filter .price_slider_wrapper .price_slider .ui-slider-handle,
	{$rule}ul.order_details.bacs_details li strong,
	{$rule}.cmsms_dynamic_cart .widget_shopping_cart_content,
	{$rule}.cmsms_dynamic_cart .widget_shopping_cart_content:before,
	{$rule}ul.order_details li *,
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .tribe-events-read-more,
	{$rule}.cart_totals table tr th, 
	{$rule}.cart_totals table tr td, 
	{$rule}ul.order_details li, 
	{$rule}ul.order_details li > span, 
	{$rule}.product_list_widget li, 
	{$rule}.select2-container .select2-choice, 
	{$rule}.select2-container.select2-drop-above .select2-choice, 
	{$rule}.input-checkbox + label:before,
	{$rule}.input-radio + label:before, 
	{$rule}input.shipping_method + label:before, 
	{$rule}.shop_table.cart .cart_item,
	{$rule}.cmsms_added_product_info {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.cmsms_added_product_info:before {
		" . cmsms_color_css('border-bottom-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.rtl .cmsms_dynamic_cart .widget_shopping_cart_content:before {
		border-right-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 1) !important;
	}
	/* Finish Borders Color */

/***************** Finish {$title} WooCommerce Color Scheme Rules ******************/


";
		}


		if (CMSMS_EVENTS_CALENDAR) {
			$custom_css .= "
/***************** Start {$title} Events Color Scheme Rules ******************/

	/* Start Main Content Font Color */
	{$rule}.tribe-events-tooltip .tribe-events-event-body .description,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-othermonth .tribe-events-tooltip .tribe-events-event-body .description,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"], 
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"] a, 
	{$rule}.recurringinfo, 
	{$rule}.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-number .tribe-countdown-under {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_color']) . "
	}
	/* Finish Main Content Font Color */
	
	
	/* Start Primary Color */
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_left .tribe-events-schedule .tribe-events-cost, 
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-back a:hover, 
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-cal-links a:hover, 
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"] a:hover, 
	{$rule}#tribe-events-footer > a:hover, 
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .cmsms_events_list_event_header .tribe-events-event-cost, 
	{$rule}.recurringinfo a:hover,
	{$rule}ul.tribe-related-events > li .tribe-related-events-thumbnail .cmsms_events_img_placeholder:hover, 
	{$rule}.tribe-events-venue .cmsms_events_venue_header .cmsms_events_venue_header_right a:hover, 
	{$rule}.tribe-events-organizer .cmsms_events_organizer_header .cmsms_events_organizer_header_right a:hover, 
	{$rule}#tribe-events-content.tribe-events-photo #tribe-events-photo-events .tribe-events-photo-event .tribe-events-photo-event-wrap .tribe-events-event-details .tribe-events-event-meta .time-details, 
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile .tribe-events-event-body .time-details,
	{$rule}.widget .vcalendar .vevent .cmsms_widget_event_info .cmsms_widget_event_info_date,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-has-events:hover *, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info .tribe-mini-calendar-event-venue a:hover {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.tribe-events-list-widget-events .cmsms_event_date:before,
	{$rule}.tribe_events .cmsms_post_cont .cmsms_post_title:before,
	{$rule}#tribe-events-content.tribe-events-photo #tribe-events-photo-events .tribe-events-photo-event .tribe-events-photo-event-wrap .tribe-events-event-details .tribe-events-list-event-title:before, 
	{$rule}.tribe-bar-views-open label.button,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-has-events div .tribe-mini-calendar-day-link:hover:before {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_link']) . "
	}
	/* Finish Primary Color */
	
	
	/* Start Highlight Color */
	{$rule}.tribe-events-sub-nav li a:hover,
	{$rule}#tribe-events-bar #tribe-bar-views .tribe-bar-views-inner ul.tribe-bar-views-list li.tribe-bar-views-option.tribe-bar-active a,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"] a:hover,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-othermonth div[id*=\"tribe-events-daynum-\"] a:hover,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td .tribe-events-month-event-title a:hover,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-present div[id*=\"tribe-events-daynum-\"],
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-present div[id*=\"tribe-events-daynum-\"] a,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-present div[id*=\"tribe-events-daynum-\"] a:hover,
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-header .tribe-grid-content-wrap .column a:hover,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta .tribe-events-venue-details a:hover, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .tribe-mini-calendar-nav div .tribe-mini-calendar-nav-link span:hover:before,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-has-events div .tribe-mini-calendar-day-link:hover,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-present *,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-present:hover *,
	{$rule}.tribe-events-countdown-widget .tribe-countdown-text a:hover,
	{$rule}.tribe-events-venue-widget .tribe-venue-widget-wrapper .tribe-venue-widget-venue .tribe-venue-widget-venue-name a:hover,
	{$rule}.recurringinfo a {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	@media only screen and (max-width: 767px) {
		{$rule}#main #tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-thismonth.mobile-active *, 
		{$rule}#main #tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.mobile-active div[id*=\"tribe-events-daynum-\"], 
		{$rule}#main #tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.mobile-active div[id*=\"tribe-events-daynum-\"] a,
		{$rule}#main #tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-thismonth a:hover {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
		}
	}
	
	{$rule}#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-submit input:hover,
	{$rule}#tribe-events-bar #tribe-bar-views label.button:hover,
	{$rule}#tribe-events-bar #tribe-bar-views.tribe-bar-views-open label.button,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .cmsms_events_list_event_header .tribe-events-event-cost,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-list-event-description .tribe-events-read-more:hover,
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-event > div:first-child > .entry-title,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-has-events div .tribe-mini-calendar-day-link:before,
	{$rule}.widget .tribe-events-widget-link > a:hover {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	@media only screen and (max-width: 767px) {
		{$rule}#main #tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-has-events:before {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
		}
	}
	
	{$rule}#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-submit input:hover,
	{$rule}#tribe-events-bar #tribe-bar-views label.button:hover,
	{$rule}#tribe-events-bar #tribe-bar-views.tribe-bar-views-open label.button,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-list-event-description .tribe-events-read-more:hover,
	{$rule}.widget .tribe-events-widget-link > a:hover {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}#tribe-events-bar .tribe-bar-filters input[type=text]:hover,
	{$rule}#tribe-events-bar .tribe-bar-filters input[type=text]:focus,
	{$rule}#tribe-events-bar #tribe-bar-views .tribe-bar-views-inner ul.tribe-bar-views-list li.tribe-bar-views-option a:hover > span,
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-back a:hover,
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-cal-links a:hover,
	{$rule}.widget .vcalendar .vevent .entry-title a:hover {
		" . cmsms_color_css('border-bottom-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_hover']) . "
	}
	/* Finish Highlight Color */
	
	
	/* Start Headings Color */
	{$rule}#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-submit input:hover,
	{$rule}.tribe-events-sub-nav li a,
	{$rule}#tribe-events-content .cmsms_event_date .cmsms_event_day,
	{$rule}#tribe-events-content .cmsms_event_date .cmsms_event_month,
	{$rule}#tribe-events-bar #tribe-bar-views .tribe-bar-views-inner ul.tribe-bar-views-list li.tribe-bar-views-option a,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .cmsms_events_list_event_header .tribe-events-event-cost,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-list-event-description .tribe-events-read-more,
	{$rule}.tribe-events-tooltip .entry-title,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-othermonth .tribe-events-tooltip .entry-title,
	{$rule}.tribe-events-tooltip .tribe-events-event-body .duration,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-othermonth .tribe-events-tooltip .tribe-events-event-body .duration .tribe-events-abbr,
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-event > div:first-child > .entry-title a,
	{$rule}.vcalendar .vevent .cmsms_event_date .cmsms_event_day,
	{$rule}.vcalendar .vevent .cmsms_event_date .cmsms_event_month,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .tribe-mini-calendar-nav div,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .tribe-mini-calendar-nav div .tribe-mini-calendar-nav-link span:before,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar th.tribe-mini-calendar-dayofweek,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td *,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-othermonth.tribe-events-has-events *,
	{$rule}.widget .vcalendar .vevent .entry-title,
	{$rule}.widget .vcalendar .vevent .entry-title a,
	{$rule}.tribe-events-countdown-widget .tribe-countdown-text a,
	{$rule}.tribe-events-venue-widget .tribe-venue-widget-wrapper .tribe-venue-widget-venue .tribe-venue-widget-venue-name,
	{$rule}.tribe-events-venue-widget .tribe-venue-widget-wrapper .tribe-venue-widget-venue .tribe-venue-widget-venue-name a,
	{$rule}.widget .tribe-events-widget-link > a,
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .tribe-events-address address .adr span:first-child, 
	{$rule}.tribe-events-list-widget-events .cmsms_event_date .cmsms_event_day, 
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_left .tribe-events-schedule > div:before, 
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta > div:before, 
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta > div > div:before, 
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-back:before,
	{$rule}.widget .vcalendar .vevent .cmsms_widget_event_info > div:before, 
	{$rule}.widget .vcalendar .vevent .cmsms_widget_event_venue_info_loc > div:before, 
	{$rule}.tribe-events-list-widget-events .tribe-events-list-widget-content-wrap .tribe-events-event-cost, 
	{$rule}.widget .vcalendar .vevent .cmsms_widget_event_info .tribe-events-event-cost, 
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-back a, 
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-cal-links a, 
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_meta .tribe-events-meta-group .cmsms_event_meta_info .cmsms_event_meta_info_item .cmsms_event_meta_info_item_title, 
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_meta .tribe-events-meta-group .cmsms_event_meta_info .cmsms_event_meta_info_item dt,
	{$rule}#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner .tribe-bar-submit label, 
	{$rule}#tribe-events-footer > a, 
	{$rule}#tribe-events-content.tribe-events-list .tribe-events-list-separator-month, 
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-mobile-day-heading, 
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-mobile-day-date, 
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta .tribe-events-venue-details .author, 
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-event-meta .tribe-events-venue-details a, 
	{$rule}.tribe-events-notices, 
	{$rule}ul.tribe-related-events > li .tribe-related-events-thumbnail .cmsms_events_img_placeholder, 
	{$rule}.tribe-events-venue .cmsms_events_venue_header .cmsms_events_venue_header_right a, 
	{$rule}.tribe-events-venue .cmsms_events_venue_header .cmsms_events_venue_header_right a:before, 
	{$rule}.tribe-events-venue .cmsms_events_venue_header .cmsms_events_venue_header_right a:hover:before, 
	{$rule}.tribe-events-organizer .cmsms_events_organizer_header .cmsms_events_organizer_header_right a, 
	{$rule}.tribe-events-organizer .cmsms_events_organizer_header .cmsms_events_organizer_header_right a:hover:before, 
	{$rule}.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-number, 
	{$rule}.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-timer .tribe-countdown-colon, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info .duration:before, 
	{$rule}.widget .vcalendar .vevent .cmsms_widget_event_info .cmsms_widget_event_info_cost, 
	{$rule}.tribe-events-venue-widget .tribe-venue-widget-wrapper .cmsms_widget_event_info > div:before,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info .tribe-mini-calendar-event-venue, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-list-wrapper .tribe-events-loop .vevent .tribe-mini-calendar-event .list-info .tribe-mini-calendar-event-venue a {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-othermonth *,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-othermonth div[id*=\"tribe-events-daynum-\"],
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-othermonth div[id*=\"tribe-events-daynum-\"] a,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-othermonth * {
		color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 0.4);
	}
	
	{$rule}#tribe-events-bar .tribe-bar-filters,
	{$rule}#tribe-events-bar input,
	{$rule}#tribe-events-bar #tribe-bar-views label.button .tribe-bar-views-icon,
	{$rule}#tribe-events-bar #tribe-bar-views label.button .tribe-bar-views-icon:before,
	{$rule}#tribe-events-bar #tribe-bar-views label.button .tribe-bar-views-icon:after,
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-header,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-has-events div .tribe-mini-calendar-day-link:hover:before,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar thead th, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-mini-calendar-today {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"],
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-event-\"] {
		border-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 0.5);
	}
	
	{$rule}.tribe-events-tooltip:before {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.tribe-events-tooltip.recurring-info-tooltip:before {
		border-bottom-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 1) !important;
	}
	
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-body .tribe-events-tooltip:before {
		border-right-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 1) !important;
	}
	
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-body .tribe-events-right .tribe-events-tooltip:before {
		border-left-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . ", 1) !important;
	}
	
	@media only screen and (max-width: 767px) {
		{$rule}#main #tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-thismonth * {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_heading']) . "
		}
	}
	/* Finish Headings Color */
	
	
	/* Start Main Background Color */
	{$rule}#tribe-events-bar .tribe-bar-filters label,
	{$rule}#tribe-events-bar .tribe-bar-filters input,
	{$rule}#tribe-events-bar #tribe-bar-views label.button:hover,
	{$rule}#tribe-events-bar #tribe-bar-views.tribe-bar-views-open label.button,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-list-event-description .tribe-events-read-more:hover,
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-header .tribe-grid-content-wrap .column,
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-header .tribe-grid-content-wrap .column a,
	{$rule}.widget .tribe-events-widget-link > a:hover,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar thead th, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-mini-calendar-today *, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-mini-calendar-today:hover * {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}#tribe-events-bar #tribe-bar-views label.button:hover .tribe-bar-views-icon,
	{$rule}#tribe-events-bar #tribe-bar-views label.button:hover .tribe-bar-views-icon:before,
	{$rule}#tribe-events-bar #tribe-bar-views label.button:hover .tribe-bar-views-icon:after,
	{$rule}#tribe-events-bar #tribe-bar-views.tribe-bar-views-open label.button .tribe-bar-views-icon,
	{$rule}#tribe-events-bar #tribe-bar-views.tribe-bar-views-open label.button .tribe-bar-views-icon:before,
	{$rule}#tribe-events-bar #tribe-bar-views.tribe-bar-views-open label.button .tribe-bar-views-icon:after,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-list-event-description .tribe-events-read-more,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td,
	{$rule}.tribe-events-tooltip,
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-grid-body .tribe-week-grid-hours div,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .tribe-mini-calendar-nav div,
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar th.tribe-mini-calendar-dayofweek,
	{$rule}.widget .tribe-events-widget-link > a,
	{$rule}#tribe-events-bar #tribe-bar-views .tribe-bar-views-inner label.button .cmsms_next_arrow, 
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-othermonth, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-present div .tribe-mini-calendar-day-link:before, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-events-has-events.tribe-events-present div .tribe-mini-calendar-day-link:hover:before, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar .vcalendar td.tribe-mini-calendar-today div .tribe-mini-calendar-day-link:before {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}#tribe-events-bar .tribe-bar-filters input,
	{$rule}.tribe-events-tooltip:after,
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-body .tribe-events-tooltip:after,
	{$rule}#tribe-events-bar #tribe-bar-views .tribe-bar-views-inner label.button .cmsms_next_arrow:before, 
	{$rule}#tribe-events-bar #tribe-bar-views .tribe-bar-views-inner label.button .cmsms_next_arrow:after {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.tribe-events-tooltip.recurring-info-tooltip:after {
		border-bottom-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 1) !important;
	}
	
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-body .tribe-events-tooltip:after {
		border-right-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 1) !important;
	}
	
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-body .tribe-events-right .tribe-events-tooltip:after {
		border-left-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 1) !important;
	}
	/* Finish Main Background Color */
	
	
	/* Start Alternate Background Color */
	{$rule}#tribe-events-content.tribe-events-day .tribe-events-day-time-slot > h5,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td div[id*=\"tribe-events-daynum-\"],
	{$rule}.tribe-events-venue-widget .tribe-venue-widget-wrapper .tribe-venue-widget-venue .tribe-venue-widget-venue-name,
	{$rule}#tribe-events-content .cmsms_event_date, 
	{$rule}.tribe-events-list-widget-events .cmsms_event_date, 
	{$rule}.vcalendar .vevent .cmsms_event_date, 
	{$rule}.tribe_events .cmsms_post_cont, 
	{$rule}#tribe-events-content.tribe-events-list .tribe-events-list-separator-month, 
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-mobile-day-heading, 
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-mobile-day-date, 
	{$rule}.tribe-events-notices, 
	{$rule}ul.tribe-related-events > li .tribe-related-events-thumbnail .cmsms_events_img_placeholder, 
	{$rule}#tribe-events-content.tribe-events-photo #tribe-events-photo-events .tribe-events-photo-event .tribe-events-photo-event-wrap, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-allday, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-week-today, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_alternate']) . "
	}
	
	@media only screen and (max-width: 767px) {
		{$rule}#main #tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td.tribe-events-thismonth {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_alternate']) . "
		}
	}
	/* Finish Alternate Background Color */
	
	
	/* Start Borders Color */
	{$rule}.bd_font_color {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}#tribe-events-content .cmsms_event_date,
	{$rule}#tribe-events-content.tribe-events-list .vevent .cmsms_events_list_event_wrap .tribe-events-list-event-description .tribe-events-read-more,
	{$rule}#tribe-events-content.tribe-events-list .tribe-events-list-separator-month,
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar,
	{$rule}.tribe-events-tooltip,
	{$rule}.vcalendar .vevent .cmsms_event_date,
	{$rule}.widget .tribe-events-widget-link > a,
	{$rule}#tribe-events-content.tribe-events-list .vevent, 
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td .tribe-events-viewmore, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-scroller, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-content-wrap .column, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-grid-allday, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-week-grid-outer-wrap .tribe-week-grid-inner-wrap .tribe-week-grid-block div, 
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-week-grid-outer-wrap .tribe-week-grid-inner-wrap .tribe-week-grid-block div:before, 
	{$rule}#tribe-mobile-container .tribe-mobile-day .tribe-events-mobile, 
	{$rule}.widget .vcalendar .vevent, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar th, 
	{$rule}.widget.tribe_mini_calendar_widget .tribe-mini-calendar-wrapper .tribe-mini-calendar-grid-wrapper .tribe-mini-calendar td {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}#tribe-events-content.tribe-events-week-grid .tribe-events-grid .tribe-week-grid-wrapper .tribe-week-grid-outer-wrap .tribe-week-grid-inner-wrap .tribe-week-grid-block div {
		border-bottom-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . ", 0.4);
	}
	
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td:first-child {
		" . cmsms_color_css('border-left-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}#tribe-events-content.tribe-events-month table.tribe-events-calendar tbody td:last-child {
		" . cmsms_color_css('border-right-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.tribe_events .cmsms_post_cont_info, 
	{$rule}.tribe_events .cmsms_post_footer_info {
		" . cmsms_color_css('border-top-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}#tribe-events-content.tribe-events-day .tribe-events-day-time-slot > h5,
	{$rule}#tribe-events-content.tribe-events-week-grid #tribe-mobile-container .tribe-mobile-day .tribe-mobile-day-date,
	{$rule}.tribe_events .cmsms_post_cont_info {
		" . cmsms_color_css('border-bottom-color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_border']) . "
	}
	
	{$rule}#tribe-events-bar #tribe-bar-views .tribe-bar-views-inner ul.tribe-bar-views-list li.tribe-bar-views-option a > span,
	{$rule}#tribe-events-bar #tribe-bar-views .tribe-bar-views-inner ul.tribe-bar-views-list li.tribe-bar-views-option.tribe-bar-active a:hover > span,
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-back a,
	{$rule}#tribe-events-content.tribe-events-single .cmsms_single_event_header .cmsms_single_event_header_right .tribe-events-cal-links a,
	{$rule}.widget .vcalendar .vevent .entry-title a {
		border-color:transparent;
	}
	/* Finish Borders Color */
	
	{$rule}#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner input::-webkit-input-placeholder {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner input::-moz-placeholder {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner input:-moz-placeholder {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}#tribe-events-bar .tribe-bar-filters .tribe-bar-filters-inner input:-ms-input-placeholder {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . "
	}

/***************** Finish {$title} Events Color Scheme Rules ******************/


";
		}
	}
	
	
	$custom_css .= "
/***************** Start Header Color Scheme Rules ******************/

	/* Start Header Content Color */
	.header_mid,
	.header_mid_inner .search_wrap.search_opened .search_bar_wrap button:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_color']) . "
	}
	/* Finish Header Content Color */
	
	
	/* Start Header Primary Color */
	.header_mid a,
	.header_mid h1 a:hover,
	.header_mid h2 a:hover,
	.header_mid h3 a:hover,
	.header_mid h4 a:hover,
	.header_mid h5 a:hover,
	.header_mid h6 a:hover,
	.header_mid .color_2,
	.header_mid h1,
	.header_mid h2,
	.header_mid h3,
	.header_mid h4,
	.header_mid h5,
	.header_mid h6,
	.header_mid h1 a,
	.header_mid h2 a,
	.header_mid h3 a,
	.header_mid h4 a,
	.header_mid h5 a,
	.header_mid h6 a,
	.header_mid #navigation > li > a,
	.header_mid .header_mid_inner .search_wrap .search_bar_wrap button:before,
	.header_mid .header_mid_inner .search_wrap .search_bar_wrap input[type=search],
	.header_mid_inner .social_wrap a {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_link']) . "
	}
	
	.header_mid_inner .search_wrap .search_bar_wrap form p.search_button button.cmsms_search_cancel:before,
	.header_mid_inner .search_wrap .search_bar_wrap form p.search_button button.cmsms_search_cancel:after,
	.header_mid .button:hover {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_link']) . "
	}
	
	.header_mid input[type=text]:focus,
	.header_mid input[type=email]:focus,
	.header_mid input[type=password]:focus,
	.header_mid input[type=search]:focus,
	.header_mid textarea:focus {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_header_link']) . "
	}
	/* Finish Header Primary Color */
	
	
	/* Start Header Rollover Color */
	.header_mid #navigation > li > a:hover,
	.header_mid #navigation > li.menu-item-highlight > a,
	.header_mid_inner .search_wrap .search_bar_wrap form p.search_button button.cmsms_theme_icon_search:hover:before,
	.header_mid_inner .social_wrap a:hover {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_hover']) . "
	}
	
	@media only screen and (min-width: 1024px) {
		.header_mid #navigation > li:hover > a,
		.header_mid #navigation > li.current-menu-ancestor > a,
		.header_mid #navigation > li.current-menu-item > a,
		.header_mid #navigation > li.current_page_item > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_hover']) . "
		}
	}
	
	.header_mid_inner .search_wrap .search_bar_wrap form p.search_button button.cmsms_search_cancel:hover:before,
	.header_mid_inner .search_wrap .search_bar_wrap form p.search_button button.cmsms_search_cancel:hover:after,
	.header_mid #navigation > li > a[data-tag]:before,
	.header_mid #navigation > li > a > span.nav_bg_clr {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_hover']) . "
	}
	/* Finish Header Rollover Color */
	
	
	/* Start Header Subtitle Color */
	.header_mid #navigation > li > a > span > span.nav_subtitle {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_subtitle']) . "
	}
	/* Finish Header Subtitle Color */
	
	
	/* Start Header Background Color */
	.header_mid .button,
	.header_mid .button:hover,
	.header_mid #navigation > li > a[data-tag]:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bg']) . "
	}
	
	.header_mid_outer,
	.header_mid .search_wrap .search_bar_wrap,
	.cmsms_dynamic_cart .cmsms_dynamic_cart_button,
	.header_mid input[type=text]:focus,
	.header_mid input[type=number]:focus,
	.header_mid input[type=email]:focus,
	.header_mid input[type=password]:focus,
	.header_mid input[type=search]:focus,
	.header_mid textarea:focus {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bg']) . "
	}
	/* Finish Header Background Color */
	
	
	/* Start Header Background Color on Scroll */
	.header_mid_scroll .header_mid_outer {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bg_scroll']) . "
	}
	
	.header_mid .search_wrap.search_opened {
		background-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_header_bg_scroll']) . ", 0.9);
	}
	/* Finish Header Background Color on Scroll */
	
	
	/* Start Header Borders Color */
	.header_mid input[type=text],
	.header_mid input[type=number],
	.header_mid input[type=email],
	.header_mid input[type=password],
	.header_mid input[type=search],
	.header_mid input[type=submit],
	.header_mid textarea,
	.header_mid select,
	.header_mid option {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_header_border']) . "
	}
	
	.header_mid hr,
	.header_mid .cmsms_divider {
		" . cmsms_color_css('border-bottom-color', $cmsms_option[CMSMS_SHORTNAME . '_header_border']) . "
	}
	/* Finish Header Borders Color */
	
	
	/* Start Header Dropdown Link Color */
	.header_mid #navigation ul li a,
	.header_mid #navigation > li > ul li.menu-item-has-children > a:after {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_link']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_mid #navigation > li > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_link']) . "
		}
	}
	
	@media only screen and (min-width: 1024px) {
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li:hover > a,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a:hover {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_link']) . "
		}
	}
	/* Finish Header Dropdown Link Color */
	
	
	/* Start Header Dropdown Rollover Color */
	.header_mid #navigation ul li > a:hover,
	.header_mid #navigation ul li.current-menu-item > a,
	.header_mid #navigation ul li.current_page_item > a,
	.header_mid #navigation > li li.menu-item-highlight > a,
	.header_mid #navigation > li li.menu-item-highlight > a:hover,
	.header_mid #navigation > li.menu-item-mega li > a:hover,
	.header_mid #navigation > li.menu-item-mega li li > a:hover,
	.header_mid #navigation > li.menu-item-mega > ul > li > a,
	.header_mid #navigation > li.menu-item-mega > ul > li > a:hover,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li > a:hover,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li.current-menu-item > a,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li.current_page_item > a,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li.menu-item-highlight > a,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li.menu-item-highlight > a:hover,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li.menu-item-highlight:hover > a:hover,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a[href]:hover,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul li li > a:hover,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul li li:hover > a:hover,
	.header_mid #navigation > li > ul li.menu-item-has-children:hover > a:after,
	.header_mid #navigation > li > ul li.menu-item-has-children > a:hover:after {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_hover']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_mid #navigation > li > a:hover,
		.header_mid #navigation > li.current-menu-item > a,
		.header_mid #navigation > li.current_page_item > a,
		.header_mid #navigation > li.menu-item-highlight > a,
		.header_mid #navigation > li > a:hover > span > span.nav_subtitle,
		.header_mid #navigation > li.current-menu-item > a > span > span.nav_subtitle,
		.header_mid #navigation > li.current_page_item > a > span > span.nav_subtitle,
		.header_mid #navigation > li.menu-item-highlight > a > span > span.nav_subtitle {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_hover']) . "
		}
	}
	
	@media only screen and (min-width: 1024px) {
		.header_mid #navigation > li.menu-item-mega > ul > li > a,
		.header_mid #navigation ul li:hover > a,
		.header_mid #navigation > li li.menu-item-highlight:hover > a,
		.header_mid #navigation > li.menu-item-mega li:hover > a,
		.header_mid #navigation > li.menu-item-mega > ul > li:hover > a,
		.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li:hover > a,
		.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul li li:hover > a,
		.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li.menu-item-highlight:hover > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_hover']) . "
		}
	}
	
	.header_mid #navigation ul li > a[data-tag]:before {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_hover']) . "
	}
	/* Finish Header Dropdown Rollover Color */
	
	
	/* Start Header Dropdown Subtitle Color */
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a > span > span.nav_subtitle,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li:hover > a > span > span.nav_subtitle,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a:hover > span > span.nav_subtitle {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_subtitle']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_mid #navigation > li > a > span > span.nav_subtitle {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_subtitle']) . "
		}
	}
	/* Finish Header Dropdown Subtitle Color */
	
	
	/* Start Header Dropdown Background Color */
	.header_mid #navigation ul li > a[data-tag]:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_bg']) . "
	}
	
	.header_mid input[type=text],
	.header_mid input[type=number],
	.header_mid input[type=email],
	.header_mid input[type=password],
	.header_mid input[type=search],
	.header_mid input[type=submit],
	.header_mid button,
	.header_mid textarea,
	.header_mid select,
	.header_mid option,
	.header_mid #navigation ul,
	.header_mid #navigation > li.menu-item-mega li > a:hover,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_bg']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_mid #navigation {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_bg']) . "
		}
	}
	
	@media only screen and (min-width: 1024px) {
		.header_mid #navigation > li.menu-item-mega li:hover > a {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_bg']) . "
		}
	}
	/* Finish Header Dropdown Background Color */
	
	
	/* Start Header Dropdown Borders Color */
	@media only screen and (max-width: 1024px) {
		.header_mid #navigation li a, 
		.header_mid #navigation > li > a:hover,
		.header_mid #navigation > li.current-menu-item > a,
		.header_mid #navigation > li.current_page_item > a {
			" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_border']) . "
		}
	}
	
	.header_mid #navigation ul li a,
	.header_mid #navigation > li.menu-item-mega > div.menu-item-mega-container li li li:first-child {
		" . cmsms_color_css('border-top-color', $cmsms_option[CMSMS_SHORTNAME . '_header_dropdown_border']) . "
	}
	/* Finish Header Dropdown Borders Color */
	
	
	/* Start Header Small Menu Background Color */
	@media only screen and (max-width: 1024px) {
		.header_mid_outer,
		.header_mid .search_wrap .search_bar_wrap,
		.cmsms_dynamic_cart .cmsms_dynamic_cart_button,
		.header_mid input[type=text]:focus,
		.header_mid input[type=number]:focus,
		.header_mid input[type=email]:focus,
		.header_mid input[type=password]:focus,
		.header_mid input[type=search]:focus,
		.header_mid textarea:focus {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_small_bg']) . "
		}
	
		.header_mid .search_wrap.search_opened {
			background-color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_header_small_bg']) . ", 0.9);
		}
	}
	/* Finish Header Small Menu Background Color */
	
	
	/* Start Header Custom Rules */
	.header_mid ::selection {
		" . cmsms_color_css('background', $cmsms_option[CMSMS_SHORTNAME . '_header_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bg']) . "
	}
	
	.header_mid ::-moz-selection {
		" . cmsms_color_css('background', $cmsms_option[CMSMS_SHORTNAME . '_header_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bg']) . "
	}
	
	#page #header .search_bar_wrap input::-webkit-input-placeholder {
		color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 0.3);
	}
	
	#page #header .search_bar_wrap input::-moz-placeholder {
		color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 0.3);
	}
	
	#page #header .search_bar_wrap input:-moz-placeholder {
		color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 0.3);
	}
	
	#page #header .search_bar_wrap input:-ms-input-placeholder {
		color:rgba(" . color2rgb($cmsms_option[CMSMS_SHORTNAME . '_' . $scheme . '_bg']) . ", 0.3);
	}
	";
	
	
	$custom_css .= "
	/* Finish Header Custom Rules */

/***************** Finish Header Color Scheme Rules ******************/



/***************** Start Header Bottom Color Scheme Rules ******************/

	/* Start Header Bottom Content Color */
	.header_bot,
	.header_bot .social_wrap a,
	.header_bot .search_wrap .search_bar_wrap button:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_color']) . "
	}
	/* Finish Header Bottom Content Color */
	
	
	/* Start Header Bottom Primary Color */
	.header_bot a,
	.header_bot h1 a:hover,
	.header_bot h2 a:hover,
	.header_bot h3 a:hover,
	.header_bot h4 a:hover,
	.header_bot h5 a:hover,
	.header_bot h6 a:hover,
	.header_bot .color_2,
	.header_bot h1,
	.header_bot h2,
	.header_bot h3,
	.header_bot h4,
	.header_bot h5,
	.header_bot h6,
	.header_bot h1 a,
	.header_bot h2 a,
	.header_bot h3 a,
	.header_bot h4 a,
	.header_bot h5 a,
	.header_bot h6 a,
	.header_bot #navigation > li > a {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_link']) . "
	}
	/* Finish Header Bottom Primary Color */
	
	
	/* Start Header Bottom Rollover Color */
	.header_bot #navigation > li > a:hover,
	.header_bot #navigation > li.menu-item-highlight > a {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_hover']) . "
	}
	
	@media only screen and (min-width: 1024px) {
		.header_bot #navigation > li:hover > a,
		.header_bot #navigation > li.current-menu-ancestor > a,
		.header_bot #navigation > li.current-menu-item > a,
		.header_bot #navigation > li.current_page_item > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_hover']) . "
		}
	}
	
	.header_bot #navigation > li > a[data-tag]:before,
	.header_bot #navigation > li > a > span.nav_bg_clr {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_hover']) . "
	}
	/* Finish Header Bottom Rollover Color */
	
	
	/* Start Header Bottom Subtitle Color */
	.header_bot #navigation > li > a > span > span.nav_subtitle {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_subtitle']) . "
	}
	/* Finish Header Bottom Subtitle Color */
	
	
	/* Start Header Bottom Background Color */
	.header_bot_outer {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_bg']) . "
	}
	/* Finish Header Bottom Background Color */
	
	
	/* Start Header Bottom Background Color on Scroll */
	.header_bot_scroll .header_bot_outer {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_bg_scroll']) . "
	}
	/* Finish Header Bottom Background Color on Scroll */
	
	
	/* Start Header Borders Color */
	.header_bot #navigation > li > a[data-tag]:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_border']) . "
	}
	/* Finish Header Bottom Borders Color */
	
	
	/* Start Header Bottom Dropdown Link Color */
	.header_bot #navigation ul li a,
	.header_bot #navigation > li > ul li.menu-item-has-children > a:after {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_link']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_bot #navigation > li > a,
		.responsive_nav:hover:before {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_link']) . "
		}
	}
	
	@media only screen and (min-width: 1024px) {
		.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a,
		.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li:hover > a,
		.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a:hover {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_link']) . "
		}
	}
	/* Finish Header Bottom Dropdown Link Color */
	
	
	/* Start Header Bottom Dropdown Rollover Color */
	.header_bot #navigation ul li > a:hover,
	.header_bot #navigation ul li.current-menu-item > a,
	.header_bot #navigation ul li.current_page_item > a,
	.header_bot #navigation > li li.menu-item-highlight > a,
	.header_bot #navigation > li li.menu-item-highlight > a:hover,
	.header_bot #navigation > li.menu-item-mega li > a:hover,
	.header_bot #navigation > li.menu-item-mega li li > a:hover,
	.header_bot #navigation > li.menu-item-mega > ul > li > a,
	.header_bot #navigation > li.menu-item-mega > ul > li > a:hover,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li > a:hover,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li.current-menu-item > a,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li.current_page_item > a,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li.menu-item-highlight > a,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li.menu-item-highlight > a:hover,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li.menu-item-highlight:hover > a:hover,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a[href]:hover,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul li li > a:hover,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul li li:hover > a:hover,
	.header_bot #navigation > li > ul li.menu-item-has-children:hover > a:after,
	.header_bot #navigation > li > ul li.menu-item-has-children > a:hover:after {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_hover']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_bot #navigation > li > a:hover,
		.header_bot #navigation > li.current-menu-item > a,
		.header_bot #navigation > li.current_page_item > a,
		.header_bot #navigation > li.menu-item-highlight > a,
		.responsive_nav:hover:before,
		.responsive_nav.active:before {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_hover']) . "
		}
	}
	
	@media only screen and (min-width: 1024px) {
		.header_bot #navigation ul li:hover > a,
		.header_bot #navigation > li li.menu-item-highlight:hover > a,
		.header_bot #navigation > li.menu-item-mega li:hover > a,
		.header_bot #navigation > li.menu-item-mega > ul > li > a,
		.header_bot #navigation > li.menu-item-mega > ul > li:hover > a,
		.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li:hover > a,
		.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul li li:hover > a,
		.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li.menu-item-highlight:hover > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_hover']) . "
		}
	}
	
	.header_bot #navigation ul li > a[data-tag]:before {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_hover']) . "
	}
	/* Finish Header Bottom Dropdown Rollover Color */
	
	
	/* Start Header Bottom Dropdown Subtitle Color */
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a > span > span.nav_subtitle,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li:hover > a > span > span.nav_subtitle,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container > ul > li > a:hover > span > span.nav_subtitle {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_subtitle']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_bot #navigation > li > a > span > span.nav_subtitle {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_subtitle']) . "
		}
	}
	/* Finish Header Bottom Dropdown Subtitle Color */
	
	
	/* Start Header Bottom Dropdown Background Color */
	.header_bot #navigation ul li > a[data-tag]:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_bg']) . "
	}
	
	.header_bot #navigation ul,
	.header_bot #navigation > li.menu-item-mega li > a:hover,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_bg']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_bot #navigation {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_bg']) . "
		}
	}
	
	@media only screen and (min-width: 1024px) {
		.header_bot #navigation > li.menu-item-mega li:hover > a {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_bg']) . "
		}
	}
	/* Finish Header Bottom Dropdown Background Color */
	
	
	/* Start Header Bottom Dropdown Borders Color */
	@media only screen and (max-width: 1024px) {
		.header_bot #navigation li a, 
		.header_bot #navigation > li > a:hover,
		.header_bot #navigation > li.current-menu-item > a,
		.header_bot #navigation > li.current_page_item > a {
			" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_border']) . "
		}
	}
	
	.header_bot #navigation ul li a,
	.header_bot #navigation > li.menu-item-mega > div.menu-item-mega-container li li li:first-child {
		" . cmsms_color_css('border-top-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_dropdown_border']) . "
	}
	/* Finish Header Bottom Dropdown Borders Color */
	
	
	/* Start Header Bottom Small Menu Background Color */
	@media only screen and (max-width: 1024px) {
		.header_bot_outer,
		.header_bot .resp_nav_wrap {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_small_bg']) . "
		}
	}
	/* Finish Header Bottom Small Menu Background Color */
	
	
	/* Start Header Bottom Custom Rules */
	.header_bot ::selection {
		" . cmsms_color_css('background', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_bg']) . "
	}
	
	.header_bot ::-moz-selection {
		" . cmsms_color_css('background', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_bottom_bg']) . "
	}
	";
	
	
	$custom_css .= "
	/* Finish Header Bottom Custom Rules */

/***************** Finish Header Bottom Color Scheme Rules ******************/



/***************** Start Header Top Color Scheme Rules ******************/

	/* Start Header Top Content Color */
	.header_top {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_color']) . "
	}
	/* Finish Header Top Content Color */
	
	
	/* Start Header Top Primary Color */
	.header_top a,
	.header_top h1 a:hover,
	.header_top h2 a:hover,
	.header_top h3 a:hover,
	.header_top h4 a:hover,
	.header_top h5 a:hover,
	.header_top h6 a:hover,
	.header_top .color_2,
	.header_top h1,
	.header_top h2,
	.header_top h3,
	.header_top h4,
	.header_top h5,
	.header_top h6,
	.header_top h1 a,
	.header_top h2 a,
	.header_top h3 a,
	.header_top h4 a,
	.header_top h5 a,
	.header_top h6 a,
	.header_top_inner .meta_wrap,
	.header_top_inner .meta_wrap a,
	.header_top_inner .social_wrap a,
	.header_top #top_line_nav > li > a,
	.responsive_top_nav:hover:before,
	.responsive_top_nav.active:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_link']) . "
	}
	/* Finish Header Top Primary Color */
	
	
	/* Start Header Top Rollover Color */
	.header_top a:hover,
	.header_top_inner .meta_wrap a:hover,
	.header_top_inner .social_wrap a:hover,
	.header_top #top_line_nav > li > a:hover,
	.header_top #top_line_nav > li.current-menu-item > a,
	.header_top #top_line_nav > li.current_page_item > a,
	.responsive_top_nav:before {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_hover']) . "
	}
	
	@media only screen and (min-width: 1024px) {
		.header_top #top_line_nav > li:hover > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_hover']) . "
		}
	}
	
	.header_top_but .cmsms_top_arrow_pixel, 
	.header_top_but .cmsms_bot_arrow_pixel {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_hover']) . "
	}
	
	.header_top_but .cmsms_top_arrow_pixel:before, 
	.header_top_but .cmsms_top_arrow_pixel:after, 
	.header_top_but .cmsms_top_arrow_pixel span:before, 
	.header_top_but .cmsms_top_arrow_pixel span:after, 
	.header_top_but .cmsms_bot_arrow_pixel:before, 
	.header_top_but .cmsms_bot_arrow_pixel:after, 
	.header_top_but .cmsms_bot_arrow_pixel span:before, 
	.header_top_but .cmsms_bot_arrow_pixel span:after {
		" . cmsms_color_css('border-color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_hover']) . "
	}
	/* Finish Header Top Rollover Color */
	
	
	/* Start Header Top Background Color */
	.header_top #top_line_nav > li > a > span.cmsms_count {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_bg']) . "
	}

	.header_top {
		" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_bg']) . "
	}
	/* Finish Header Top Background Color */
	
	
	/* Start Header Top Dropdown Link Color */
	.header_top #top_line_nav ul li a,
	.header_top  nav > div > ul > li > ul li.menu-item-has-children > a:after {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_dropdown_link']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_top #top_line_nav > li > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_dropdown_link']) . "
		}
	}
	/* Finish Header Top Dropdown Link Color */
	
	
	/* Start Header Top Dropdown Rollover Color */
	.header_top #top_line_nav ul li > a:hover,
	.header_top #top_line_nav ul li.current-menu-item > a,
	.header_top #top_line_nav ul li.current_page_item > a,
	.header_top nav > div > ul > li > ul li.menu-item-has-children:hover > a:after,
	.header_top nav > div > ul > li > ul li.menu-item-has-children > a:hover:after {
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_dropdown_hover']) . "
	}
	
	@media only screen and (max-width: 1024px) {
		.header_top #top_line_nav > li > a:hover,
		.header_top #top_line_nav > li.current-menu-item > a,
		.header_top #top_line_nav > li.current_page_item > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_dropdown_hover']) . "
		}
	}
	
	@media only screen and (min-width: 1024px) {
		.header_top #top_line_nav ul li:hover > a {
			" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_dropdown_hover']) . "
		}
	}
	/* Finish Header Top Dropdown Rollover Color */
	
	
	/* Start Header Top Dropdown Background Color */
	@media only screen and (max-width: 1024px) {
		.header_top #top_line_nav {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_dropdown_bg']) . "
		}
	}
	
	@media only screen and (min-width: 1024px) {
		.header_top #top_line_nav ul {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_dropdown_bg']) . "
		}
	}
	/* Finish Header Top Dropdown Background Color */
	
	
	/* Start Header Top Small Menu Background Color */
	@media only screen and (max-width: 1024px) {
		.header_top {
			" . cmsms_color_css('background-color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_small_bg']) . "
		}
	}
	/* Finish Header Top Small Menu Background Color */
	
	
	/* Start Header Top Custom Rules */
	.header_top ::selection {
		" . cmsms_color_css('background', $cmsms_option[CMSMS_SHORTNAME . '_header_top_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_bg']) . "
	}
	
	.header_top ::-moz-selection {
		" . cmsms_color_css('background', $cmsms_option[CMSMS_SHORTNAME . '_header_top_link']) . "
		" . cmsms_color_css('color', $cmsms_option[CMSMS_SHORTNAME . '_header_top_bg']) . "
	}
	";
	
	
	$custom_css .= "
	/* Finish Header Top Custom Rules */

/***************** Finish Header Top Color Scheme Rules ******************/

";
	
	
	return $custom_css;
}

