<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.0
 * 
 * Views Functions
 * Changed by CMSMasters
 * 
 */


function cmsmsView($show = true) {
	$post_ID = get_the_ID();
	
	
	$ip = $_SERVER['REMOTE_ADDR'];
	
	$ip_name = str_replace('.', '-', $ip);
	
	
	$views = (get_post_meta($post_ID, 'cmsms_views', true) != '') ? get_post_meta($post_ID, 'cmsms_views', true) : '0';
	
	
	$ipPost = new WP_Query(array( 
		'post_type' => 		'cmsms_view', 
		'post_status' => 	'draft', 
		'post_parent' => 	$post_ID, 
		'name' => 			$ip_name 
	));
	
	
	$ipCheck = $ipPost->posts;
	
	
	
	if (
		is_single() && 
		(
			!isset($_COOKIE['view-' . $post_ID]) || 
			count($ipCheck) == 0
		)
	) {
		$counter = '<span id="cmsmsView-' . esc_attr($post_ID) . '" class="cmsmsView no_active cmsms_theme_icon_view"><span>' . esc_html($views) . '</span></span>';
	} elseif (
		isset($_COOKIE['view-' . $post_ID]) || 
		count($ipCheck) != 0
	) {
		$counter = '<span id="cmsmsView-' . esc_attr($post_ID) . '" class="cmsmsView active cmsms_theme_icon_view"><span>' . esc_html($views) . '</span></span>';
	} else {
		$counter = '<span id="cmsmsView-' . esc_attr($post_ID) . '" class="cmsmsView cmsms_theme_icon_view"><span>' . esc_html($views) . '</span></span>';
	}
	
	
	if ($show != true) {
		return $counter;
	} else {
		echo $counter;
	}
}

