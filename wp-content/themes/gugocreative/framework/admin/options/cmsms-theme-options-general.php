<?php 
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.0
 * 
 * Post, Page, Project & Profile General Options
 * Created by CMSMasters
 * 
 */


$cmsms_option = cmsmasters_get_global_options();


$cmsms_global_bg_col = (isset($cmsms_option[CMSMS_SHORTNAME . '_bg_col']) && $cmsms_option[CMSMS_SHORTNAME . '_bg_col'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_bg_col'] : '#fefefe';
$cmsms_global_bg_img_enable = (isset($cmsms_option[CMSMS_SHORTNAME . '_bg_img_enable']) && $cmsms_option[CMSMS_SHORTNAME . '_bg_img_enable'] !== '') ? (($cmsms_option[CMSMS_SHORTNAME . '_bg_img_enable'] == 1) ? 'true' : 'false') : 'true';
$cmsms_global_bg_img = (isset($cmsms_option[CMSMS_SHORTNAME . '_bg_img']) && $cmsms_option[CMSMS_SHORTNAME . '_bg_img'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_bg_img'] : '';
$cmsms_global_bg_rep = (isset($cmsms_option[CMSMS_SHORTNAME . '_bg_rep']) && $cmsms_option[CMSMS_SHORTNAME . '_bg_rep'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_bg_rep'] : 'repeat';
$cmsms_global_bg_pos = (isset($cmsms_option[CMSMS_SHORTNAME . '_bg_pos']) && $cmsms_option[CMSMS_SHORTNAME . '_bg_pos'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_bg_pos'] : 'top center';
$cmsms_global_bg_att = (isset($cmsms_option[CMSMS_SHORTNAME . '_bg_att']) && $cmsms_option[CMSMS_SHORTNAME . '_bg_att'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_bg_att'] : 'scroll';
$cmsms_global_bg_size = (isset($cmsms_option[CMSMS_SHORTNAME . '_bg_size']) && $cmsms_option[CMSMS_SHORTNAME . '_bg_size'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_bg_size'] : 'auto';


$cmsms_global_heading_alignment = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_alignment']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_alignment'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_heading_alignment'] : 'left';
$cmsms_global_heading_scheme = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_scheme']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_scheme'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_heading_scheme'] : 'first';
$cmsms_global_heading_bg_img_enable = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_bg_image_enable']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_image_enable'] !== '') ? (($cmsms_option[CMSMS_SHORTNAME . '_heading_bg_image_enable'] == 1) ? 'true' : 'false') : 'true';
$cmsms_global_heading_bg_img = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_bg_image']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_image'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_image'] : '';
$cmsms_global_heading_bg_rep = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_bg_repeat']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_repeat'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_repeat'] : 'repeat';
$cmsms_global_heading_bg_att = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_bg_attachment']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_attachment'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_attachment'] : 'scroll';
$cmsms_global_heading_bg_size = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_bg_size']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_size'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_size'] : 'cover';
$cmsms_global_heading_bg_color = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_bg_color']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_color'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_heading_bg_color'] : '';
$cmsms_global_heading_height = (isset($cmsms_option[CMSMS_SHORTNAME . '_heading_height']) && $cmsms_option[CMSMS_SHORTNAME . '_heading_height'] !== '') ? $cmsms_option[CMSMS_SHORTNAME . '_heading_height'] : '';


$cmsms_global_breadcrumbs = (isset($cmsms_option[CMSMS_SHORTNAME . '_breadcrumbs']) && $cmsms_option[CMSMS_SHORTNAME . '_breadcrumbs'] !== '') ? (($cmsms_option[CMSMS_SHORTNAME . '_breadcrumbs'] == 1) ? 'true' : 'false') : 'true';


$custom_general_meta_fields = array( 
	array( 
		'id'	=> 'cmsms_bg', 
		'type'	=> 'tab_start', 
		'std'	=> '' 
	), 
	array( 
		'label'	=> __('Background', 'music-band'), 
		'desc'	=> __('Use Default', 'music-band'), 
		'id'	=> 'cmsms_bg_default', 
		'type'	=> 'checkbox', 
		'hide'	=> '', 
		'std'	=> 'true' 
	), 
	array( 
		'label'	=> __('Background Color', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_bg_col', 
		'type'	=> 'color', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_bg_col 
	), 
	array( 
		'label'	=> __('Background Image Visibility', 'music-band'), 
		'desc'	=> __('Show', 'music-band'), 
		'id'	=> 'cmsms_bg_img_enable', 
		'type'	=> 'checkbox', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_bg_img_enable 
	), 
	array( 
		'label'	=> __('Background Image', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_bg_img', 
		'type'	=> 'image', 
		'hide'	=> 'true', 
		'cancel' => '', 
		'std'	=> $cmsms_global_bg_img, 
		'frame' => 'select', 
		'multiple' => false 
	), 
	array( 
		'label'	=> __('Background Repeat', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_bg_rep', 
		'type'	=> 'radio', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_bg_rep, 
		'options' => array( 
			'no-repeat' => array( 
				'label' => __('No Repeat', 'music-band'), 
				'value'	=> 'no-repeat' 
			), 
			'repeat-x' => array( 
				'label' => __('Repeat Horizontally', 'music-band'), 
				'value'	=> 'repeat-x' 
			), 
			'repeat-y' => array( 
				'label' => __('Repeat Vertically', 'music-band'), 
				'value'	=> 'repeat-y' 
			), 
			'repeat' => array( 
				'label' => __('Repeat', 'music-band'), 
				'value'	=> 'repeat' 
			) 
		) 
	), 
	array( 
		'label'	=> __('Background Position', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_bg_pos', 
		'type'	=> 'select', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_bg_pos, 
		'options' => array( 
			'top left' => array( 
				'label' => __('Top Left', 'music-band'), 
				'value'	=> 'top left' 
			), 
			'top center' => array( 
				'label' => __('Top Center', 'music-band'), 
				'value'	=> 'top center' 
			), 
			'top right' => array( 
				'label' => __('Top Right', 'music-band'), 
				'value'	=> 'top right' 
			), 
			'center left' => array( 
				'label' => __('Center Left', 'music-band'), 
				'value'	=> 'center left' 
			), 
			'center center' => array( 
				'label' => __('Center Center', 'music-band'), 
				'value'	=> 'center center' 
			), 
			'center right' => array( 
				'label' => __('Center Right', 'music-band'), 
				'value'	=> 'center right' 
			), 
			'bottom left' => array( 
				'label' => __('Bottom Left', 'music-band'), 
				'value'	=> 'bottom left' 
			), 
			'bottom center' => array( 
				'label' => __('Bottom Center', 'music-band'), 
				'value'	=> 'bottom center' 
			), 
			'bottom right' => array( 
				'label' => __('Bottom Right', 'music-band'), 
				'value'	=> 'bottom right' 
			) 
		) 
	), 
	array( 
		'label'	=> __('Background Attachment', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_bg_att', 
		'type'	=> 'radio', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_bg_att, 
		'options' => array( 
			'scroll' => array( 
				'label' => __('Scroll', 'music-band'), 
				'value'	=> 'scroll' 
			), 
			'fixed' => array( 
				'label' => __('Fixed', 'music-band'), 
				'value'	=> 'fixed' 
			) 
		) 
	), 
	array( 
		'label'	=> __('Background Size', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_bg_size', 
		'type'	=> 'radio', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_bg_size, 
		'options' => array( 
			'auto' => array( 
				'label' => __('Auto', 'music-band'), 
				'value'	=> 'auto' 
			), 
			'cover' => array( 
				'label' => __('Cover', 'music-band'), 
				'value'	=> 'cover' 
			), 
			'contain' => array( 
				'label' => __('Contain', 'music-band'), 
				'value'	=> 'contain' 
			)
		) 
	), 
	array( 
		'id'	=> 'cmsms_bg', 
		'type'	=> 'tab_finish' 
	), 
	array( 
		'id'	=> 'cmsms_heading', 
		'type'	=> 'tab_start', 
		'std'	=> '' 
	), 
	array( 
		'label'	=> __('Heading Text', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading', 
		'type'	=> 'radio', 
		'hide'	=> '', 
		'std'	=> 'disabled', 
		'options' => array( 
			'default' => array( 
				'label' => __('Default', 'music-band'), 
				'value'	=> 'default' 
			), 
			'custom' => array( 
				'label' => __('Custom', 'music-band'), 
				'value'	=> 'custom' 
			), 
			'disabled' => array( 
				'label' => __('Disabled', 'music-band'), 
				'value'	=> 'disabled' 
			) 
		) 
	), 
	array( 
		'label'	=> __('Heading Title', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_title', 
		'type'	=> 'text_long', 
		'hide'	=> 'true', 
		'std'	=> '' 
	), 
	array( 
		'label'	=> __('Heading Subtitle', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_subtitle', 
		'type'	=> 'textarea', 
		'hide'	=> 'true', 
		'std'	=> '' 
	), 
	array( 
		'label'	=> __('Heading Icon', 'music-band'), 
		'desc'	=> __('Choose your custom icon for this heading', 'music-band'), 
		'id'	=> 'cmsms_heading_icon', 
		'type'	=> 'icon', 
		'hide'	=> 'true', 
		'std'	=> '' 
	), 
	array( 
		'label'	=> __('Heading Alignment', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_alignment', 
		'type'	=> 'radio', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_heading_alignment, 
		'options' => array( 
			'left' => array( 
				'label' => __('Left', 'music-band'), 
				'value'	=> 'left' 
			), 
			'right' => array( 
				'label' => __('Right', 'music-band'), 
				'value'	=> 'right' 
			), 
			'center' => array( 
				'label' => __('Center', 'music-band'), 
				'value'	=> 'center' 
			) 
		) 
	), 
	array( 
		'label'	=> __('Heading Color Scheme', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_scheme', 
		'type'	=> 'select_scheme', 
		'hide'	=> 'false', 
		'std'	=> $cmsms_global_heading_scheme 
	), 
	array( 
		'label'	=> __('Heading Background Image Visibility', 'music-band'), 
		'desc'	=> __('Show', 'music-band'), 
		'id'	=> 'cmsms_heading_bg_img_enable', 
		'type'	=> 'checkbox', 
		'hide'	=> 'false', 
		'std'	=> $cmsms_global_heading_bg_img_enable 
	), 
	array( 
		'label'	=> __('Heading Background Image', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_bg_img', 
		'type'	=> 'image', 
		'hide'	=> 'true', 
		'cancel' => '', 
		'std'	=> $cmsms_global_heading_bg_img, 
		'frame' => 'select', 
		'multiple' => false 
	), 
	array( 
		'label'	=> __('Heading Background Repeat', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_bg_rep', 
		'type'	=> 'radio', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_heading_bg_rep, 
		'options' => array( 
			'no-repeat' => array( 
				'label' => __('No Repeat', 'music-band'), 
				'value'	=> 'no-repeat' 
			), 
			'repeat-x' => array( 
				'label' => __('Repeat Horizontally', 'music-band'), 
				'value'	=> 'repeat-x' 
			), 
			'repeat-y' => array( 
				'label' => __('Repeat Vertically', 'music-band'), 
				'value'	=> 'repeat-y' 
			), 
			'repeat' => array( 
				'label' => __('Repeat', 'music-band'), 
				'value'	=> 'repeat' 
			) 
		) 
	), 
	array( 
		'label'	=> __('Heading Background Attachment', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_bg_att', 
		'type'	=> 'radio', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_heading_bg_att, 
		'options' => array( 
			'scroll' => array( 
				'label' => __('Scroll', 'music-band'), 
				'value'	=> 'scroll' 
			), 
			'fixed' => array( 
				'label' => __('Fixed', 'music-band'), 
				'value'	=> 'fixed' 
			) 
		) 
	), 
	array( 
		'label'	=> __('Heading Background Size', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_bg_size', 
		'type'	=> 'radio', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_heading_bg_size, 
		'options' => array( 
			'auto' => array( 
				'label' => __('Auto', 'music-band'), 
				'value'	=> 'auto' 
			), 
			'cover' => array( 
				'label' => __('Cover', 'music-band'), 
				'value'	=> 'cover' 
			), 
			'contain' => array( 
				'label' => __('Contain', 'music-band'), 
				'value'	=> 'contain' 
			)
		) 
	),
	array( 
		'label'	=> __('Heading Background Color Overlay', 'music-band'), 
		'desc'	=> '', 
		'id'	=> 'cmsms_heading_bg_color', 
		'type'	=> 'rgba', 
		'hide'	=> 'false', 
		'std'	=> $cmsms_global_heading_bg_color 
	), 
	array( 
		'label'	=> __('Heading Height', 'music-band'), 
		'desc'	=> __('pixels', 'music-band'), 
		'id'	=> 'cmsms_heading_height', 
		'type'	=> 'number', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_heading_height, 
		'min' 	=> '0', 
		'max' 	=> '', 
		'step' 	=> '' 
	), 
	array( 
		'label'	=> __('Breadcrumbs', 'music-band'), 
		'desc'	=> __('Show', 'music-band'), 
		'id'	=> 'cmsms_breadcrumbs', 
		'type'	=> 'checkbox', 
		'hide'	=> 'true', 
		'std'	=> $cmsms_global_breadcrumbs 
	), 
	array( 
		'id'	=> 'cmsms_heading', 
		'type'	=> 'tab_finish' 
	) 
);


$custom_meta_fields = $custom_general_meta_fields;

