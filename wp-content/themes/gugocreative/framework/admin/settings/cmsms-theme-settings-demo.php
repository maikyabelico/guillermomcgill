<?php 
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.0
 * 
 * Admin Panel Theme Settings Import/Export
 * Created by CMSMasters
 * 
 */


function cmsmasters_options_demo_tabs() {
	$tabs = array();
	
	
	$tabs['import'] = esc_attr__('Import', 'music-band');
	$tabs['export'] = esc_attr__('Export', 'music-band');
	
	
	return $tabs;
}


function cmsmasters_options_demo_sections() {
	$tab = cmsmasters_get_the_tab();
	
	
	switch ($tab) {
	case 'import':
		$sections = array();
		
		$sections['import_section'] = esc_html__('Theme Settings Import', 'music-band');
		
		
		break;
	case 'export':
		$sections = array();
		
		$sections['export_section'] = esc_html__('Theme Settings Export', 'music-band');
		
		
		break;
	}
	
	
	return $sections;
} 


function cmsmasters_options_demo_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = cmsmasters_get_the_tab();
	}
	
	
	$options = array();
	
	
	switch ($tab) {
	case 'import':
		$options[] = array( 
			'section' => 'import_section', 
			'id' => CMSMS_SHORTNAME . '_demo_import', 
			'title' => __('Theme Settings', 'music-band'), 
			'desc' => __("Enter your theme settings data here and click 'Import' button", 'music-band'), 
			'type' => 'textarea', 
			'std' => '', 
			'class' => '' 
		);
		
		
		break;
	case 'export':
		$options[] = array( 
			'section' => 'export_section', 
			'id' => CMSMS_SHORTNAME . '_demo_export', 
			'title' => __('Theme Settings', 'music-band'), 
			'desc' => __("Click here to export your theme settings data to the file", 'music-band'), 
			'type' => 'button', 
			'std' => __('Export Theme Settings', 'music-band'), 
			'class' => 'cmsms-demo-export' 
		);
		
		
		break;
	}
	
	
	return $options;	
}

