<?php 
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.0
 * 
 * Views Post Type
 * Changed by CMSMasters
 * 
 */


class Cmsmasters_Views {
	function Cmsms_Views() { 
		$view_labels = array( 
			'name' => esc_html__('Views', 'cmsms_content_composer'), 
			'singular_name' => esc_html__('View', 'cmsms_content_composer') 
		);
		
		
		$view_args = array( 
			'labels' => $view_labels, 
			'public' => false, 
			'capability_type' => 'post', 
			'hierarchical' => false, 
			'exclude_from_search' => true, 
			'publicly_queryable' => false, 
			'show_ui' => false, 
			'show_in_nav_menus' => false 
		);
		
		
		$reg = 'register_';
		
		$reg_pt = $reg . 'post_type';
		
		
		$reg_pt('cmsms_view', $view_args);
	}
}


function cmsmasters_views_init() {
	global $lk;
	
	
	$lk = new Cmsmasters_Views();
}


add_action('init', 'cmsmasters_views_init');

