/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.0
 * 
 * Visual Content Composer Schortcodes Extend
 * Created by CMSMasters
 * 
 */
 

/* 
// For Change Fields in Shortcodes

var sc_name_new_fields = {};


for (var id in cmsmsShortcodes.sc_name.fields) {
	if (id === 'field_name') { // Delete Field
		delete cmsmsShortcodes.sc_name.fields[id];
	} else if (id === 'field_name') { // Delete Field Attribute
		delete cmsmsShortcodes.sc_name.fields[id]['field_attribute'];  
		
		
		sc_name_new_fields[id] = cmsmsShortcodes.sc_name.fields[id];
	} else if (id === 'field_name') { // Add/Change Field Attribute
		cmsmsShortcodes.sc_name.fields[id]['field_attribute'] = 'value';
		
		
		sc_name_new_fields[id] = cmsmsShortcodes.sc_name.fields[id];
	} else if (id === 'field_name') { // Add Field (before the field as found, add new field)
		sc_name_new_fields['field_name'] = { 
			type : 		'rgba', 
			title : 	composer_shortcodes_extend.sc_name_field_custom_bg_color, 
			descr : 	'', 
			def : 		'', 
			required : 	false, 
			width : 	'half' 
		};
		
		
		sc_name_new_fields[id] = cmsmsShortcodes.sc_name.fields[id];
	} else { // Allways add this in the bottom
		sc_name_new_fields[id] = cmsmsShortcodes.sc_name.fields[id];
	}
}


cmsmsShortcodes.sc_name.fields = sc_name_new_fields; 
*/



/**
 * Heading Extend
 */

var heading_new_fields = {};


for (var id in cmsmsShortcodes.cmsms_heading.fields) {
	if (id === 'font_weight') {
		heading_new_fields['tablet_check'] = { 
			type : 		'checkbox', 
			title : 	composer_shortcodes_extend.heading_tablet_check, 
			descr : 	'', 
			def : 		'false', 
			required : 	false, 
			width : 	'half',  
			choises : { 
				'true' : cmsms_shortcodes.choice_enable 
			} 
		};
		heading_new_fields['tablet_font_size'] = { 
			type : 		'input', 
			title : 	composer_shortcodes_extend.heading_tablet_font_size, 
			descr : 	"<span>" + cmsms_shortcodes.note + ' ' + cmsms_shortcodes.size_zero_note + "</span>", 
			def : 		'', 
			required : 	false, 
			width : 	'number', 
			min : 		'0', 
			depend : 	'tablet_check:true' 
		};
		heading_new_fields['tablet_line_height'] = { 
			type : 		'input', 
			title : 	composer_shortcodes_extend.heading_tablet_line_height, 
			descr : 	"<span>" + cmsms_shortcodes.note + ' ' + cmsms_shortcodes.size_zero_note + "</span>", 
			def : 		'', 
			required : 	false, 
			width : 	'number', 
			min : 		'0', 
			depend : 	'tablet_check:true' 
		};
		
		heading_new_fields[id] = cmsmsShortcodes.cmsms_heading.fields[id];
	} else {
		heading_new_fields[id] = cmsmsShortcodes.cmsms_heading.fields[id];
	}
}


cmsmsShortcodes.cmsms_heading.fields = heading_new_fields;



/**
 * Quotes Extend
 */

var quotes_new_fields = {};


for (var id in cmsmsShortcodes.cmsms_quotes.fields) {
	if (id === 'mode') {
		quotes_new_fields[id] = cmsmsShortcodes.cmsms_quotes.fields[id];
		
		
		quotes_new_fields['type'] = { 
			type : 		'radio', 
			title : 	composer_shortcodes_extend.quotes_field_slider_type_title, 
			descr : 	composer_shortcodes_extend.quotes_field_slider_type_descr, 
			def : 		'box', 
			required : 	true, 
			width : 	'half', 
			choises : { 
						'box' : 	composer_shortcodes_extend.quotes_field_type_choice_box, 
						'center' : 	composer_shortcodes_extend.quotes_field_type_choice_center 
			}, 
			depend : 	'mode:slider' 
		};
	} else {
		quotes_new_fields[id] = cmsmsShortcodes.cmsms_quotes.fields[id];
	}
}


cmsmsShortcodes.cmsms_quotes.fields = quotes_new_fields;



/**
 * Posts Slider Extend
 */

var posts_slider_new_fields = {};


for (var id in cmsmsShortcodes.cmsms_posts_slider.fields) {

	if (id === 'blog_metadata') {
		cmsmsShortcodes.cmsms_posts_slider.fields[id]['choises'] = {
			'title' : 		cmsms_shortcodes.choice_title, 
			'excerpt' : 	cmsms_shortcodes.choice_excerpt, 
			'date' : 		cmsms_shortcodes.choice_date, 
			'categories' : 	cmsms_shortcodes.choice_categories, 
			'author' : 		cmsms_shortcodes.choice_author, 
			'comments' : 	cmsms_shortcodes.choice_comments, 
			'likes' : 		cmsms_shortcodes.choice_likes, 
			'views' : 		composer_shortcodes_extend.choice_view, 
			'more' : 		cmsms_shortcodes.choice_more 
		};
		
		
		posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
	} else {
		posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
	}
	
	
	if (composer_shortcodes_extend.cmsms_woo_exists === 'true') {
	
		if (id === 'post_type') {
			cmsmsShortcodes.cmsms_posts_slider.fields[id]['choises'] = {
				'post' : 		cmsms_shortcodes.posts_slider_field_poststype_choice_post, 
				'project' : 	cmsms_shortcodes.posts_slider_field_poststype_choice_project, 
				'product' : 	cmsms_shortcodes.products_title 
			};
			
			
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		} else {
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		}
		
		
		if (id === 'columns') {
			posts_slider_new_fields['product_columns'] = {
				type : 		'select', 
				title : 	cmsms_shortcodes.columns_count, 
				descr : 	cmsms_shortcodes.posts_slider_field_col_count_descr, 
				def : 		'4', 
				required : 	false, 
				width : 	'half', 
				choises : { 
							'1' : 	'1', 
							'2' : 	'2', 
							'3' : 	'3', 
							'4' : 	'4' 
				}, 
				depend : 	'post_type:product' 
			}; 
				
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		} else {
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		}
		
		
		if (id === 'portfolio_metadata') {
			posts_slider_new_fields['product_metadata'] = {
				type : 		'checkbox', 
				title : 	composer_shortcodes_extend.posts_slider_field_prmeta_title, 
				descr : 	composer_shortcodes_extend.posts_slider_field_prmeta_descr, 
				def : 		'rating,title,categories,price', 
				required : 	true, 
				width : 	'half', 
				choises : { 
							'rating' : 		composer_shortcodes_extend.choice_rating, 
							'title' : 		cmsms_shortcodes.choice_title, 
							'categories' : 	cmsms_shortcodes.choice_categories, 
							'price' : 		composer_shortcodes_extend.choice_price 
				}, 
				depend : 	'post_type:product' 
			}; 
				
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		} else {
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		}
		
		
		if (id === 'portfolio_categories') {
			posts_slider_new_fields['product_categories'] = {
				type : 		'select_multiple', 
				title : 	composer_shortcodes_extend.posts_slider_field_prcateg_title, 
				descr : 	composer_shortcodes_extend.posts_slider_field_prcateg_descr + "<br /><span>" + cmsms_shortcodes.note + ' ' + composer_shortcodes_extend.posts_slider_field_prcateg_descr_note + "</span>", 
				def : 		'', 
				required : 	false, 
				width : 	'half', 
				choises : 	cmsms_composer_product_categories(), 
				depend : 	'post_type:product' 
			}; 
				
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		} else {
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		}
		
		
		if (id === 'amount') {
			delete cmsmsShortcodes.cmsms_posts_slider.fields[id];
		} else if (id === 'columns') {
			delete cmsmsShortcodes.cmsms_posts_slider.fields[id]['depend'];
			
			
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		} else {
			posts_slider_new_fields[id] = cmsmsShortcodes.cmsms_posts_slider.fields[id];
		}
	}
}


cmsmsShortcodes.cmsms_posts_slider.fields = posts_slider_new_fields;



/**
 * Blog Extend
 */

var blog_new_fields = {};


for (var id in cmsmsShortcodes.cmsms_blog.fields) {
	if (id === 'layout_mode') {
		cmsmsShortcodes.cmsms_blog.fields[id]['choises']['puzzle'] = composer_shortcodes_extend.blog_field_layout_mode_puzzle;
		
		
		blog_new_fields[id] = cmsmsShortcodes.cmsms_blog.fields[id];
	} else {
		blog_new_fields[id] = cmsmsShortcodes.cmsms_blog.fields[id];
	}
	
	
	if (id === 'metadata') {
		cmsmsShortcodes.cmsms_blog.fields[id]['choises'] = {
			'date' : 		cmsms_shortcodes.choice_date, 
			'categories' : 	cmsms_shortcodes.choice_categories, 
			'author' : 		cmsms_shortcodes.choice_author, 
			'comments' : 	cmsms_shortcodes.choice_comments, 
			'likes' : 		cmsms_shortcodes.choice_likes, 
			'views' : 		composer_shortcodes_extend.choice_view, 
			'tags' : 		cmsms_shortcodes.choice_tags, 
			'more' : 		cmsms_shortcodes.choice_more 
		};
		
		
		blog_new_fields[id] = cmsmsShortcodes.cmsms_blog.fields[id];
	} else {
		blog_new_fields[id] = cmsmsShortcodes.cmsms_blog.fields[id];
	}
}


cmsmsShortcodes.cmsms_blog.fields = blog_new_fields;



/**
 * Player
 */
 
var cmsmsShortcodes_new_shortcode = {};


for (var id in cmsmsShortcodes) {
	if (id === 'cmsms_notice') {
		cmsmsShortcodes_new_shortcode['cmsms_players'] = { 
			title : 	composer_shortcodes_extend.player_title, 
			icon : 		'cmsms-icon-play', 
			pair : 		true, 
			content : 	'audio', 
			visual : 	false, 
			multiple : 	true, 
			def : 		'[cmsms_player]' + composer_shortcodes_extend.player_title + '[/cmsms_player]', 
			fields : { 
				// Player Type
				type : { 
					type : 		'radio', 
					title : 	composer_shortcodes_extend.player_type, 
					descr : 	'', 
					def : 		'classic', 
					required : 	false, 
					width : 	'half', 
					choises : { 
								'classic' : 		composer_shortcodes_extend.classic, 
								'full' : 			composer_shortcodes_extend.full 
					} 
				}, 
				// Player List Image
				image : { 
					type : 			'upload', 
					title : 		composer_shortcodes_extend.play_list_image, 
					descr : 		'', 
					def : 			'', 
					required : 		false, 
					width : 		'half', 
					frame : 		'post', 
					library : 		'image', 
					multiple : 		false, 
					description : 	false, 
					caption : 		false, 
					align : 		false, 
					link : 			false, 
					size : 			false 
				}, 
				// Audio
				audio : { 
					type : 		'multiple', 
					title : 	cmsms_shortcodes.audio, 
					descr : 	cmsms_shortcodes.audio_field_audio_descr, 
					def : 		'[cmsms_player]' + cmsms_shortcodes.media_def + '[/cmsms_player]', 
					required : 	true, 
					width : 	'full' 
				}, 
				// CSS3 Animation
				animation : { 
					type : 		'select', 
					title : 	cmsms_shortcodes.animation_title, 
					descr : 	cmsms_shortcodes.animation_descr + " <br /><span>" + cmsms_shortcodes.note + ' ' + cmsms_shortcodes.animation_descr_note + "</span>", 
					def : 		'', 
					required : 	false, 
					width : 	'half', 
					choises : 	get_animations() 
				}, 
				// Animation Delay
				animation_delay : { 
					type : 		'input', 
					title : 	cmsms_shortcodes.animation_delay_title, 
					descr : 	cmsms_shortcodes.animation_delay_descr + " <br /><span>" + cmsms_shortcodes.note + ' ' + cmsms_shortcodes.animation_delay_descr_note + "</span>", 
					def : 		'0', 
					required : 	false, 
					width : 	'number', 
					min : 		'0', 
					step : 		'50' 
				}, 
				// Additional Classes
				classes : { 
					type : 		'input', 
					title : 	cmsms_shortcodes.classes_title, 
					descr : 	cmsms_shortcodes.classes_descr, 
					def : 		'', 
					required : 	false, 
					width : 	'half' 
				}  
			}  
		};
		
		
		cmsmsShortcodes_new_shortcode[id] = cmsmsShortcodes[id];
	} else {
		cmsmsShortcodes_new_shortcode[id] = cmsmsShortcodes[id];
	}
}


cmsmsShortcodes = cmsmsShortcodes_new_shortcode;



/**
 * Player
 */

var cmsmsMultipleShortcodes_new_shortcode = {};


for (var id in cmsmsMultipleShortcodes) {
	if (id === 'cmsms_video') {
		cmsmsMultipleShortcodes_new_shortcode['cmsms_player'] = { 
			title : 	cmsms_shortcodes.cmsms_player_title, 
			pair : 		true, 
			content : 	'visibility', 
			visual : 	'<span>{{ data.artist }}</span><span> - </span><span>{{ data.audio_title }}</span>', 
			def : 		"", 
			fields : { 
				// Player List Image
				poster : { 
					type : 			'upload', 
					title : 		composer_shortcodes_extend.audio_image, 
					descr : 		'', 
					def : 			'', 
					required : 		false, 
					width : 		'half', 
					frame : 		'post', 
					library : 		'image', 
					multiple : 		false, 
					description : 	false, 
					caption : 		false, 
					align : 		false, 
					link : 			false, 
					size : 			false 
				}, 
				// Artist
				artist : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.artist_title, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'half' 
				}, 
				// Audio Title
				audio_title : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.audio_title, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'half' 
				}, 
				// Buy Audio
				visibility : { 
					type : 		'checkbox', 
					title : 	composer_shortcodes_extend.visibility_format_audio_title, 
					descr : 	'', 
					def : 		'true', 
					required : 	false, 
					width : 	'half', 
					choises : { 
								'true' : 	cmsms_shortcodes.choice_enable 
					}, 
					depend : 	'' 
				}, 
				// Audio Link mp3 Format
				src_mp3 : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.audio_link_field_audio_link_title_mp3, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'full' 
				}, 
				// Audio Link ogg Format
				src_ogg : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.audio_link_field_audio_link_title_ogg, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'full' 
				}, 
				// Audio Link webm Format
				src_webm : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.audio_link_field_audio_link_title_webm, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'full' 
				}, 
				// Buy Button
				buy : { 
					type : 		'checkbox', 
					title : 	composer_shortcodes_extend.buy_button_title, 
					descr : 	'', 
					def : 		'true', 
					required : 	false, 
					width : 	'half', 
					choises : { 
								'true' : 	cmsms_shortcodes.choice_enable 
					}, 
					depend : 	'' 
				},  
				// Link URL For Buy on Amazon
				amazon : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.link_field_buy_url_amazon_title, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'half' 
				}, 
				// Link URL For Buy on App-Store
				itunes : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.link_field_buy_url_app_title, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'half' 
				}, 
				// Link URL For Buy on Google-Play
				google : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.link_field_buy_url_google_title, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'half' 
				}, 
				// Link URL For Buy on Shopping-Cart
				shop : { 
					type : 		'input', 
					title : 	composer_shortcodes_extend.link_field_buy_url_cart_title, 
					descr : 	'', 
					def : 		'', 
					required : 	false, 
					width : 	'half' 
				} 
			} 
		};
		
		
		cmsmsMultipleShortcodes_new_shortcode[id] = cmsmsMultipleShortcodes[id];
	} else {
		cmsmsMultipleShortcodes_new_shortcode[id] = cmsmsMultipleShortcodes[id];
	}
}


cmsmsMultipleShortcodes = cmsmsMultipleShortcodes_new_shortcode;
