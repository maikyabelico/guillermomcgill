<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * Content Composer Toggle Shortcode
 * Created by CMSMasters
 * 
 */


extract(shortcode_atts($new_atts, $atts));
	
	
global $sort_toggles, 
	$toggle_active, 
	$toggle_counter;


$toggle_counter++;


$toggle_tags = explode(',', $tags);


foreach ($toggle_tags as $toggle_tag) {
	if ($toggle_tag != '') {
		$sort_toggles[generateSlug(trim($toggle_tag), 30)] = trim($toggle_tag);
	}
}


$out = '<div class="cmsms_toggle_wrap' . 
(($toggle_active == $toggle_counter) ? ' current_toggle' : '') . 
(($classes != '') ? ' ' . $classes : '') . 
'" data-tags="all ';


$tgl_tag_str = '';


foreach ($toggle_tags as $tgl_tag) {
	$tgl_tag_str .= generateSlug(trim($tgl_tag), 30) . ' ';
}


$out .= substr($tgl_tag_str, 0, strlen($tgl_tag_str) - 1);


$out .= '">' . "\n" . 
	'<div class="cmsms_toggle_title">' . "\n" . 
		'<a href="#">' . 
			$title . 
			'<span class="cmsms_toggle_plus">' . "\n" . 
				'<span class="cmsms_toggle_plus_hor"></span>' . "\n" . 
				'<span class="cmsms_toggle_plus_vert"></span>' . "\n" . 
			'</span>' . "\n" . 
		'</a>' . "\n" . 
	'</div>' . "\n" . 
	'<div class="cmsms_toggle">' . "\n" . 
		cmsms_divpdel('<div class="cmsms_toggle_inner">' . "\n" . 
			do_shortcode(wpautop($content)) . 
		'</div>' . "\n") . 
	'</div>' . "\n" . 
'</div>';


return $out;