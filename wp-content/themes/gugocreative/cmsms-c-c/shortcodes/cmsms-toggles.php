<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * Content Composer Toggles Shortcode
 * Created by CMSMasters
 * 
 */


extract(shortcode_atts($new_atts, $atts));
	
	
global $sort_toggles, 
	$toggle_active, 
	$toggle_counter;


$sort_toggles = array();

$toggle_active = (int) $active;

$toggle_counter = 0;

$toggles_filter = '';

$toggles = do_shortcode($content);


if ($sort == 'true') {
	$toggles_filter = '<div class="cmsms_toggles_filter">' . "\n\t" . 
		'<a href="#" data-key="all" title="' . __('All', 'cmsms_content_composer') . '" class="current_filter">' . __('All', 'cmsms_content_composer') . '</a>' . "\n";
	
	foreach ($sort_toggles as $sort_toggle_key => $sort_toggle_value) {
		$toggles_filter .= "\t" . ' <span class="cmsms_toggles_filter_sep">.<span></span></span> <a href="#" data-key="' . $sort_toggle_key . '" title="' . $sort_toggle_value . '">' . $sort_toggle_value . '</a>' . "\n";
	}
	
	$toggles_filter .= '</div>';
}


$out = '<div class="cmsms_toggles toggles_mode_' . $mode . 
(($classes != '') ? ' ' . $classes : '') . 
'"' . 
(($animation != '') ? ' data-animation="' . $animation . '"' : '') . 
(($animation != '' && $animation_delay != '') ? ' data-delay="' . $animation_delay . '"' : '') . 
'>' . 
	$toggles_filter . "\n" . 
	$toggles . 
'</div>';

