<?php
/**
 * @package 	WordPress
 * @subpackage 	Garage
 * @version		1.0.0
 * 
 * Content Composer Theme Shortcodes
 * Created by CMSMasters
 * 
 */


$cmsms_add = 'add_';

$cmsms_add_shcd = $cmsms_add . 'shortcode';


/**
 * Players
 */
function cmsms_players($atts, $content = null) {
	$new_atts = apply_filters('cmsms_players_atts_filter', array( 
		'type' => 				'', 
		'image' => 				'',
		'animation' => 			'', 
		'animation_delay' => 	'', 
		'classes' => 			'' 
	) );
	
	
	$shortcode_name = 'players';
	
	$shortcode_path = CMSMS_CONTENT_COMPOSER_TEMPLATE_DIR . '/cmsms-' . $shortcode_name . '.php';
	
	
	if (locate_template($shortcode_path)) {
		$template_out = cmsms_composer_load_template($shortcode_path, array( 
			'atts' => 		$atts, 
			'new_atts' => 	$new_atts, 
			'content' => 	$content 
		) );
		
		
		return $template_out;
	}
	
	
	extract(shortcode_atts($new_atts, $atts));
	
	
	$unique_id = uniqid();
	
	
	global $cmsms_players_image;
	
	$cmsms_players_image = $image;
	
	
	$out = '';
	$player_item_out = '';
	
	$player_item_out = do_shortcode($content);
	
	$player_item_out = substr($player_item_out, 0, -1);
	
	
	$out .= "
	<script type=\"text/javascript\">
		jQuery(document).ready(function () { 
			var cmsms_jplayer = '#cmsms_jplayer_{$unique_id}', 
				cmsms_jplayer_container = '#cmsms_jp_container_{$unique_id}';
			
			
			new jPlayerPlaylist( { 
				jPlayer : 				cmsms_jplayer, 
				cssSelectorAncestor : 	cmsms_jplayer_container 
			}, [ 
				{$player_item_out}
			], { 
				supplied : 				'webm, oga, mp3', 
				useStateClassSkin : 	true, 
				autoBlur : 				true, 
				smoothPlayBar : 		true, 
				volume : 				1,
				size : { 
					width : 	'60', 
					height : 	'60' 
				}, 
				play : function (e) { 
					jQuery(this).jPlayer('pauseOthers');
				}, 
				setmedia : function (e) { 
					var jp_container = jQuery(this).parent(), 
						jp_media = e.jPlayer.status.media;
					
					
					if (jp_media.artist !== undefined) {
						jp_container.find('.jp-details .jp-artist').html(jp_media.artist);
					} else {
						jp_container.find('.jp-details .jp-artist').empty();
					}
					
					
					if (jp_media.buy !== undefined && jp_media.buy) {
						if (jp_media.amazon !== undefined && jp_media.amazon !== '') {
							jp_container.find('.jp-buy-button-list .jp-item-buy-amazon').attr('href', jp_media.amazon);
						} else {
							jp_container.find('.jp-buy-button-list .jp-item-buy-amazon').removeAttr('href');
						}
						
						if (jp_media.itunes !== undefined && jp_media.itunes !== '') {
							jp_container.find('.jp-buy-button-list .jp-item-buy-itunes').attr('href', jp_media.itunes);
						} else {
							jp_container.find('.jp-buy-button-list .jp-item-buy-itunes').removeAttr('href');
						}
						
						if (jp_media.google !== undefined && jp_media.google !== '') {
							jp_container.find('.jp-buy-button-list .jp-item-buy-google').attr('href', jp_media.google);
						} else {
							jp_container.find('.jp-buy-button-list .jp-item-buy-google').removeAttr('href');
						}
						
						if (jp_media.shop !== undefined && jp_media.shop !== '') {
							jp_container.find('.jp-buy-button-list .jp-item-buy-shop').attr('href', jp_media.shop);
						} else {
							jp_container.find('.jp-buy-button-list .jp-item-buy-shop').removeAttr('href');
						}
					} else {
						jp_container.find('.jp-buy-button-list .jp-item-buy').removeAttr('href');
					}
				} 
			} );
			
			
			jQuery(cmsms_jplayer_container).find('.jp-toggles .jp-playlist').on('click', function () { 
				var cmsms_playlist_toggle = jQuery(this),
					cmsms_playlist = jQuery(cmsms_jplayer_container).find('.cmsms-playlist');
				
				
				if (cmsms_playlist_toggle.hasClass('jp-playlist-open')) {
					cmsms_playlist_toggle.removeClass('jp-playlist-open');
					
					
					cmsms_playlist.removeClass('cmsms-playlist-open');
				} else {
					cmsms_playlist_toggle.addClass('jp-playlist-open');
					
					
					cmsms_playlist.addClass('cmsms-playlist-open');
				}
			} );
		} );
	</script>";
	
	$out .= "<div id=\"cmsms_jp_container_{$unique_id}\" class=\"jp-audio cmsms-jplayer" . 
	(($classes != '') ? ' ' . $classes : '');
	
	if ($type == 'full') {
		$out .= ' cmsms-jplayer-full';
	}
	
	$out .= "\" role=\"application\" aria-label=\"media player\"" . 
	(($animation != '') ? ' data-animation="' . $animation . '"' : '') . 
	(($animation != '' && $animation_delay != '') ? ' data-delay="' . $animation_delay . '"' : '');
	$out .= ">
		<div class=\"jp-type-playlist\">
			<div class=\"jp-jplayer-wrap\">
				<div class=\"jp-jplayer-container\">
					<div class=\"jp-buy-button-list\">
						<a class=\"jp-item-buy jp-item-buy-amazon cmsms-player-amazon\" tabindex=\"0\"></a>
						<a class=\"jp-item-buy jp-item-buy-itunes cmsms-player-app-store\" tabindex=\"0\"></a>
						<a class=\"jp-item-buy jp-item-buy-google cmsms-player-google-play\" tabindex=\"0\"></a>
						<a class=\"jp-item-buy jp-item-buy-shop cmsms-player-shopping-cart\" tabindex=\"0\"></a>
					</div>
					<div id=\"cmsms_jplayer_{$unique_id}\" class=\"jp-jplayer\"></div>
					<div class=\"jp-gui jp-interface\">
						<div class=\"jp-interface-top\">
							<div class=\"jp-details\">
								<span class=\"jp-artist\" aria-label=\"artist\"></span>
								<span class=\"jp-title\" aria-label=\"title\">&nbsp;</span>
							</div>
							<div class=\"jp-toggles\">
								<button class=\"jp-repeat cmsms-player-repeat\" role=\"button\" tabindex=\"0\">repeat</button>
								<button class=\"jp-shuffle cmsms-player-shuffle\" role=\"button\" tabindex=\"0\">shuffle</button>
								<button class=\"jp-playlist cmsms-player-list-1\" role=\"button\" tabindex=\"0\">playlist</button>
							</div>
						</div>
						<div class=\"jp-progress\">
							<div class=\"jp-seek-bar\">
								<div class=\"jp-play-bar\"></div>
							</div>
						</div>
						<div class=\"jp-interface-bot\">
							<div class=\"jp-controls\">
								<button class=\"jp-previous cmsms-player-fast-backward\" role=\"button\" tabindex=\"0\">previous</button>
								<button class=\"jp-play cmsms-player-play\" role=\"button\" tabindex=\"0\">play</button>
								<button class=\"jp-next cmsms-player-fast-forward\" role=\"button\" tabindex=\"0\">next</button>
							</div>
							<div class=\"jp-time-holder\">
								<span class=\"jp-current-time\" role=\"timer\" aria-label=\"time\">&nbsp;</span>
								<span class=\"jp-time-space\">&nbsp;-&nbsp;</span>
								<span class=\"jp-duration\" role=\"timer\" aria-label=\"duration\">&nbsp;</span>
							</div>
						</div>
					</div>
					<div class=\"jp-buy-buttons\"></div>
				</div>
				<div class=\"jp-playlist cmsms-playlist\">
					<ul>
						<li>&nbsp;</li>
					</ul>
				</div>
				<div class=\"jp-no-solution\">
					<span>Update Required</span>
					To play the media you will need to either update your browser to a recent version.
				</div>
			</div>
		</div>
	</div>
	";

	
	return $out;
}

$cmsms_add_shcd('cmsms_players', 'cmsms_players');



/**
 * Player
 */
function cmsms_player($atts, $content = null) {
	$new_atts = apply_filters('cmsms_player_atts_filter', array( 
		'poster' => 		'',
		'artist' => 		'',
		'audio_title' =>	'',
		'visibility' => 	'',
		'src_mp3' => 		'',
		'src_ogg' => 		'', 
		'src_webm' => 		'', 
		'buy' => 			'',  
		'amazon' => 		'', 
		'itunes' => 		'', 
		'google' => 		'', 
		'shop' => 			'' 
	) );
	
	
	$shortcode_name = 'player';
	
	$shortcode_path = CMSMS_CONTENT_COMPOSER_TEMPLATE_DIR . '/cmsms-' . $shortcode_name . '.php';
	
	
	if (locate_template($shortcode_path)) {
		$template_out = cmsms_composer_load_template($shortcode_path, array( 
			'atts' => 		$atts, 
			'new_atts' => 	$new_atts, 
			'content' => 	$content 
		) );
		
		
		return $template_out;
	}
	
	
	extract(shortcode_atts($new_atts, $atts));
	
	
	global $cmsms_players_image;
	
	$player_image = $cmsms_players_image;
	
	
	if ($player_image != '') {
		$player_image_id = explode('|', $player_image);
		
		$player_image_src = wp_get_attachment_image_src($player_image_id[0], 'small-thumb');
		
		$player_image = $player_image_src[0];
	}
	
	
	if ($poster != '') {
		$poster_id = explode('|', $poster);
		
		$poster_src = wp_get_attachment_image_src($poster_id[0], 'small-thumb');
		
		$poster = $poster_src[0];
	} elseif ($player_image != '') {
		$poster = $player_image;
	}
	
	
	$player_item_out = "{
		title :			'{$audio_title}', 
		artist :		'{$artist}', 
		free :			'{$visibility}', 
		mp3 : 			'{$src_mp3}', 
		oga : 			'{$src_ogg}', 
		webm : 			'{$src_webm}', 
		poster :		'{$poster}', 
		buy : 			'{$buy}', 
		amazon :		'{$amazon}', 
		itunes : 		'{$itunes}', 
		google : 		'{$google}', 
		shop : 			'{$shop}' 
	},";
	
	
	return $player_item_out;
}

$cmsms_add_shcd('cmsms_player', 'cmsms_player');


