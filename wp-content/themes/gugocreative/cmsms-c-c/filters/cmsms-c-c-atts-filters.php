<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version 	1.0.0
 * 
 * Content Composer Attributes Filters
 * Created by CMSMasters
 * 
 */

/* // Sc Name Shortcode Attributes Filter
add_filter('sc_name_atts_filter', 'sc_name_atts');

function sc_name_atts() { // copy default atts from shortcodes.php in plugin folder, paste here and add custom atts
	return array( 
		'attr_name_1' => 				'attr_std_val_1', 
		'attr_name_2' => 				'attr_std_val_2', 
		'attr_name_3' => 				'attr_std_val_3', 
		...
		'custom_attr_name_1' => 		'custom_attr_val_1', 
		'custom_attr_name_2' => 		'custom_attr_val_2', 
		'custom_attr_name_3' => 		'custom_attr_val_3' 
	);
} */


/* Register Admin Panel JS Scripts */
function register_admin_js_scripts() {
	global $pagenow;
	
	
	if (CMSMS_WOOCOMMERCE) {
		$cmsms_woo_exists = 'true';
	} else {
		$cmsms_woo_exists = 'false';
	}
	
	
	if ( 
		$pagenow == 'post-new.php' || 
		($pagenow == 'post.php' && isset($_GET['post']) && get_post_type($_GET['post']) != 'attachment') 
	) {
		wp_enqueue_script('composer-shortcodes-extend', get_template_directory_uri() . '/cmsms-c-c/js/cmsms-c-c-shortcodes-extend.js', array('cmsms_composer_shortcodes_js'), '1.0.0', true);
		
		wp_localize_script('composer-shortcodes-extend', 'composer_shortcodes_extend', array( 
			'cmsms_woo_exists' 							=> 		$cmsms_woo_exists, 
			'choice_view' 								=> 		esc_attr__('Views', 'music-band'),
			'heading_tablet_check' 						=> 		esc_attr__('Font size for small tablet', 'music-band'), 
			'heading_tablet_font_size' 					=> 		esc_attr__('Tablet font size', 'music-band'), 
			'size_zero_note' 							=> 		esc_attr__('number, in pixels (default value if empty or 0)', 'music-band'), 
			'heading_tablet_line_height' 				=> 		esc_attr__('Tablet line height', 'music-band'), 
			'posts_slider_field_prcateg_title' 			=> 		esc_attr__('Product Categories', 'music-band'), 
			'posts_slider_field_prcateg_descr' 			=> 		esc_attr__('Show product associated with certain categories', 'music-band'), 
			'posts_slider_field_prcateg_descr_note' 	=> 		esc_attr__('Note: If you dont choose any product categories, all your product will be shown', 'music-band'), 
			'posts_slider_field_prmeta_title' 			=> 		esc_attr__('Product Metadata', 'music-band'), 
			'posts_slider_field_prmeta_descr' 			=> 		esc_attr__('Choose product metadata you want to be shown', 'music-band'), 
			'choice_rating' 							=> 		esc_attr__('Rating', 'music-band'), 
			'choice_price' 								=> 		esc_attr__('Price', 'music-band'), 
			'blog_field_layout_mode_puzzle' 			=> 		esc_attr__('Puzzle', 'music-band'), 
			'quotes_field_slider_type_title' 			=> 		esc_attr__('Slider Type', 'music-band'), 
			'quotes_field_slider_type_descr'			=> 		esc_attr__('Choose your quotes slider style type', 'music-band'), 
			'quotes_field_type_choice_box' 				=> 		esc_attr__('Boxed', 'music-band'), 
			'quotes_field_type_choice_center' 			=> 		esc_attr__('Centered', 'music-band'), 
			'player_title' 								=> 		esc_attr__('Audio Player', 'music-band'), 
			'player_type' 								=> 		esc_attr__('Player Type', 'music-band'), 
			'classic' 									=> 		esc_attr__('Classic', 'music-band'), 
			'full' 										=> 		esc_attr__('Full', 'music-band') , 
			'play_list_image' 							=> 		esc_attr__('Play List Image', 'music-band'), 
			'audio_image' 								=> 		esc_attr__('Audio Image', 'music-band'),
			'audio_title' 								=> 		esc_attr__('Music Title', 'music-band'), 
			'artist_title' 								=> 		esc_attr__('Artist', 'music-band'), 
			'audio_link_field_audio_link_title_mp3' 	=> 		esc_attr__('Audio .mp3 File Format URL:', 'music-band'),
			'audio_link_field_audio_link_title_ogg' 	=> 		esc_attr__('Audio .ogg File Format URL:', 'music-band'), 
			'audio_link_field_audio_link_title_webm' 	=> 		esc_attr__('Audio .webm File Format URL:', 'music-band'), 
			'buy_button_title' 							=> 		esc_attr__('Buy Button', 'music-band'), 
			'visibility_format_audio_title' 			=> 		esc_attr__('Visibility Format Audio', 'music-band'), 
			'link_field_buy_url_amazon_title' 			=> 		esc_attr__('Link for button buy to Amazon', 'music-band'), 
			'link_field_buy_url_app_title' 				=> 		esc_attr__('Link for button buy to App-Store', 'music-band'), 
			'link_field_buy_url_google_title'			=> 		esc_attr__('Link for button buy to Google-Play', 'music-band'), 
			'link_field_buy_url_cart_title' 			=> 		esc_attr__('Link for button buy to Shopping-Cart', 'music-band') 
		));
	}
}

add_action('admin_enqueue_scripts', 'register_admin_js_scripts');


// Quotes Shortcode Attributes Filter
add_filter('cmsms_quotes_atts_filter', 'cmsms_quotes_atts');

function cmsms_quotes_atts() {
	return array( 
		'mode' => 				'grid', 
		'type' => 				'boxed', 
		'columns' => 			'2', 
		'speed' => 				'10', 
		'animation' => 			'', 
		'animation_delay' => 	'', 
		'classes' => 			'' 
	);
}

// Heading Shortcode Attributes Filter
add_filter('cmsms_custom_heading_atts_filter', 'cmsms_custom_heading_atts');

function cmsms_custom_heading_atts() {
	return array( 
		'type' => 					'h1', 
		'font_family' => 			'', 
		'font_size' => 				'', 
		'line_height' => 			'', 
		'tablet_check' =>  			'', 
		'tablet_font_size' => 		'', 
		'tablet_line_height' => 	'', 
		'font_weight' => 			'400', 
		'font_style' => 			'normal', 
		'icon' => 					'', 
		'text_align' => 			'left', 
		'color' => 					'', 
		'bg_color' => 				'', 
		'link' => 					'', 
		'target' => 				'', 
		'margin_top' => 			'0', 
		'margin_bottom' => 			'0', 
		'border_radius' => 			'', 
		'divider' => 				'', 
		'divider_type' => 			'short', 
		'divider_height' => 		'1', 
		'divider_style' => 			'solid', 
		'divider_color' => 			'', 
		'animation' => 				'', 
		'animation_delay' => 		'', 
		'classes' => 				'' 
	);
}

// Posts Slider Shortcode Attributes Filter
add_filter('cmsms_posts_slider_atts_filter', 'cmsms_posts_slider_atts');

function cmsms_posts_slider_atts() {
	return array( 
		'orderby' => 				'date', 
		'order' => 					'DESC', 
		'post_type' => 				'post', 
		'blog_categories' => 		'', 
		'portfolio_categories' => 	'', 
		'columns' => 				'4', 
		'amount' => 				'1', 
		'count' => 					'12', 
		'pause' => 					'5', 
		'blog_metadata' => 			'title,date,categories,comments,likes,more', 
		'portfolio_metadata' => 	'title,categories,likes', 
		'animation' => 				'', 
		'animation_delay' => 		'', 
		'classes' => 				'', 
		'product_columns' => 		'4', 
		'product_metadata' => 		'rating,title,categories,price', 
		'product_categories' => 	'' 
	);
}


function cmsms_composer_product_categories() {
	$categories = get_terms('product_cat', array( 
		'hide_empty' => 0 
	));
	
	
	$out = "\n" . '<script type="text/javascript"> ' . "\n" . 
	'/* <![CDATA[ */' . "\n\t" . 
		'function cmsms_composer_product_categories() { ' . "\n\t\t" . 
			'return { ' . "\n";
	
	
	if (CMSMS_WOOCOMMERCE && !empty($categories)) {
		foreach ($categories as $category) {
			$out .= "\t\t\t\"" . $category->slug . "\" : \"" . esc_html($category->name) . "\", \n";
		}
		
		
		$out = substr($out, 0, -3);
	}
	
	
	$out .= "\n\t\t" . '}; ' . "\n\t" . 
		'} ' . "\n" . 
	'/* ]]> */' . "\n" . 
	'</script>' . "\n\n";
	
	
	echo $out;
}


add_action('admin_footer', 'cmsms_composer_product_categories');

