<?php
/**
 * @package 	WordPress
 * @subpackage 	Music Band
 * @version		1.0.1
 * 
 * Main Theme Functions File
 * Created by CMSMasters
 * 
 */


// Current Theme Constants
if (!defined('CMSMS_SHORTNAME')) {
	define('CMSMS_SHORTNAME', 'music-band');
}

if (!defined('CMSMS_FULLNAME')) {
	define('CMSMS_FULLNAME', 'Music Band');
}



/*** START EDIT THEME PARAMETERS HERE ***/

// Theme Settings System Fonts List
if (!function_exists('cmsmasters_system_fonts_list')) {
	function cmsmasters_system_fonts_list() {
		$fonts = array( 
			"Arial, Helvetica, 'Nimbus Sans L', sans-serif" => 'Arial', 
			"Calibri, 'AppleGothic', 'MgOpen Modata', sans-serif" => 'Calibri', 
			"'Trebuchet MS', Helvetica, Garuda, sans-serif" => 'Trebuchet MS', 
			"'Comic Sans MS', Monaco, 'TSCu_Comic', cursive" => 'Comic Sans MS', 
			"Georgia, Times, 'Century Schoolbook L', serif" => 'Georgia', 
			"Verdana, Geneva, 'DejaVu Sans', sans-serif" => 'Verdana', 
			"Tahoma, Geneva, Kalimati, sans-serif" => 'Tahoma', 
			"'Lucida Sans Unicode', 'Lucida Grande', Garuda, sans-serif" => 'Lucida Sans', 
			"'Times New Roman', Times, 'Nimbus Roman No9 L', serif" => 'Times New Roman', 
			"'Courier New', Courier, 'Nimbus Mono L', monospace" => 'Courier New', 
		);
		
		
		return $fonts;
	}
}



// Theme Settings Google Fonts List
if (!function_exists('cmsms_google_fonts_list')) {
	function cmsms_google_fonts_list() {
		$fonts = array( 
			'' => esc_html__('None', 'music-band'), 
			'Inconsolata:400,700' => 'Inconsolata', 
			'Roboto:300,300italic,400,400italic,500,500italic,700,700italic' => 'Roboto', 
			'Roboto+Condensed:400,400italic,700,700italic' => 'Roboto Condensed', 
			'Open+Sans:300,300italic,400,400italic,700,700italic' => 'Open Sans', 
			'Open+Sans+Condensed:300,300italic,700' => 'Open Sans Condensed', 
			'Droid+Sans:400,700' => 'Droid Sans', 
			'Droid+Serif:400,400italic,700,700italic' => 'Droid Serif', 
			'PT+Sans:400,400italic,700,700italic' => 'PT Sans', 
			'PT+Sans+Caption:400,700' => 'PT Sans Caption', 
			'PT+Sans+Narrow:400,700' => 'PT Sans Narrow', 
			'PT+Serif:400,400italic,700,700italic' => 'PT Serif', 
			'Ubuntu:400,400italic,700,700italic' => 'Ubuntu', 
			'Ubuntu+Condensed' => 'Ubuntu Condensed', 
			'Headland+One' => 'Headland One', 
			'Source+Sans+Pro:300,300italic,400,400italic,700,700italic' => 'Source Sans Pro', 
			'Lato:400,400italic,700,700italic' => 'Lato', 
			'Cuprum:400,400italic,700,700italic' => 'Cuprum', 
			'Oswald:300,400,700' => 'Oswald', 
			'Yanone+Kaffeesatz:300,400,700' => 'Yanone Kaffeesatz', 
			'Lobster' => 'Lobster', 
			'Lobster+Two:400,400italic,700,700italic' => 'Lobster Two', 
			'Questrial' => 'Questrial', 
			'Raleway:300,400,500,600,700' => 'Raleway', 
			'Dosis:300,400,500,700' => 'Dosis', 
			'Cutive+Mono' => 'Cutive Mono', 
			'Quicksand:300,400,700' => 'Quicksand', 
			'Titillium+Web:300,300italic,400,400italic,600,600italic,700,700italic' => 'Titillium Web', 
			'Montserrat:400,700' => 'Montserrat', 
			'Cookie' => 'Cookie', 
		);
		
		
		return $fonts;
	}
}



// Theme Settings Font Weights List
if (!function_exists('cmsms_font_weight_list')) {
	function cmsms_font_weight_list() {
		$list = array( 
			'normal' => 	'normal', 
			'100' => 		'100', 
			'200' => 		'200', 
			'300' => 		'300', 
			'400' => 		'400', 
			'500' => 		'500', 
			'600' => 		'600', 
			'700' => 		'700', 
			'800' => 		'800', 
			'900' => 		'900', 
			'bold' => 		'bold', 
			'bolder' => 	'bolder', 
			'lighter' => 	'lighter', 
		);
		
		
		return $list;
	}
}



// Theme Settings Font Styles List
if (!function_exists('cmsms_font_style_list')) {
	function cmsms_font_style_list() {
		$list = array( 
			'normal' => 	'normal', 
			'italic' => 	'italic', 
			'oblique' => 	'oblique', 
			'inherit' => 	'inherit', 
		);
		
		
		return $list;
	}
}



// Theme Settings Text Transforms List
if (!function_exists('cmsmasters_text_transform_list')) {
	function cmsmasters_text_transform_list() {
		$list = array( 
			'none' => 'none', 
			'uppercase' => 'uppercase', 
			'lowercase' => 'lowercase', 
			'capitalize' => 'capitalize', 
		);
		
		
		return $list;
	}
}



// Theme Settings Text Decorations List
if (!function_exists('cmsmasters_text_decoration_list')) {
	function cmsmasters_text_decoration_list() {
		$list = array( 
			'none' => 'none', 
			'underline' => 'underline', 
			'overline' => 'overline', 
			'line-through' => 'line-through', 
		);
		
		
		return $list;
	}
}



// Theme Settings Custom Color Schemes
if (!function_exists('cmsmasters_custom_color_schemes_list')) {
	function cmsmasters_custom_color_schemes_list() {
		$list = array( 
			'first' => esc_html__('Custom 1', 'music-band'), 
			'second' => esc_html__('Custom 2', 'music-band'), 
			'third' => esc_html__('Custom 3', 'music-band'), 
			'fourth' => esc_html__('Custom 4', 'music-band'), 
			'fifth' => esc_html__('Custom 5', 'music-band') 
		);
		
		
		return $list;
	}
}



// WP Color Picker Palettes
if (!function_exists('cmsms_color_picker_palettes')) {
	function cmsms_color_picker_palettes() {
		$palettes = array( 
			'#000000', 
			'#ffffff', 
			'#d43c18', 
			'#5173a6', 
			'#959595', 
			'#c0c0c0', 
			'#f4f4f4', 
			'#e1e1e1' 
		);
		
		
		return $palettes;
	}
}

/*** STOP EDIT THEME PARAMETERS HERE ***/



// Theme Plugin Support Constants
if (!defined('CMSMS_WOOCOMMERCE') && class_exists('woocommerce')) {
	define('CMSMS_WOOCOMMERCE', true);
} elseif (!defined('CMSMS_WOOCOMMERCE')) {
	define('CMSMS_WOOCOMMERCE', false);
}

if (!defined('CMSMS_EVENTS_CALENDAR') && class_exists('Tribe__Events__Main')) {
	define('CMSMS_EVENTS_CALENDAR', true);
} elseif (!defined('CMSMS_EVENTS_CALENDAR')) {
	define('CMSMS_EVENTS_CALENDAR', false);
}

if (!defined('CMSMS_PAYPALDONATIONS') && class_exists('PayPalDonations')) {
	define('CMSMS_PAYPALDONATIONS', true);
} elseif (!defined('CMSMS_PAYPALDONATIONS')) {
	define('CMSMS_PAYPALDONATIONS', false);
}

if (!defined('CMSMS_DONATIONS') && class_exists('Cmsms_Donations')) {
	define('CMSMS_DONATIONS', false);
} elseif (!defined('CMSMS_DONATIONS')) {
	define('CMSMS_DONATIONS', false);
}

if (!defined('CMSMS_TIMETABLE') && function_exists('timetable_events_init')) {
	define('CMSMS_TIMETABLE', true);
} elseif (!defined('CMSMS_TIMETABLE')) {
	define('CMSMS_TIMETABLE', false);
}



// Theme Image Thumbnails Size
if (!function_exists('cmsms_image_thumbnail_list')) {
	function cmsms_image_thumbnail_list() {
		$list = array( 
			'small-thumb' => array( 
				'width' => 		55, 
				'height' => 	55, 
				'crop' => 		true 
			), 
			'square-thumb' => array( 
				'width' => 		300, 
				'height' => 	300, 
				'crop' => 		true, 
				'title' => 		esc_attr__('Square', 'music-band') 
			), 
			'blog-masonry-thumb' => array( 
				'width' => 		580, 
				'height' => 	325, 
				'crop' => 		true, 
				'title' => 		esc_attr__('Masonry Blog', 'music-band') 
			), 
			'project-thumb' => array( 
				'width' => 		580, 
				'height' => 	580, 
				'crop' => 		true, 
				'title' => 		esc_attr__('Project', 'music-band') 
			), 
			'project-masonry-thumb' => array( 
				'width' => 		580, 
				'height' => 	9999, 
				'title' => 		esc_attr__('Masonry Project', 'music-band') 
			), 
			'post-thumbnail' => array( 
				'width' => 		860, 
				'height' => 	615, 
				'crop' => 		true, 
				'title' => 		esc_attr__('Featured', 'music-band') 
			), 
			'masonry-thumb' => array( 
				'width' => 		860, 
				'height' => 	9999, 
				'title' => 		esc_attr__('Masonry', 'music-band') 
			), 
			'full-thumb' => array( 
				'width' => 		1160, 
				'height' => 	535, 
				'crop' => 		true, 
				'title' => 		esc_attr__('Full', 'music-band') 
			), 
			'project-full-thumb' => array( 
				'width' => 		800, 
				'height' => 	470, 
				'crop' => 		true, 
				'title' => 		esc_attr__('Project Full', 'music-band') 
			), 
			'full-masonry-thumb' => array( 
				'width' => 		1160, 
				'height' => 	9999, 
				'title' => 		esc_attr__('Masonry Full', 'music-band') 
			) 
		);
		
		
		if (CMSMS_EVENTS_CALENDAR) {
			$list['event-thumb'] = array( 
				'width' => 		580, 
				'height' => 	420, 
				'crop' => 		true, 
				'title' => 		esc_attr__('Event', 'music-band') 
			);
		}
		
		
		return $list;
	}
}



// Theme Settings All Color Schemes List
if (!function_exists('cmsmasters_all_color_schemes_list')) {
	function cmsmasters_all_color_schemes_list() {
		$list = array( 
			'default' => 		esc_html__('Default', 'music-band'), 
			'header' => 		esc_html__('Header', 'music-band'), 
			'header_top' => 	esc_html__('Header Top', 'music-band'), 
			'header_bottom' => 	esc_html__('Header Bottom', 'music-band'), 
			'bottom' => 		esc_html__('Bottom', 'music-band'), 
			'footer' => 		esc_html__('Footer', 'music-band') 
		);
		
		
		$out = array_merge($list, cmsmasters_custom_color_schemes_list());
		
		
		return $out;
	}
}



// Theme Settings Color Schemes List
if (!function_exists('cmsms_color_schemes_list')) {
	function cmsms_color_schemes_list() {
		$list = cmsmasters_all_color_schemes_list();
		
		
		unset($list['header']);
		
		unset($list['header_top']);
		
		unset($list['header_bottom']);
		
		
		$out = array_merge($list, cmsmasters_custom_color_schemes_list());
		
		
		return $out;
	}
}



// Theme Settings Color Schemes Default Colors
if (!function_exists('cmsmasters_color_schemes_defaults')) {
	function cmsmasters_color_schemes_defaults() {
		$list = array( 
			'default' => array( // content default color scheme
				'color' => 		'#2b2b2b', 
				'link' => 		'#111111', 
				'hover' => 		'#00e5cd', 
				'heading' => 	'#111111', 
				'bg' => 		'#fcfcfc', 
				'alternate' => 	'#fcfcfc', 
				'border' => 	'#1c1c1c' 
			), 
			'header' => array( // Header color scheme
				'color' => 				'#ffffff', 
				'link' => 				'#ffffff', 
				'hover' => 				'#00e5cd', 
				'subtitle' => 			'rgba(255,255,255,.4)', 
				'bg' => 				'rgba(17,17,17,0)', 
				'bg_scroll' => 			'#111111', 
				'border' => 			'#000000', 
				'dropdown_link' => 		'#ffffff', 
				'dropdown_hover' => 	'#00e5cd', 
				'dropdown_subtitle' => 	'rgba(255,255,255,.4)', 
				'dropdown_bg' => 		'#1e1e1e', 
				'dropdown_border' => 	'#000000', 
				'small_bg' => 			'#111111' 
			), 
			'header_top' => array( // Header Top color scheme
				'color' => 				'#00e5cd', 
				'link' => 				'#00e5cd', 
				'hover' => 				'#ffffff', 
				'bg' => 				'#000000', 
				'dropdown_link' => 		'#ffffff', 
				'dropdown_hover' => 	'#00e5cd', 
				'dropdown_bg' => 		'#000000', 
				'small_bg' => 			'#000000' 
			), 
			'header_bottom' => array( // Header Bottom color scheme
				'color' => 				'#ffffff', 
				'link' => 				'#ffffff', 
				'hover' => 				'#00e5cd', 
				'subtitle' => 			'rgba(255,255,255,.4)', 
				'bg' => 				'rgba(28,28,28,.9)', 
				'bg_scroll' => 			'#1c1c1c', 
				'border' => 			'#000000', 
				'dropdown_link' => 		'#ffffff', 
				'dropdown_hover' => 	'#00e5cd', 
				'dropdown_subtitle' => 	'rgba(255,255,255,.4)', 
				'dropdown_bg' => 		'#1c1c1c', 
				'dropdown_border' => 	'#000000', 
				'small_bg' => 			'rgba(28,28,28,.9)' 
			), 
			'bottom' => array( // Bottom color scheme
				'color' => 		'#ffffff', 
				'link' => 		'#ffffff', 
				'hover' => 		'#00e5cd', 
				'heading' => 	'#ffffff', 
				'bg' => 		'#1f2121', 
				'alternate' => 	'#1f2121', 
				'border' => 	'#2d2d2d' 
			), 
			'footer' => array( // Footer color scheme
				'color' => 		'rgba(255,255,255,.3)', 
				'link' => 		'rgba(255,255,255,.3)', 
				'hover' => 		'#00e5cd', 
				'heading' => 	'#ffffff', 
				'bg' => 		'#1c1c1c', 
				'alternate' => 	'#1f2121', 
				'border' => 	'#2d2d2d' 
			), 
			'first' => array( // custom color scheme 1
				'color' => 		'#959595', 
				'link' => 		'#d43c18', 
				'hover' => 		'#c0c0c0', 
				'heading' => 	'#1e1e1e', 
				'bg' => 		'#ffffff', 
				'alternate' => 	'#ffffff', 
				'border' => 	'#e1e1e1' 
			), 
			'second' => array( // custom color scheme 2
				'color' => 		'#959595', 
				'link' => 		'#d43c18', 
				'hover' => 		'#c0c0c0', 
				'heading' => 	'#0a0a0a', 
				'bg' => 		'#ffffff', 
				'alternate' => 	'#ededed', 
				'border' => 	'#e1e1e1' 
			), 
			'third' => array( // custom color scheme 3
				'color' => 		'#ffffff', 
				'link' => 		'#ffffff', 
				'hover' => 		'#ffffff', 
				'heading' => 	'#ffffff', 
				'bg' => 		'#3d3d3d', 
				'alternate' => 	'#3d3d3d', 
				'border' => 	'#ffffff' 
			), 
			'fourth' => array( // custom color scheme 4
				'color' => 		'#959595', 
				'link' => 		'#d43c18', 
				'hover' => 		'#c0c0c0', 
				'heading' => 	'#1e1e1e', 
				'bg' => 		'#f4f4f4', 
				'alternate' => 	'#ffffff', 
				'border' => 	'#e1e1e1' 
			), 
			'fifth' => array( // custom color scheme 5
				'color' => 		'#959595', 
				'link' => 		'#d43c18', 
				'hover' => 		'#c0c0c0', 
				'heading' => 	'#1e1e1e', 
				'bg' => 		'#ffffff', 
				'alternate' => 	'#f4f4f4', 
				'border' => 	'#e1e1e1' 
			) 
		);
		
		
		return $list;
	}
}



// CMSMasters Framework Directories Constants
if (!defined('CMSMS_FRAMEWORK')) {
	define('CMSMS_FRAMEWORK', get_template_directory() . '/framework');
}

if (!defined('CMSMS_ADMIN')) {
	define('CMSMS_ADMIN', CMSMS_FRAMEWORK . '/admin');
}

if (!defined('CMSMS_SETTINGS')) {
	define('CMSMS_SETTINGS', CMSMS_ADMIN . '/settings');
}

if (!defined('CMSMS_OPTIONS')) {
	define('CMSMS_OPTIONS', CMSMS_ADMIN . '/options');
}

if (!defined('CMSMS_ADMIN_INC')) {
	define('CMSMS_ADMIN_INC', CMSMS_ADMIN . '/inc');
}

if (!defined('CMSMS_CLASS')) {
	define('CMSMS_CLASS', CMSMS_FRAMEWORK . '/class');
}

if (!defined('CMSMS_FUNCTION')) {
	define('CMSMS_FUNCTION', CMSMS_FRAMEWORK . '/function');
}


if (!defined('CMSMS_COMPOSER')) {
	define('CMSMS_COMPOSER', get_template_directory() . '/cmsms-c-c');
}



// Load Framework Parts
require_once(CMSMS_SETTINGS . '/cmsms-theme-settings.php');

require_once(CMSMS_OPTIONS . '/cmsms-theme-options.php');

require_once(CMSMS_ADMIN_INC . '/admin-scripts.php');

require_once(CMSMS_ADMIN_INC . '/plugin-activator.php');

require_once(CMSMS_CLASS . '/likes-posttype.php');

require_once(CMSMS_CLASS . '/views-posttype.php');

require_once(CMSMS_CLASS . '/twitteroauth.php');

require_once(CMSMS_CLASS . '/widgets.php');

require_once(CMSMS_FUNCTION . '/breadcrumbs.php');

require_once(CMSMS_FUNCTION . '/likes.php');

require_once(CMSMS_FUNCTION . '/views.php');

require_once(CMSMS_FUNCTION . '/pagination.php');

require_once(CMSMS_FUNCTION . '/single-comment.php');

require_once(CMSMS_FUNCTION . '/theme-functions.php');

require_once(CMSMS_FUNCTION . '/theme-fonts.php');

require_once(CMSMS_FUNCTION . '/theme-colors-primary.php');

require_once(CMSMS_FUNCTION . '/theme-colors-secondary.php');

require_once(CMSMS_FUNCTION . '/template-functions.php');

require_once(CMSMS_FUNCTION . '/template-functions-post.php');

require_once(CMSMS_FUNCTION . '/template-functions-project.php');

require_once(CMSMS_FUNCTION . '/template-functions-profile.php');

require_once(CMSMS_FUNCTION . '/template-functions-shortcodes.php');

require_once(CMSMS_FUNCTION . '/template-functions-widgets.php');


if (class_exists('Cmsms_Content_Composer')) {
	require_once(CMSMS_COMPOSER . '/filters/cmsms-c-c-atts-filters.php');
	
	require_once(CMSMS_COMPOSER . '/shortcodes/theme-shortcodes.php');
}


// Woocommerce functions
if (CMSMS_WOOCOMMERCE) {
	require_once(get_template_directory() . '/woocommerce/cmsms-woo-functions.php');
}

// Events functions
if (CMSMS_EVENTS_CALENDAR) {
	require_once(get_template_directory() . '/tribe-events/cmsms-events-functions.php');
}



// Load Theme Local File
if (!function_exists('cmsmasters_load_theme_textdomain')) {
	function cmsmasters_load_theme_textdomain() {
		load_theme_textdomain('music-band', CMSMS_FRAMEWORK . '/languages');
	}
}

// Load Theme Local File Action
if (!has_action('after_setup_theme', 'cmsmasters_load_theme_textdomain')) {
	add_action('after_setup_theme', 'cmsmasters_load_theme_textdomain');
}



// Framework Activation & Data Import
if (!function_exists('cmsmasters_theme_activation')) {
	function cmsmasters_theme_activation() {
		if (get_option('cmsms_active_theme') != CMSMS_SHORTNAME) {
			add_option('cmsms_active_theme', CMSMS_SHORTNAME, '', 'yes');
			
			
			cmsmasters_add_global_options();
			
			
			cmsmasters_regenerate_styles();
			
			
			cmsmasters_add_global_icons();
			
			
			flush_rewrite_rules();
			
			
			wp_redirect(esc_url(admin_url('admin.php?page=cmsms-settings&upgraded=true')));
		}
	}
}

add_action('after_switch_theme', 'cmsmasters_theme_activation');



// Framework Deactivation
if (!function_exists('cmsmasters_theme_deactivation')) {
	function cmsmasters_theme_deactivation() {
		delete_option('cmsms_active_theme');
	}
}

// Framework Deactivation Action
if (!has_action('switch_theme', 'cmsmasters_theme_deactivation')) {
	add_action('switch_theme', 'cmsmasters_theme_deactivation');
}

