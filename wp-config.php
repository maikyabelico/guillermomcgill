<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mcgill_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y2]pQ8)A+w[E/*EBuD@(KyNL}HMSGxN%gUyk`kiZLaprMI! ,WXm5<jaVo.1z(=i');
define('SECURE_AUTH_KEY',  '2F]DIzX<Wz)3~Ii#*,!@P$g:7s_Q23=7K^M!)D#`I:{g9B;M`o2p?`pddawrehKW');
define('LOGGED_IN_KEY',    'yEA+} H9cC55NUbQQtefbZs^klM$pidzuA8o:p0h%T+ckhi-89P@>HAZ;k;`14T%');
define('NONCE_KEY',        'zP*gMP:YmB9Ql%Q8A(e0kVP<;B_TeA_h06uy*Jy{C93YW?pA6T#n!v(6skGEro?X');
define('AUTH_SALT',        '/=_0V5:@#T~-O >)Xqo8V#.,]gIV+pGSG`WR]_[@3m&A|k_b)UIHX4.ekD*I>#cO');
define('SECURE_AUTH_SALT', '+Qe [_OB;L8L*u[J.pggIvh~_!yVpPWSyvH;VMkSO)<j<;[PzUcB22*3<k$?n$0H');
define('LOGGED_IN_SALT',   '&*v_=b(D?:r*`iOy4CJn_vl`C,~.J1kI4N}Re5.T mpU7iXbbtf?CeVrX9^uqyi?');
define('NONCE_SALT',       'U[:%YLj;jc[CWQ{$*3{XJsG&spz5KcY;-h{;}g3|J}Y1L*@4#UR76IZ< MlP&AsW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('VP_ENVIRONMENT', 'guillermomcgill');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
